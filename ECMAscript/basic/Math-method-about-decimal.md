> 다음 링크를 학습한 내용입니다.  
> - [MSDN](https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/Math)


# ECMAscript

## basic - `Math` 소수점 관련 메소드

### 목차
- Math.ceil(number)
- Math.floor(number)
- Math.round(number)
- Math.trunc(number)

<br />

### Math.ceil(number)

`Math.ceil(number)` 함수는 주어진 숫자보다 크거나 같은 숫자 중 가장 작은 숫자를 integer로 반환합니다.

```JavaScript
let result;
result = Math.ceil(.95);
console.log(result); // 1
result = Math.ceil(4);
console.log(result); // 4
result = Math.ceil(7.004);
console.log(result); // 8
result = Math.ceil(-0.95);
console.log(result); // -0
result = Math.ceil(-4);
console.log(result); // -4
result = Math.ceil(-7.004);
console.log(result); // -7
```

<br />

### Math.floor(number)

`Math.floor(number)` 함수는 주어진 숫자와 같거나 작은 정수 중에서 가장 큰 수를 반환합니다.

```JavaScript
let result;
result = Math.floor(45.95);
console.log(result); // 45
result = Math.floor(-45.95);
console.log(result); // -46
```

<br />

### Math.round(number)

`Math.round(number)` 함수는 가장 가까운 정수로 반올림 한 값을 반환합니다.

```JavaScript
let result;
result = Math.floor(45.95);
console.log(result); // 45
result = Math.floor(-45.95);
console.log(result); // -46
```

<br />

### Math.trunc(number)

`Math.trunc(number)` 함수는 주어진 값의 소수부분을 제거하고 숫자의 정수부분을 반환합니다.

Math의 유사함수 3개: Math.floor(number), Math.ceil(number), Math.round(number) 와는 다르게,  
Math.trunc() 함수는 주어진 값이 양수이건 음수이건 상관없이 소수점 이하 우측 부분을 제거하는 매우 단순한 동작을합니다.  

```JavaScript
result = Math.trunc(13.37);
console.log(result); // 13
result = Math.trunc(0.123);
console.log(result); // 0
result = Math.trunc(-3.123);
console.log(result); // -3
result = Math.trunc('-3.123'); // 숫자형으로 자동 변환됨
console.log(result); // -3
result = Math.trunc(NaN);
console.log(result); // NaN
result = Math.trunc('foo');
console.log(result); // NaN
result = Math.trunc();
console.log(result); // NaN
```
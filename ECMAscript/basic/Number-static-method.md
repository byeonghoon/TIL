> 다음 링크들을 학습한 내용입니다.  
> - [MSDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)  
> - [https://github.com/ES678/Exploring-ES6](https://github.com/ES678/Exploring-ES6/tree/master/05%20Number%EC%99%80%20Math%EC%9D%98%20%EC%83%88%20%EA%B8%B0%EB%8A%A5)  


# ECMAscript

## basic - `Number 정적 메소드`

### 목차
- 개요
- Number.isFinite(number)
- Number.isInteger(number)
- Number.isNaN(number)
- Number.isSafeInteger(number)
- Number.parseFloat(number), Number.parseInt(number)


### 개요
수와 관련된 4개의 함수들은 여전히 전역 함수에 있고, `Number에 메소드로 추가 되었다.`  
`isFinite`와 `isNaN`, `parseFloat`와 `parseInt` 이 함수들은 모두 전역에 짝지어 진것과 같은 일을 하지만  
`isFinite`와 `isNaN`은 그 `함수의 인자값을 수로 더 이상 강제 변환 하지 않는다.`


<br />

### Number.isFinite(number)

- `Number.isFinite(number)`는 Number가 정확한 수인지(또는 Infinity, -Infinity, NaN이 아니지) 여부를 판단한다.
- 이 메소드는 전역에 있는 `isFinite`와 달리 강제로 매개변수를 숫자로 변환하지 않는다.

```JavaScript
let result;
result = Number.isFinite(Infinity);
console.log(result); // false
result = isFinite(Infinity);
console.log(result); // false
result = Number.isFinite(-Infinity);
console.log(result); // false
result = isFinite(-Infinity);
console.log(result); // false
result = Number.isFinite(NaN);
console.log(result); // false
result = isFinite(NaN);
console.log(result); // false
result = Number.isFinite(0);
console.log(result); // true
result = isFinite(0);
console.log(result); // true

// 이 메소드는 강제로 매개변수를 숫자로 변환하지 않습니다.
// 이는 오직 수치형 값에만 또한 그 값이 유한하면 true를 반환합니다.
result = Number.isFinite('123');
console.log(result); // false
result = isFinite('123');
console.log(result); // true => 매개변수가 숫자로 변환된 후 판단됨
result = Number.isFinite('0');
console.log(result); // false
result = isFinite('0');
console.log(result); // true => 매개변수가 숫자로 변환된 후 판단됨
result = Number.isFinite(null);
console.log(result); // false
result = isFinite(null);
console.log(result); // true => 매개변수가 숫자(0)로 변환된 후 판단됨
result = Number.isFinite(false);
console.log(result); // false
result = isFinite(false);
console.log(result); // true => 매개변수가 숫자(0)로 변환된 후 판단됨
result = Number.isFinite(undefined);
console.log(result); // false
result = isFinite(undefined);
console.log(result); // false
```

<br />

### Number.isInteger(number)

Javascript는 오로지 부동소수점 수(doubles)만 가진다.  
따라서 정수는 간단하게 소수 부분이 없는 부동 소수점 수 이다.  
만약 number에 소수가 없다면 `Number.isInteger(number)`는 true를 반환한다.  

```JavaScript
let result;
result = Number.isInteger(-17);
console.log(result);
result = Number.isInteger(33);
console.log(result);
result = Number.isInteger(33.1);
console.log(result);
result = Number.isInteger('33');
console.log(result);
result = Number.isInteger(NaN);
console.log(result);
result = Number.isInteger(Infinity);
console.log(result);
result = Number.isInteger(undefined);
console.log(result);
```

<br />

### Number.isNaN(number)

NaN인지 확인 할 때 등식(equality) 연산자 == 및 ===를 사용하면 false를 리턴한다.  
그래서 값이 NaN인지 판단하기 위해서 `Number.isNaN()` or `isNan()`이 필요하다.  
global에 있는 isNaN()은 매개변수가 숫자가 아닐시 강제 숫자로 변환 후 판단한다.  
반면 Number.isNaN()은 그렇지 않다.  

```JavaScript
let result;
result = NaN == NaN;
console.log(result); // false
result = NaN === NaN;
console.log(result); // false
result = Number.isNaN(NaN);
console.log(result); // true
result = Number.isNaN(Number.NaN);
console.log(result); // true
result = Number.isNaN(0 / 0);
console.log(result); // true
result = isNaN(NaN);
console.log(result); // true
result = isNaN(Number.NaN);
console.log(result); // true
result = isNaN(0 / 0);
console.log(result); // true
// 아래 사항들은 global isNaN()으로는 숫자로 강제 변환 후 true가 됐을 것임
result = isNaN("NaN");
console.log(result); // true
result = isNaN(undefined);
console.log(result); // true
result = isNaN({});
console.log(result); // true
result = isNaN("dwwdw");
console.log(result); // true
// 아래 사항들은 강제 변환이 되지 않기 때문에 모두 false를 반환함
result = Number.isNaN("NaN");
console.log(result); // false
result = Number.isNaN(undefined);
console.log(result); // false
result = Number.isNaN({});
console.log(result); // false
result = Number.isNaN("dwwdw");
console.log(result); // false
result = Number.isNaN(false);
console.log(result); // false
result = Number.isNaN(null);
console.log(result); // false
result = Number.isNaN(37);
console.log(result); // false
result = Number.isNaN("23");
console.log(result); // false
result = Number.isNaN("42.34");
console.log(result); // false
result = Number.isNaN("");
console.log(result); // false
result = Number.isNaN(" ");
console.log(result); // false
```

<br />

### Number.isSafeInteger(number)

Javascript 수는 오직 53bit의 부호있는 정수를 표현하는 저장 공간에 있다.  
-2^53 < i < 2^53은 정수 i는 안전 하다.  
아래 프로퍼티가 해당 정수가 Javascript 정수 표현 범위에 있는지(안전한지) 결정하는것을 돕는다.  

- `Number.isSafeInteger(number)`
- `Number.MIN_SAFE_INTEGER`
- `Number.MAX_SAFE_INTEGER`

```JavaScript
result = Number.isSafeInteger(Math.pow(-2, 53));
console.log(result);
result = Number.isSafeInteger(Math.pow(-2, 53) + 1);
console.log(result);
result = Number.isSafeInteger(Math.pow(2, 53) -1);
console.log(result);
result = Number.isSafeInteger(Math.pow(2, 53));
console.log(result);
console.log(Number.MAX_SAFE_INTEGER);
console.log(Number.MIN_SAFE_INTEGER);
```

<br />

### Number.parseFloat(number), Number.parseInt(number)

`Number.parseFloat(string)`,  `Number.parseInt(string, radix)` 이 두 메소드는 이름이 같은 전역함수와 정확히 동작이 같다.  
이것들은 완전성을 위하여 Number에 추가 되었다.  
이제 모든 수와 관련된 함수는 Number로 사용가능 하다.
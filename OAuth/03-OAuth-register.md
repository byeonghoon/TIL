# 03-OAuth-register

지금부터 구글과 페이스북에서 실제로 OAuth를 등록하는 절차를 살펴 볼 것이다.  

Client가 Resource Server를 이용하기 위해서는 사전에 Resource Server에 Client를 등록해 놔야한다.  

Resource Server마다 Client를 등록하는 방법은 다 다른다.  

하지만 공통적인 부분은 다음과 같다.  

- Client ID
    - 우리가 만들고 있는 애플리케이션을 식별할 수 있는 아이디
- Client Secret
    - 애플리케이션을 식별할 수 있는 아이디에 대한 비밀번호
    - Client ID는 외부에 노출될 수도 있지만, Client Secret는 절대로 외부에 노출되면 안된다.
- Authorized redirect URIs
    - ex: https://client/callback
    - Resource Server가 Client에게 권한을 부여하는 과정에서 Authorized Code라는 것을 전달해준다.
    - Authorized Code를 받기 위해서 Client에서 요청을 할 때 "이 주소로 요청할 거에요"를 Resource Server에 알려주는것아다.
    - 그럼 Resource Server는 해당 주소말고 다른 곳에서 들어온 요청들은 무시한다.

여기까지가 기본적인 구조이다.

그럼 이제부터 구글과 페이스북에서 OAuth 시스템을 어떻게 등록하는지 살펴볼것이다.  

<br />

## Facebook


[https://developers.facebook.com/](https://developers.facebook.com/) 접속한다.  
로그인을 한다.  
`내 앱` 탭에서 `새 앱 추가` 항목을 클릭한다.  

<img src="./img/OAuth-facebook-01.png" width="50%" height="50%">

<br />

앱 ID에 자신의 주소(opentutorials)를 적는다.  
그리고 앱 ID 만들기 버튼을 클릭한다.  

그럼 다음과 같은 화면이 나온다.  
그 중 에서 `facebook 로그인` 항목의 설정 버튼을 클릭한다.  

<img src="./img/OAuth-facebook-02.png" width="50%" height="50%">

<br />


여기서 우리는 웹을 할 것이기 때문에 웹 탭을 선택한다.  
그리고 우리의 사이트를 입력하고 `save` 버튼을 클릭한다.  

<img src="./img/OAuth-facebook-03.png" width="50%" height="50%">

<br />

다음으로 오른쪽 중앙쯤에 있는 `facebook 로그인` 부분의 `설정`을 클릭한다.  
여기서 하단에 내려보면 `URI 리디렉션 유효성 검사기` 부분에 Authorized redirect URIs을 입력하고 `변경 내용 저장` 버튼을 클릭한다.  
ex) http://client/callback  

<img src="./img/OAuth-facebook-04.png" width="50%" height="50%">

<br />

마지막으로 `설정` -> `기본 설정`에 들어와 보면 "앱 ID"와 "앱 시크릿 코드"를 확인 할 수 있다.  

<img src="./img/OAuth-facebook-05.png" width="50%" height="50%">

<br />

## Google

[https://console.cloud.google.com/](https://console.cloud.google.com/) 접속한다.  
로그인을 한다.  
`프로젝트를 선택`버튼을 클릭한다.  
그리고 `새 프로젝트`버튼을 클릭한다.  
여기서 프로젝트의 이름을 "opentutorials"이렇게 넣고 만들기 버튼을 클릭한다.
 
<img src="./img/OAuth-google-01.png" width="50%" height="50%">

<br />

다시 `프로젝트를 선택`버튼을 클릭해서 내가 생성한 "opentutorials" 프로젝트르 선택한다.  

<img src="./img/OAuth-google-02.png" width="50%" height="50%">

<br />

오른쪽 상단에 있는 햄버거 메뉴를 클릭한다.  

<img src="./img/OAuth-google-03.png" width="50%" height="50%">

<br />

오른쪽 상단에 있는 햄버거 메뉴를 클릭한다.  

<img src="./img/OAuth-google-03.png" width="50%" height="50%">

<br />

그리고 `API 및 서비스`에서 `사용자 인증 정보` 탭을 선택한다.  

<img src="./img/OAuth-google-04.png" width="50%" height="50%">

<br />

`사용자 인정 정보 만들기` 버튼을 클릭 -> `OAuth 클라이언트 ID` 탭을 선택한다.  

<img src="./img/OAuth-google-05.png" width="50%" height="50%">

<br />

오른쪽에 있는 `동의 화면 구성`버튼을 클릭한다.

<img src="./img/OAuth-google-06.png" width="50%" height="50%">

<br />

OAuth 동의 화면에서 `제품 이름`("opentutorials")를 입력하고 `저장`버튼을 클릭한다.

<img src="./img/OAuth-google-07.png" width="50%" height="50%">

<br />

그 다음에 우리 애플리케이션의 타입을 `웹 애플리케이션`으로 설정하고  
이름은 "opentutorials"  
승인된 리디렉션 URI: http://client.org/callback  
을 입력하고 `생성`버튼을 클릭한다.  

<img src="./img/OAuth-google-08.png" width="50%" height="50%">

<br />

그럼 다음과 같이 "앱 ID"와 "앱 시크릿 코드"를 확인 할 수 있다.  

<img src="./img/OAuth-google-09.png" width="50%" height="50%">

<br />

여기까지 구글과 페이스북에서 실제로 어떻게 우리의 Client를 등록하는지 살펴보았다.  

## 참고자료

- [생활코딩 - oAuth 2.0](https://opentutorials.org/module/3668)
- [facebook for developers](https://developers.facebook.com/)
- [Google Cloud Platform](https://cloud.google.com/)
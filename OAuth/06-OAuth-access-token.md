# 06-OAuth-access-token

OAuth의 목적은 access-token을 발급하는 것이다.  

이전 챕터에서 모든 인증이 통과했으면  
`Resource Server와 Client는 authorization_code를 지운다.`  

그리고 Resource Server는 accessToken을 생성, 저장하고  
Client에게 accessToken을 발급해준다.  
Client는 accessToken을 내부적으로 저장한다.  

accessToken은 다음과 같은 것을 보장한다.  
Client가 accessToken를 Resource Server로 전송하면  
Resource Server는 accessToken을 보고  
해당 Client가 accessToken에 해당하는 userId, scope에 대한 접근을 허용한다.  

<br />

## 참고자료

- [생활코딩 - oAuth 2.0](https://opentutorials.org/module/3668)
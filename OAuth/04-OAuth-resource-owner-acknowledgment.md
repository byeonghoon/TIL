# 04-OAuth-resource-owner-acknowledgment

인증을 받는 과정을 살펴볼것이다.

우선 등록을 하게되면  
Resource Server와 Client는 `Client Id`와 `Client Secret`, `redirect URL`을 알게된다.  

Resource Server가 가지고 있는 기능이 4개이고 각각의 기능이 A, B, C, D라고 할 때  
Client가 Resource Server의 모든 기능이 필요한 것이 아니라  
4개의 기능 중에서 B와 C의 기능이 필요하면 모든 기능에 대해서 인증을 받는것이 아니라  
최소한의 기능에 대해서만 인증을 받는것이 훨씬 더 합리적이다.  

이런 상황일 때 어떤 일이 일어나는지 살펴보자  

Resource Owner는 Client에 접속을 한다.  
그런데 Resource Owner가 Client를 통해서 Resource Server에 어떤 작업을 해야된다면  
Client는 Resource Owner에게 다음과 같은 화면을 보여준다.  
(페이스북, 구글, 트위터 로그인 할 수 있는 `버튼` 과 같이 동의를 구하는 화면)  
예를들면 다음과 같은 버튼들
[login with Facebook], [login with Google]m ,,,  

여기서 `버튼`은 별것이 아니라 다음과 같은 Resouce Server의 링크를 제공하는것이다.

예를들면 다음과 같다.  

```
https://resource.server/?client_id=1&scope=B,C&redirect_url=https://client/callback
```

Resource Owner가 버튼을 누르면 링크를 타고 Resource Server에 접속을 한다.  

그러면 Resource Server는 Resource Owner가 로그인이 되어 있는지 확인을 하고  
로그인이 안되어 있으면 로그인을 화면을 제공한다.  

Resource Owner가 로그인에 성공을하면  

Resource Server는 Resource Owner가 링크를 타고 접속한 URL에 client_id가 있는지 확인하고  
그리고 가지고 있는 client_id와 Resource Owner가 전송한 URL에 있는 client_id가 동일한지 확인을 한다.  
다르면 작업을 끝낸다.  
만약 동일하다면 Resource Owner에게 `scope`에 해당하는 권한을
Client에게 부여할 것 인지를 확인하는 메시지를 전송한다.  

Resource Owner가 메시지를 읽고 허용 버튼을 클릭하면  
Resource Server는 다음과 같은 정보를 기록한다. (userId: 1, scope: B, C)  
이 내용의 의미는 다음과 같다.  
`userId 1번 (Resource Owner의 아이디)은 scope B, C 사용하는 것을 허용하였다.`

여기까지가 Resource Owner로부터 Resource Server에 접속하는 것에 동의를 구하는 과정이다.  

다음 단원에서는 Resource Server가 실제로 인증을 어떻게 하는지는 다음에 살펴볼것이다.  

<br />

## 참고자료

- [생활코딩 - oAuth 2.0](https://opentutorials.org/module/3668)
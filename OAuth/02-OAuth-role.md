# 02-OAuth-role

## 동작 메커니즘

- 사용자
- 우리가 만드는 web or app(나의 서비스)
- 사용자의 정보를 보관하고 있는 구글의 칼린더(외부 서비스)

우리가 만드는 web or app에서 사용자의 구글의 캘린더 정보를 가지고와서  
그 정보를 변형해서 사용자에게 제공할려면 어떻게 해야 할까??  

여기서 부터는 OAuth 생태계를 이해하기 위해서 OAuth 생태계에서 사용하는 용어를 사용한다.  

- 우리가 만드는 web or app을 `Clinet`라고 부른다.  
- 우리가 만드는 web or app을 사용하고, 사용자(구글 칼린더의 정보의 주인)을 `Resource Owner`라고 부른다.  
- 그리고 Resource Owner의 리소스를 가지고 있는 서버를 `Resource Server`라고 부른다.  

먼저 Clinet를 Resource Server에 등록을 한다.(우리의 서비스는 당신들의 서비스를 사용할 것이라고)  
그러면 Resource Server는 Clinet로 `Client ID`, `Client Secret` 정보를 발급해 준다.  
Clinet는 발급해준 정보를 저장해 둔다. Client Secret은 외부에 절대로 노출되면 안된다.  

그 다음에는 Resource Owner가 Client를 통해서 Resource Server와 관련된 어떤 작업이 있을때  
Clinet는 Resource Owner에게 `Resource Server에 어떠한 정보를 사용 할 수 있게 허락을 해주세요`를 요청한다.  

Resource Owner가 Clinet로 접속하면 Resource Owner의 모니터 화면에는  
어떠한 이유 때문에 우리(Clinet)는 Resource Server에 정보가 필요합니다라는 내용과 함께 동의 버튼을 누를수 있는 화면이나온다.  

Resource Owner가 화면에서 동의 버튼을 누르면 Resource Owner의 요청이 Resource Server로 전달된다.  

Resource Server는 Resource Owner가 로그인이 안되어 있으면 로그인 화면을 연다.  

그 다음에 로그인을 하면 다음과 같은 화면이 나온다.  
`현재 구글 캘린더(Resource Server에 있는 정보)를 Client에서 사용할려고 한다.` 이것에 동의할려면 동의 버튼을 누르세요.

그리고 Resource Owner가 동의 버튼을 클릭하면 Resource Server가 Clinet에게 패스워드 정보를 준다.  
어떤 패스워드냐면 Client가 Resource Server에 있는 Resource Owner에 정보(글목록)을 사용하는 것에 동의했다는  
의미를 가지는 `code`라고 하는 것을 준다.  

`code`는 일종의 비밀번호 같은 것이다.(Client가 Resource Server에 있는 Resource Owner에 정보(글목록)을 읽을 수 있는 것에 대한)  

그런데 이게 끝이 아니고 한단계가 더 있다.  

이 Clinet는 Resource Server가 전달해준 `code`와 이전에 보관하고 있던 `Client ID`, `Client Secret`  
이 3개의 정보를 담아서 Resource Server로 다시 전달한다.  

그럼 Resource Server는 code, Client ID, Client Secret  
이 3개의 값을 비교해서 검증한다.  

검증 결과 모두 유효하면 Resource Server는 Client에게 `accessToken`을 발급해준다.  

그럼 Client는 accessToken을 자신의 파일이나 데이터베이스에 보관한다.  

그리고 Resource Server에서 제공하는 API를 사용할 떄  
accessToken을 Resource Server에 제출하면서 검증 후 Resource Owner의 리소스를 CRUD 한다.  

여기까지가 전체적인 동작 방법이다.  

그러나 이것을 애플리케이션에서 실제로 구현하는것은 굉장히 어렵기 때문에  
구글, 페이스북등과 같이 대부분의 OAuth를 지원하는 회사에서는 구현의 편의를 돕고자 `SDK`라는 도구를 제공한다.  

<br />

## 참고자료

- [생활코딩 - oAuth 2.0](https://opentutorials.org/module/3668)
# 07-OAuth-refresh-token

accessToken은 수명이 있다.  
일반적으로 1시간 2시간 길게는 60일 90일
그런데 수명이 끝난 accessToken을 가지고 API를 호출 했을 때 Resource Server는 더 이상 데이터를 주지 않는다.  

그럼 어떻게 해야 될까??

accessToken을 다시 발급 받아야 한다.  
그런데 그때 마다 사용자가 accessToken을 발급 받는 과정을 다시 거치는 것은 힘들다.  

바로 그런 경우에 손쉽가 accessToken을 발급 받을 수 있는  
방법이 refreshToken이라는 것이다.  

다음 [OAuth 2.0 RFC](https://tools.ietf.org/html/rfc6749#section-1.5)에서 refreshToken에 대한 설명이다.  

지금까지는 Resource Server를 Authorization Server가 포함된 개념으로 보았다.  
경우 따라서 포함된 경우도 있고 아닌 경우도 있다.  

refreshToken은 accessToken과 같이 발급되는 경우도 있고 그렇지 않은 경우도 있다.  

refreshToken을 통해서 accessToken을 다시 발급 받을 떄  
refreshToken도 다시 발급 받는 경우도 있고 그렇지 않는 경우도 있다.  

다음은 구글에서 Refresh Token 관련된 예제이다.  

```
POST /oauth2/v4/token HTTP/1.1
Host: www.googleapis.com
Content-Type: application/x-www-form-urlencoded

client_id=<your_client_id>&
client_secret=<your_client_secret>&
refresh_token=<refresh_token>&
grant_type=refresh_token
```

The following snippet shows a sample response:

```json
{
  "access_token":"1/fFAGRNJru1FTz70BzhT3Zg",
  "expires_in":3920,
  "token_type":"Bearer"
}
```

<br />

## 참고자료

- [생활코딩 - oAuth 2.0](https://opentutorials.org/module/3668)
- [OAuth 2.0 RFC - refreshToken](https://tools.ietf.org/html/rfc6749#section-1.5)
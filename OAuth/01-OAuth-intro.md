# 01-OAuth-intro

OAuth 2.0를 살펴보기 앞서  
먼저 생각해봐야 될것은 OAuth에는 3개의 참여자가 등장하는데  
각각이 누구인지를 살펴봐야 한다.  

- `나의 서비스`
- (나의 서비스를 이용하는)`사용자`
- 사용자의 자원을 가지고 있는 `외부 서비스`(구글, 페이스북, 트위터등)

예를들면 사용자가 나의 서비스에 접속해서 외부 서비스 에 글을 쓰거나 또는 저장되어 있는 글을 보기 위해서  
나의 서비스는 외부 서비스에 있는 사용자 정보에 접근 할 수 있도록 사용자로부터 허가를 받아야 한다.  

가장 쉬운 방법은 사용자의 외부 서비스의 아이디와 패스워드를  
사용자로 부터 전달받아서 우리가 사용자의 외부 서비스의 아이디와 패스워드를 저장하고 있다가  
실제로 외부 서비스에 접속할 때 아이디와 비밀번호를 이용하는것이다.  

이것은 아주 간단하고 외부 서비스에 저장되어 있는 사용자의  
모든 기능을 다 사용 할 수 있기 때문에 아주 강력한 방법이다.  

하지만 이것은 상당히 위험하다.  
사용자 입장에서는 자신의 외부 서비스의 아이디와 비밀번호를 처음 보는 서비스에 맡겨야 하는것이다.  
그리고 대부분의 사용자들은 동일한 아이디와 비밀번호를 다른 서비스를 사용하기 때문에 굉장히 위험성이 크다.  

나의 서비스 입장에서도 좋은 방법이 아니다.  
왜냐하면 사용자의 생명줄과 같은 아이디와 비밀번호를 나의 서비스에서 맡고 있기 때문에  
이것이 유실 됬을 때 겪을 수 있는 다양한 책임 문제가 있다.  

외부 서비스 입장 에서도 자신들의 사용자의 아이디와 비밀번호를  
신뢰할 수 없는 제3자가 가지고 있는 것은 매우 불만족스러운 상황이 될것이다.  

바로 이러한 상황에서 우리를 구원해줄것이 `OAuth` 이다.  

OAuth를 이용하면 훨씬 더 안정되게  
내가 만든 서비스에서 외부 서비스에 있는 사용자의 정보와 상호 작용 할 수 있게 해준다.  

OAuth를 적용하면 다음과 같이 동작한다.  

외부 서비스에서 사용자의 아이디 비밀번호 대신해서 `accessToken`을 발급해 준다.  
그리고 accessToken은  
첫 번째, 외부 서비스에 아이디와 비밀번호가 아니다 라는 장점이 있다.  
두 번째, 외부 서비스가 가지고 있는 모든 기능이 아니라  
나의 서비스에서 필요한 필수 적인 기능만 부분적으로 허용하는 비밀번호이다.  

그래서 외부 서비스에서 제공한 accessToken을 나의 서비스에서 OAuth를 통해서 획득한 다음에  
그 accessToken을 통해서 외부 서비스에서 제공하는API를 통해서 사용자의 데이터를 CRUD할 수 있게 해준다.  

이것을 가능하게 해준것은 OAuth이고 OAuth을 통해 accessToken을 얻어 낼 수 있다.  

OAuth에 이러한 특징을 이용하면 사용자들의 아이디와 비밀번호를 처음부터 보관하지 않고  
사용자를 식별할 수 있는 기능을 구현 할 수 있다.  

이러한 것을 `federeated Identity` 하는데 이것의 기반에 있는 기술이 OAuth 이다.  

<br />

## 참고자료

- [생활코딩 - oAuth 2.0](https://opentutorials.org/module/3668)
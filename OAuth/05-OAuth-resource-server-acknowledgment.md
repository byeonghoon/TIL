# 05-OAuth-resource-server-acknowledgment

Resource Server가 승인을 하기 위해서 바로 accessToken을 발급하지 않고 절차가 하나 더 있다.  

바로 임시 비밀번호 `Authorization Code`이다.  

Authorization Code를 Resource Server는 Resource Owner에게 응답한다.(이전에 scope 허가 하는 요청에 대한 응답이다.)  
다음과 같은 주소로 (Location: https://client/callback?code=3)  

여기 보면 `Location`이라는 것이 있는데 응답할 때 헤더에 Location을 주면 redirect가 된다.  

의미를 해석해 보면 Resource Server가 Resource Owner에 웹 브라우저에게  
https://client/callback?code=3이라는 주소로 이동하라고 명령을 한 것이다.  

그럼 Resource Owner에 웹 브라우저는 Resource Owner에 사용자가 인식하지도 못하게 은밀하게  
https://client/callback?code=3 주소로 Client에 접속한다.  

그럼 Client는 접속한 주소를 보고 Resource Owner에 `Authorization Code`인 3을 알게된다.  

여기 까지가 accessToken을 발급하기 전 까지의 단계이다.  

그 다음으로 Client는 Resource Owner를 통하지 않고  
다음과 같은 URL로 Resource Server에 바로 접속을 한다.  

```
https://resource.server/token?grant_type=authorization_code&code=3&redirect_url=https://client/callback&client_id=1&client_secret=2
```

그럼 Resource Server는 전송받은  
authorization_code, redirect_url, client_id, client_secret가  
자기가 가지고 있는 것과 완전히 일치하는지 확인하고 그 때 다음 단게로 진행한다.  

<br />

## 참고자료

- [생활코딩 - oAuth 2.0](https://opentutorials.org/module/3668)
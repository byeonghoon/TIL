> [programmers](https://programmers.co.kr/learn/challenges) 참조


# Algorithm

## Programmers - level1 - 문자열 다루기 기본

## 문제 설명
문자열 s의 길이가 4혹은 6이고, 숫자로만 구성되있는지 확인해주는 함수, solution을 완성하세요.

예를들어 s가 a234이면 False를 리턴하고 1234라면 True를 리턴하면 됩니다.

### 재한사항
s는 길이 1 이상, 길이 8 이하인 문자열입니다.

### 입출력 예

|  s  | return |
| ---- | ---- |
| a234 | false |
| 12343 | true |

### 구현

- c#

  ```c#
  class Solution
  {
    public static bool test(string s)
    {
      try
      {
        Convert.ToInt32(s);
        return s.Length == 4 || s.Length == 6;
      }
      catch
      {
        return false;
      }
    }
  }

  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine(Solution.test("21f3")); // false
      Console.WriteLine(Solution.test("2223")); // true
      Console.WriteLine(Solution.test("222")); // false
      Console.WriteLine(Solution.test("qwert3")); // false
      Console.WriteLine(Solution.test("222322")); // true
    }
  }
  ```

- Typescript

  ```Typescript
  function solution(s: string): boolean {
    const strLength: number = s.length;
    let result: boolean = false;

    if (strLength === 4 || strLength === 6) {
      for (let i = 0; i < strLength; i++) {
        if (Number.isNaN(parseInt(s[i], 10))) {
          return result;
        }
      }

      result = true;
    }

    return result;
  }

  console.log(solution('21f3')); // false
  console.log(solution('2223')); // true
  console.log(solution('222')); // false
  console.log(solution('qwert3')); // false
  console.log(solution('222322')); // true
  ```

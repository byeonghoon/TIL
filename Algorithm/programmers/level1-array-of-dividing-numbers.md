> [programmers](https://programmers.co.kr/learn/challenges) 참조


# Algorithm

## Programmers - level1 - 나누어 떨어지는 숫자 배열

## 문제 설명
array의 각 element 중 divisor로 나누어 떨어지는 값을 오름차순으로 정렬한 배열을 반환하는 함수, solution을 작성해주세요.
divisor로 나누어 떨어지는 element가 하나도 없다면 배열에 -1을 담아 반환하세요..

### 재한사항
- arr은 자연수를 담은 배열입니다.
- 정수 i, j에 대해 i ≠ j 이면 arr[i] ≠ arr[j] 입니다.
- divisor는 자연수입니다.
- array는 길이 1 이상인 배열입니다.

### 입출력 예 설명

- 입출력 예제1: arr의 원소 중 5로 나누어 떨어지는 원소는 5와 10입니다. 따라서 [5, 10]을 리턴합니다.

- 입출력 예제2: arr의 모든 원소는 1으로 나누어 떨어집니다. 원소를 오름차순으로 정렬해 [1, 2, 3, 36]을 리턴합니다.

- 입출력 예제3: 3, 2, 6은 10으로 나누어 떨어지지 않습니다. 나누어 떨어지는 원소가 없으므로 [-1]을 리턴합니다.


### 구현

- c#

  ```c#
  class Solution
  {
    public static int[] solution(int[] arr, int divisor)
    {
      List<int> result = new List<int>();
      
      foreach (int item in arr)
      {
        if (item % divisor == 0)
        {
          result.Add(item);
        }
      }

      if (result.Count == 0)
      {
        result.Add(-1);
      }

      result.Sort();
      
      return result.ToArray();
    }
  }

  class Program
  {
    static void Main(string[] args)
    {
      int[] arr = {5, 9, 7, 10};
      Console.WriteLine(Solution.solution(arr, 5).Length);
      int[] arr2 = {2, 36, 1, 3};
      Console.WriteLine(Solution.solution(arr2, 1).Length);
      int[] arr3 = {3, 2, 6};
      Console.WriteLine(Solution.solution(arr3, 10).Length);
    }
  }
  ```

- Typescript

  ```Typescript
  function solution(arr: Array<number>, divisor: number): Array<number> {
    const result: Array<number> = [];
    for (const item of arr) {
      if (Number.isInteger(item / divisor)) {
        result.push(item);
      }
      // if (item / divisor === Math.round(item / divisor)) {
      //   result.push(item);
      // }
      // if (item % divisor === 0) {
      //   result.push(item);
      // }
    }
    return (result.length === 0) ? [-1] : result.sort((a, b) => a - b);
  }

  let result = solution([5, 9, 7, 10], 5);
  console.log(result);
  result = solution([2, 36, 1, 3], 1);
  console.log(result);
  result = solution([3, 2, 6], 10);
  console.log(result);
  ```

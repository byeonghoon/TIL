> [programmers](https://programmers.co.kr/learn/challenges) 참조


# Algorithm

## Programmers - level1 - 가운데 글자 가져오기

## 문제 설명
단어 s의 가운데 글자를 반환하는 함수, solution을 만들어 보세요.
단어의 길이가 짝수라면 가운데 두글자를 반환하면 됩니다.

### 재한사항
s는 길이가 1 이상, 100이하인 스트링입니다.

### 구현

- c#

  ```c#
  class Solution
  {
    public static string solution(string s)
    {
      int sLength = s.Length;
      return sLength % 2 == 0 ? s.Substring(sLength / 2 - 1, 2) : s.Substring(sLength / 2, 1);
     }
  }

  class Program
  {
    static void Main(string[] args)
    {
      string result = Solution.solution("abcdef");
      Console.WriteLine(result); // cd

      string result2 = Solution.solution("abcde");
      Console.WriteLine(result2); // c
    }
  }
  ```

- Typescript

  ```Typescript
  function solution(s: string): string {
    const sLength: number = s.length;
    return ((sLength % 2) === 0) ? s.substr((sLength / 2) - 1, 2) : s[Math.trunc(sLength / 2)];
  }

  const result = solution("abcdef");
  console.log(result); // cd

  const result2 = solution("abcde");
  console.log(result2); // c
  ```

<br />

### 메모

- Typescript:
    - `String.substr(from(include)?, length?);`
        ```Typescript
        const testStr = "abcde";
        console.log(testStr.substr(0)); // abcde
        console.log(testStr.substr(2)); // cde
        console.log(testStr.substr(0, 2)); // ab
        console.log(testStr.substr(0, 6)); // abcde
        console.log(testStr.substr(2, 1)); // c
        ```

    - `String.slice(start(include)?, end(exclude)?);`
        ```Typescript
        const testStr = "abcde";
        console.log(testStr.slice()); // abcde
        console.log(testStr.slice(0)); // abcde
        console.log(testStr.slice(1)); // bcde
        console.log(testStr.slice(1, 3)); // bc
        ```

- c#:
    - `String.Substring(from, length?)`
        ```C#
        string testStr = "abcde";
        // Console.WriteLine(testStr.Substring()); // error
        Console.WriteLine(testStr.Substring(0)); // abcde
        Console.WriteLine(testStr.Substring(0, 2)); // ab
        // Console.WriteLine(testStr.Substring(0, 7)); // error
        Console.WriteLine(testStr.Substring(0, 5)); // ab
        Console.WriteLine(testStr.Substring(2, 2)); // cd
        // Console.WriteLine(testStr.Substring(2, 4)); // error
        ```

    - 자동 형 변환

        ```c#
        public static string solution(string s)
        {
            int sLength = s.Length;
            return sLength % 2 == 0 ? s.Substring(sLength / 2 - 1, 2) : s.Substring(sLength / 2, 1);
        }

        // 여기서 sLength가 홀수 인경우, sLength / 2는 소수 값이 나온다.
        // 그런데 s.Substring(sLength / 2, 1)이 가능한 이유는
        // Substring에서 sLength / 2를 정수형으로 자동 형 변환되서 소수점을 버리기 때문이다.
        ```

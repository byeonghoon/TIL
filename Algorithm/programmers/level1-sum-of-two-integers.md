> [programmers](https://programmers.co.kr/learn/challenges) 참조


# Algorithm

## Programmers - level1 - 두 정수 사이의 합

## 문제 설명
두 정수 a, b가 주어졌을 때 a와 b 사이에 속한 모든 정수의 합을 리턴하는 함수, solution을 완성하세요. 

예를 들어 a = 3, b = 5인 경우, 3 + 4 + 5 = 12이므로 12를 리턴합니다.

### 재한사항
- a와 b가 같은 경우는 둘 중 아무 수나 리턴하세요.
- a와 b는 -10,000,000 이상 10,000,000 이하인 정수입니다.
- a와 b의 대소관계는 정해져있지 않습니다

### 입출력 예

|  a  |  b | return |
| ---- | ---- | ---- |
| 3 | 5 | 12 |
| 3 | 3 | 3 |
| 5 | 3 | 12 |

### 구현

- c#

  ```c#
  public class Solution {
    public static long test(int a, int b) {
      if (a == b)
      {
        return a;
      }

      long result = 0;
      int current = a < b ? a : b;
      int final = a < b ? b : a;
      
      while (true)
      {
        if (current > final)
        {
          break;
        }

        result = result + current;
        current++;
      }

      return result;
    }
  }

  class Program
  {
    static void Main(string[] args)
    {
      Console.Write(Solution.test(3, 5)); // 12
      Console.Write(Solution.test(3, 3)); // 3
      Console.Write(Solution.test(5, 3)); // 12
    }
  }
  ```

- Typescript

  ```Typescript
  function solution(a: number, b: number): number {
    if (a === b) {
      return a;
    }

    let result: number = 0;
    let current: number = a < b ? a : b;
    const final: number = a < b ? b : a;

    while (true) {
      if (current > final) {
        break;
      }

      result = result + current;
      current++;
    }

    return result;
  }

  console.log(solution(3, 5)); // 12
  console.log(solution(3, 3)); // 3
  console.log(solution(5, 3)); // 12
  ```

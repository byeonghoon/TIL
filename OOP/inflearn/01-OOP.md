# 객체 지향 프로그래밍 입문 - 들어가며

## 목차

- 개요
- 내용
- 선수 지식
- 들어가며
  - 비용에 대한 설명
  - 비용과 변화
  - 객체 지향과 비용
- 참고 자료

<br />

## 개요

본 문서는 `인프런 - "객체 지향 프로그랭 입문" 동영상`을 학습하고 정리한 문서입니다.

<br />

## 내용

객체 지향 프로그랭 입문에서 다룰 내용은 다음과 같다.

- 비용
- 객체
- 캡슐화
- 다형성과 추상화
- 상속보단 조립
- 기능과 책임 분리
- 의존과 DI

<br />

## 선수 지식

적어도 아래 지식정도는 있어야 한다.

- private, public
- 클래스, 추상 클래스, 인터페이스
- 추상 메서드
- 상속
- 오버라이딩

<br />

## 들어가며

### 비용에 대해서 설명

실제로 제품에 추가되는 기능은 얼마 없는데  
코드 한줄 만드는데 비용은
첫번째 출시 비용과 여덜번째 출시 비용간 차이가 엄청나게 크다.


예를 들어 설명...

주요 원인은 `코드 분석 시간 증가`, `코드 변경 시간 증가`

소프트웨어를 유지하는것은 그것을 이전과 같이 동작하게 하는것이 아니다.
변화하는 셰게에 맞추어 유용하게 하는 것이다.

### 비용과 변화

낮은 비용으로 변화할 수 있어야함
이를 위한 해결 방법으로는 아래와 같이 다양한 방법이 있다.

- 패러다임 
  - 객체 지향, 함수형, 리액티브, ...
- 코드, 설계, 아키텍쳐
  - DRY, TDD, SOLID, DDD, ...
  - 클린 아키텍처, MSA, ...
- 업무 프로세스/문화
  - 애자일, DevOps , ...

### 객체 지향과 비용

객체 지향이 비용을 낮춰주는 방법은 `캡슐화 + 다형성(추상화)`이다.

<br />

## 참고 자료

- [인프런-객체-지향-프로그래밍-입문](https://www.inflearn.com/course/객체-지향-프로그래밍-입문)

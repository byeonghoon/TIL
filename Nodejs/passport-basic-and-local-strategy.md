# Passport-Basic-and-Local-Strategy

Simple, unobtrusive authentication for Node.js

Passport is authentication middleware for Node.js.  
Extremely flexible and modular,  
Passport can be unobtrusively dropped in to any Express-based web application.  
A comprehensive set of strategies support authentication using a username and password, Facebook, Twitter, and more.

<br />

## Passport.js 설치

express-session 프로젝트에서 passport 라이브러리를 적용하면서 실습할 것이다.  

```bash
git clone https://github.com/web-n/express-session-auth.git nodejs-express-passport

cd nodejs-express-passport

npm i passport
```

`passport`는 `Stratgy`라는 것을 이용해서 약 300여가지의 인증방법을 제공한다.  

우리는 아이디와 비밀번호를 통해서 인증을 하는 로컬 방식의 Stratgy를 사용할 것이다.  
해당 Stratgy를 사용 위해서 `passport-local`모듈을 설치해야한다.  

```bash
npm i passport-local
```

<br />

## Passport.js 인증 구현

로그인 폼에서 로그인을 요청하는 라우터를 passport 방식으로 변경해야 한다.  

기존 로그인 요청을 처리하는 라우터 코드(`{Project}/routes/auth.js`)를 주석 처리한다.  

```javascript
// ~/{Project}/routes/auth.js

...

// router.post('/login_process', function (request, response) {
//   const post = request.body;
//   const email = post.email;
//   const password = post.pwd;
//   if (email === authData.email && password === authData.password) {
//     request.session.is_logined = true;
//     request.session.nickname = authData.nickname;
//     request.session.save(function () {
//       response.redirect(`/`);
//     });
//   } else {
//     response.send('Who?');
//   }
// });

...
```

그리고 다음과 같이 코드(`{Project}/main.js`)를 수정한다.  

```javascript
// ~/{Project}/main.js

...
const FileStore = require('session-file-store')(session);
const passport = require('passport'); // 추가
const LocalStrategy = require('passport-local').Strategy; // 추가

app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(compression());
app.use(session({
  secret: 'asadlfkj!@#!@#dfgasdg',
  resave: false,
  saveUninitialized: true,
  store: new FileStore()
}));

// 추가
app.post('/auth//login_process', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/auth/login'
  })
);

...
```

다음 코드를 해석해 보면  

```javascript
app.post('/auth/login_process', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/auth/login'
  })
);
```

로그인 요청이 `/auth/login_process`로 들어오면 passport에서 제공하는 특정 함수가 호출되도록 구현되어 있다.  

첫번째 인자로는 Strategy가 들어가는데 여기서는 `local`이 들어있다.  
아이디와 비밀번호를 사용하는 전략을 설정한것이다.  

두 번째 인자로 들어간 것은 passport에서 제공한 local전략을 통해서 `인증에 성공/실패 여부에 따라 redirect`를 설정한 것이다.  
인증에 성공했을 때 `/`, 인증에 실패했을 때 `/auth/login` 로 redirect 시키도록 설정하였다.  

위의 설정을 커스텀 하게 설정할 때는 다음과 같이 할 수도 있다.  

passport에서는 요청에 대한 인증을 하기 위해서 다음과 같은 코드(`passport.authenticate(strategy)`)를 사용한다.  
인자(strategy)는 passport에서 제공하는 strategy중 1개를 선택해서 사용하면 된다.  

```javascript
app.post('/login',
  passport.authenticate('local'),
  function(req, res) {
    // If this function gets called, authentication was successful.
    // `req.user` contains the authenticated user.
    res.redirect('/users/' + req.user.username);
  });

```

기본적으로 인증에 실패하면 `401` 상태 코드를 응답한다.  
그리고 인증 실패시 다음 인자인 `라우터 핸들러`는 동작하지 않는다.  
만약 인증에 성공하면 다음 인자인 `라우터 핸들러`는 동작한다.  

여기까지 내용은 [passportjs.org - authenticate](http://www.passportjs.org/docs/)의 있는 설명이다.  

그런데 이해가 되지 않는 부분이 있다.  
기존 로그인 요청 코드(아래 코드)에서는 아이디와 비밀번호를 검증했는데

```javascript
if (email === authData.email && password === authData.password) {
    request.session.is_logined = true;
    request.session.nickname = authData.nickname;
    request.session.save(function () {
      response.redirect(`/`);
    });
  } else {
    response.send('Who?');
  }
```

지금 작성한 passport가 추가된 로그인 요청 라우터에서 사용한 인증 방법은 단순히 아래 있는 코드와 같다.  

```javascript
app.post('/auth//login_process', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/auth/login'
  })
);
```

어떻게 로그인을 실패했는지, 성공했는지를 판가름하느냐??는 현재 알 수가 없다.  

<br />

## Passport.js 자격확인

우리는 라우터에서 Strategy를 Local로 사용했는데, Local Strategy가 어떻게 동작할지는 정의를 하지 았았다.  
Passport에서는 Strategy를 사용하고 그리고 그 Strategy가 어떻게 동작할지를 정의해서 사용해야 한다.  
Strategy는 라우터에서 사용하기 전에 미리 어떻게 동작될지 구현되어 있어야 한다.  
Local Strategy를 설정하는 방법은 다음과 같다.  

```javascript
// ~/{Project}/main.js
...

passport.use(new LocalStrategy(function(username, password, done) {
    console.log('LocalStrategy', username, password);
}));

app.post('/auth/login_process', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/auth/login'
  })
);

...
```

위 코드를보면 Local Strategy를 설정하였고, 라우터에서는 설정한 Local Strategy를 사용하였다.  

요청이 들어오면 다음과 같은 순서로 진행된다.  
`/auth/login_process`로 요청이 들어오면 `passport.authenticate('local', ...)`가 실행된다.  
그러면 설정되어 있는 Local Strategy 미들웨어(`passport.use(new LocalStrategy(...)`)가 실행된다.  
하지만 실행을 시켜보면 로그가 찍히지 않는것을 확인할 수 있다.  
그 이유는 Passport는 기본적으로 인증 파라미터로 name 필드에서 `username`, `password` 를 찾는다.  
만약 화면에서 name 필드에 다른 이름을 사용할려면 Strategy 미들웨어의 옵션 파라미터를 사용해야 한다.  

예를들면 다음과 같다.  

name 필드를 email, passwd를 사용하는 경우  

```html
<form action="/login" method="post">
    <div>
        <label>Username:</label>
        <input type="text" name="email"/>
    </div>
    <div>
        <label>Password:</label>
        <input type="password" name="passwd"/>
    </div>
    <div>
        <input type="submit" value="Log In"/>
    </div>
</form>
```

```javascript
passport.use(new LocalStrategy({usernameField: 'email', passwordField: 'passwd'},
  function(username, password, done) {
    // ...
  }
));
```

name 필드를 username, password로 사용하는 경우  

```html
<form action="/login" method="post">
    <div>
        <label>Username:</label>
        <input type="text" name="username"/>
    </div>
    <div>
        <label>Password:</label>
        <input type="password" name="password"/>
    </div>
    <div>
        <input type="submit" value="Log In"/>
    </div>
</form>
```

```javascript
// 이때는 default 설정을 사용하끼 때문에, 옵셔널 인자를 추가할 필요가 없다.  
passport.use(new LocalStrategy(function(username, password, done) {
    // ...
  }
));
```

현재 구현되어 있는 로그인 화면을 보면 name 필드를 email, pwd로 사용하고 있다.  

```javascript
// ~/{Project}/routes/auth.js

router.get('/login', function (request, response) {
  const title = 'WEB - login';
  const list = template.list(request.list);
  const html = template.HTML(title, list, `
    <form action="/auth/login_process" method="post">
      <p><input type="text" name="email" placeholder="email"></p>
      <p><input type="password" name="pwd" placeholder="password"></p>
      <p>
        <input type="submit" value="login">
      </p>
    </form>
  `, '');
  response.send(html);
});
```

Local Strategy 미들웨어에서 처리하기 위해서는 옵셔널 인자를 사용 해야한다.  

```javascript
// ~/{Project}/main.js
...

app.use(session({
  secret: 'asadlfkj!@#!@#dfgasdg',
  resave: false,
  saveUninitialized: true,
  store: new FileStore()
}));

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'pwd'
}, function (username, password, done) {
  console.log(username, password);
}));

app.post('/auth/login_process', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/auth/login'
  })
);

...
```

실행을 시켜보면 로그가 정상적으로 찍히는것을 확인 할 수 있다.  

{Project}/routes/auth.js에서 다음 코드를 복사 후 삭제하자

```javascript
// ~/{Project}/routes/auth.js

const authData = {
  email: 'egoing777@gmail.com',
  password: '111111',
  nickname: 'egoing'
};
```

그리고 복사한 코드를 {Project}/main.js에 붙여넣고, {Project}/main.js를 수정한다.  

```javascript
// ~/{Project}/main.js

...

const authData = {
  email: 'egoing777@gmail.com',
  password: '111111',
  nickname: 'egoing'
};

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'pwd'
}, function (username, password, done) {
  if (username === authData.email) {
    if (password === authData.password) {
      return done(null, authData);
    } else {
      return done(null, false, { message: 'Incorrect password.' });
    }
  } else {
    return done(null, false, { message: 'Incorrect username.' });
  }
}));

...
```

여기서 이해해야 되는 부분은 `done` 함수이다.  
passport 문서에서는 done 함수를 `Verify Callback` 이라고 정의되어 있다.  

다음 내용을 보면서 `done` 함수를 이해해 보자.  

This example introduces an important concept.  
"Strategies" require what is known as a verify callback.  

The purpose of a "verify callback" is to find the user that possesses a set of credentials.  

When "Passport" authenticates a request, it parses the credentials contained in the request.  

It then invokes the "verify callback" with those credentials as arguments, in this case username and password.  

If the credentials are valid,  
the "verify callback" invokes "done" to supply "Passport" with the user that authenticated.  

```javascript
// 로그인이 성공한 경우, false가 아닌 사용자의 정보를 넘긴다.
return done(null, user);
```

```javascript
// 로그인이 성공한 경우, false가 아닌 사용자의 정보와 추가적으로 메시지를 넘길 수 있다.
return done(null, user, { message: 'success.' });
```

If the credentials are not valid (for example, if the password is incorrect),  
"done" should be invoked with `false` instead of a user to indicate an authentication failure.  

```javascript
// 로그인이 실패한 경우, false를 넘긴다.
return done(null, false);
```

An additional info message can be supplied to indicate the reason for the failure.  
This is useful for displaying a "flash message" prompting the user to try again.  

```javascript
// 로그인이 실패한 경우, false를 넘기고 추가적으로 사용자에게 메시지를 넘길 수 있다.
return done(null, false, { message: 'Incorrect password.' });
```

Finally, if an exception occurred while verifying the credentials (for example, if the database is not available),
"done" should be invoked with an error, in conventional Node style.

```javascript
// 인증 과정에서 서버에서 발생한 에러인 경우, 첫 번째 인자에 null이 아닌 err를 넘긴다.
return done(err);
```

Note that it is important to distinguish the two failure cases that can occur.  
The latter is a server exception, in which `err` is set to a non-`null` value.  
Authentication failures are natural conditions, in which the server is operating normally.  
Ensure that `err` remains null, and use the final argument to pass additional details.  

By delegating in this manner,  
the "verify callback" keeps Passport database agnostic.  

Applications are free to choose how user information is stored,  
without any assumptions imposed by the authentication layer.  

여기까지 [passportjs.org - Verify Callback](http://www.passportjs.org/docs/downloads/html/)에 정리되어 있는 내용이다.  

<br />

## Passport.js 세션이용

작성한 서버를 실행하고 정상적으로 로그인을 하면 다음과 같은 에러가 발생한다.  

```bash
Error: passport.initialize() middleware not in use
```

Express 프레임워크에서 Passport를 사용하기 위해서는 Express 미들웨어에 `passport.initialize()`을 등록해야 한다.  

추가적으로 passport에서 session을 활용하기 위해서는  

애플리케이션에서 session을 사용하면 Express session 미들웨어에 아래에 `passport.session()`을 반드시 등록해야 한다.  
=> `passport.session()` 미들웨어는 Express session미들웨어를 활용해서 그 위에서 동작하기 때문에, 먼저 정의가 되어 있어야 한다.  

{Project}/main.js에서 passport.initialize() 와 passport.session()를 express-session 미들웨어 아래에 등록한다.  

```javascript
// ~/{Project}/main.js

...

app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(compression());
app.use(session({
  secret: 'asadlfkj!@#!@#dfgasdg',
  resave: false,
  saveUninitialized: true,
  store: new FileStore()
}));
app.use(passport.initialize());
app.use(passport.session());

...
```

서버를 실행하고 정상적으로 로그인을 하면 다음과 같은 에러가 발생한다.  

```bash
Error: Failed to serialize user into session
```

로그인은 성공했는데, 사용자에 대한 session에 대한 세팅이 되어 있지 않다는 에러이다.  
passport가 내부적으로 session을 이용 할 때는 다음과 같은 방법을 사용한다.  

In a typical web application,  
the credentials used to authenticate a user will only be transmitted during the login request.  
사용자를 인증하는데 사용하는 자격 증명은 로그인 요청동안 전송되어진다.

If authentication succeeds, a session will be established and maintained via a cookie set in the user's browser.
만약 인증을 성공하였으면, 브라우저의 쿠키를 통하여 세션은 만들어지고 유지되진다.  

Each subsequent request will not contain credentials, but rather the unique cookie that identifies the session.  
후속 요청마다 자격 증명이 포함되지 않고 세션을 식별하는 고유 쿠키가 포함됩니다.

In order to support login sessions, Passport will `serialize` and `deserialize` user instances to and from the session.  

```javascript
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});
```

In this example,  
only the user ID is serialized to the session,  
keeping the amount of data stored within the session small.  

When subsequent requests are received, this ID is used to find the user, which will be restored to req.user.

The serialization and deserialization logic is supplied by the application,  
allowing the application to choose an appropriate database and/or object mapper, without imposition by the authentication layer.

serializeUser, deserializeUser 이 코드들은 세션을 처리하는 코드인 것이다.  

로그인을 성공 했을 때, `passport.serializeUser(..)`에 있는 콜백함수가 호출되기로 약속되어 있다.  

여기까지 [passportj.org - Sessions](http://www.passportjs.org/docs/downloads/html/))에 있는 내용이다.  

아래 코드에서 `done(null, authData);`부분이 실행됬을 때, `passport.serializeUser(..)`가 실행된다.  

```javascript
passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'pwd'
}, function (username, password, done) {
  if (username === authData.email) {
    if (password === authData.password) {
      return done(null, authData);
    } else {
      return done(null, false, { message: 'Incorrect password.' });
    }
  } else {
    return done(null, false, { message: 'Incorrect username.' });
  }
}));
```

`passport.serializeUser()`와 `passport.deserializeUser()`가 언제 호출되는지 로그를 통해서 확인을 해보자.

```javascript
// ~/{Project}/main.js

...

app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
  console.log('serializeUser', user);
});

passport.deserializeUser(function(id, done) {
  console.log('deserializeUser', id);
});

...
```

로그인을 성공했을 때, `passport.serializeUser()`가 호출되는 것을 확인할 수 있다.  
그리고 그 때 `passport.serializeUser()`의 콜백 메서드의 첫 번째 인자(user)로  
`passport.use(new LocalStrategy({...`에서 로그인이 성공하였을 때 `done(null, authData);`을 호출하였는데  
done의 두 번째 인자 값(authData)가 passport.serializeUser의 콜백 메서드의 첫 번째 인자(user)로 주입 되는 것을 확인 할 수 있다.  

그런데 예제 코드를 보면 다음과 같이 되어 있다.  

```javascript
passport.serializeUser(function(user, done) {
  done(null, user.id);
});
```

`passport.serializeUser(function(user, done) {});`에 있는 `done`을 보면  
첫 번째 인자로 null을 주고  
두 번째 인자는 해단 user정보를 식별할 수 있는 식별자를 주도록 구성되어 있다.  
이렇게 식별자를 주는것은 passport의 약속이기 때문에 그냥 따르면 된다.  

그런데 우리의 식별자는 `email`이다.  
그래서 우리는 코드를 다음과 같이 수정한다.  

```javascript
// ~/{Project}/main.js

...

app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
  console.log('serializeUser', user);
  done(null, user.email);
});

passport.deserializeUser(function(id, done) {
  console.log('deserializeUser', id);
});
...
```

로그인을 성공했을 때, 동작을 확인을 해보자  
다음과 같이 세션 정보안에 passport의 user정보로 사용자의 식별자 정보가 추가된 것을 확인 할 수 있다.  

로그인 전

```json
{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"__lastAccess":1534398931786}
```

로그인 후

```json
{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"__lastAccess":1534398955508,"passport":{"user":"egoing777@gmail.com"}}
```

serializeUser 메서드에 콜백 메서드에서 done의 두 번째 인자(`user.email`)로 전달한 식별자 값이  
`"passport":{"user":"egoing777@gmail.com"}`의 user 키의 값으로 들어간 것을 확인 할 수 있다.

`즉 serializeUser는 로그인이 성공하였을 때, 세션정보를 세션 스토어에 저장하는 역할을 한다.`  
그래서 `로그인에 성공했을 때마다 serializeUser가 딱 한번 호출된다.`  

그리고 `passport.deserializeUser(function(id, done) {...`가 호출된 것을 확인 할 수 있다.  

`passport.deserializeUser(function(id, done) {...`는 페이지에 방문할 때 마다(서버에 요청이 있을 때마다) 실행된다.  
사용자의 실제 정보를 조회해서 가지고 오는 동작을 한다.  

deserializeUser는 사용자가 전송한 토큰을 통해서
식별자 정보(email)를 가지고 오고  
그 식별자 정보를 통해서 사용자의 정보를 조회하고  
그 사용자의 정보를 `done`에 전달한다.  

done에 첫 번째 인자로 null을 주고, 두 번째 인자는 user 정보를 주도록 구성되어 있다.  
이렇게 user 정보를 주는 것도 passport의 약속이기 때문에 그냥 따르면 된다.  

우리 코드를 수정한다.  

```javascript
// ~/{Project}/main.js

...

passport.deserializeUser(function(id, done) {
  console.log('deserializeUser', id);
  done(null, authData);
});
...
```

페이지에 들어갈 때 마다 deserializeUser가 호출 되는 것을 확인 할 수 있다.  

사용자가 로그인에 성공한 다음에 각각에 페이지에 접속할 때,  
`그 사용자가 로그인에 성공한 사용자인지 아닌지를 체크해야 하는데`  
그것을 체크할 때 passport는 deserializeUser 메서드를 호출한다.  
그래서 페이지를 리로드 할 때마다 deserializeUser 메서드가 호출되어진다.  

다시 정리하면  
`serializeUser는 로그인이 성공하였을 때 딱 한번 호출되고, 사용자의 식별자를 세션 스토어에 저장한다.`  
`deserializeUser는 세션 스토어에 저장된 데이터를 기준으로 해서 식별자를 통해 우리가 필요한 정보를 조회 하고 인증된 사용자인지를 체크한다.`  

passport를 통해서 로그인에 성공하고 세션 스토어에 저장까지 하였다.  

그럼 passport는 어떻게 로그인이 되었는지, 안되었는지를 확인할까?  
다음 단원에서 확인한다.  

<br />

## passport.js 로그인 확인

routes/index.js에 콘솔 코드를 추가한다.  

```javascript
// ~/{Project}/routes/index.js

...

router.get('/', function (request, response) {
  console.log(request.user);

...
```

그리고 로그인을 해보면 다음과 같은 정보가 로그에 찍히는것을 확인 할 수 있다.  

```json
{
  "email": "egoing777@gmail.com",
  "password": "111111",
  "nickname": "egoing"
}
```

request 인자에 있는 user 객체는 어디서 왔을 까?  

`request.user`는 `deserializeUser` 함수 안에서 사용한 `done`함수에 있는 두 번째 인자(authData) 값 이다.  

```javascript
passport.deserializeUser(function(id, done) {
  done(null, authData);
});
```

우리가 passport를 사용하지 않으면 Express - request는 user 객체를 가지고 있지 않다.  
하지만 우리는 passport를 사용하기 때문에,  
passport는 우리가 사용 할 수 있도록 request에 user 객체를 주입해 준다.  
이것 또한 약속인 것이다.  

request에 user 값을 기준으로 사용자가 로그인을 했는지 하지 않았는지 확인을 할 수 있게 해준다.  

{Project}/lib/auth.js에서 request에 user 객체를 활용해보자.  

```javascript
// ~/{Project}/lib/auth.js

module.exports = {
  isOwner: function (request, response) {
    return !!request.user;
  },
  statusUI: function (request, response) {
    let authStatusUI = '<a href="/auth/login">login</a>';
    if (this.isOwner(request, response)) {
      authStatusUI = `${request.user.nickname} | <a href="/auth/logout">logout</a>`;
    }
    return authStatusUI;
  }
};
```

동작을 시켜보면 정상적으로 로그인이 되고, 그리고 로그인한 사용자의 계정이 보이는 것을 확인할 수 있다.  

여기까지 passport가 로그인이 되었는지 안되었는지 확인하는 작업을 하였다.  

그럼 `로그아웃`을 했을 때는 어떻게 해야될지 다음 단원에서 확인해 보자

<br />

## passport.js 로그아웃

`router.get('/logout', ...` 로그아웃 라우터의 코드를 다음과 같이 변경한다.  

```javascript
// ~/{Project}/routes/auth.js

...

router.get('/logout', function (request, response) {
  request.logout();
  response.redirect('/');
});

..
```

그리고 로그인된 상태에서 로그아웃 버튼을 클릭해 보면  

```javascript
// ~/{Project}/routes/index.js

...
router.get('/', function (request, response) {
  console.log('/', request.user);
...
```

routes/index.js에 있는 `/` 라우터에서 로그를 확인하면 다음과 같다.  

```bash
$ / undefined
```

passport가 request의 user 객체를 삭제시켜 준 것이다.  

그런데 계속 로그인을 하고 로그아웃을 할 때  
세션이 남아있어서 로그인이 된것처럼 보이는 경우(비동기 문제)가 있다.  

그렇기 떄문에 routes/auth.js에서 로그아웃 라우터를 다음과 같이 변경한다.  

```javascript
// ~/{Project}/routes/auth.js

...

router.get('/logout', function (request, response) {
  request.logout();
  
  // 현재 session의 상태를 저장
  request.session.save(function (err) {
    response.redirect('/');
  });
});
..
```

<br />

## passport.js 플래쉬 메시지

passport.js에서는 `플래쉬 메시지`라는 것을 사용 할 수 있다.  
`플래쉬 메시지`는 아이디 혹은 비밀번호가 틀렸을 때 출력 되는 1회성 메시지  

다음을 보고 이해하자.  

Redirects are often combined with flash messages in order to `display status information to the user.`  

```javascript
app.post('/login',
  passport.authenticate('local', { successRedirect: '/',
                                   failureRedirect: '/login',
                                   failureFlash: true })
);
```

Setting the failureFlash option to true instructs Passport  
to flash an error message using the message given by the strategy's verify callback,  
if any. This is often the best approach,  
because the verify callback can make the most accurate determination of why authentication failed.  

Alternatively, the flash message can be set specifically.

```javascript
passport.authenticate('local', { failureFlash: 'Invalid username or password.' });
```

A `successFlash` option is available which flashes a success message when authentication succeeds.  

```javascript
passport.authenticate('local', { successFlash: 'Welcome!' });
```

flash messages를 사용할려면 req.flash() 함수를 사용해라 그리고  
Express 3.x 이상부터는 flash messages를 Express에 적용하기 위해서는 connect-flash middleware를 사용해라  

여기까지 passport 홈페이지에서 [Flash Message](http://www.passportjs.org/docs/downloads/html/)에 대한 설명이다.  

`connect-flash` 모듈을 설치하고, 적용하자  

```bash
npm i connect-flash
```

Flash Message는 내부적으로 세션을 사용하기 때문에  
항상 session 미들웨어 아래에 Flash Message 미들웨어를 등록해야 한다.  

{Project}/main.js 코드를 수정하자.  

```javascript
// ~/{Project}/main.js

...
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const flash = require('connect-flash'); // 추가

...

app.use(session({
  secret: 'asadlfkj!@#!@#dfgasdg',
  resave: false,
  saveUninitialized: true,
  store: new FileStore()
}));
app.use(flash()); // 추가
app.use(passport.initialize());
app.use(passport.session());

...

```

이제 connect-flash 미들웨어는 요청이 들어올 때마다 내부적으로 동작한다.  
connect-flash 미들웨어는 내부적으로 request 인자에 flash() 메소드를 추가해준다.  

다음을 보고 이해하자.  

With the flash middleware in place, all requests will have a req.flash() function that can be used for flash messages.  

```javascript
app.get('/flash', function(req, res){
  // Set a flash message by passing the key, followed by the value, to req.flash().
  req.flash('info', 'Flash is back!')
  res.redirect('/');
});
 
app.get('/', function(req, res){
  // Get an array of flash messages by passing the key to req.flash()
  res.render('index', { messages: req.flash('info') });
});
```

여기까지 [npm, connect-flash](https://www.npmjs.com/package/connect-flash)에 정리되어 있는 설명이다.

{Project}/main.js 코드를 수정하자.  

```javascript
// ~/{Project}/main.js

...

app.use(flash()); // 추가

app.get('/flash', function (req, res) {
  // Set a flash message by passing the key, followed by the value, to req.flash().
  req.flash('info', 'Flash is back!');
  res.send('flash');
});

...
```

동작이 어떻게 진행되는지 확인을 해보자

`GET /flash`로 접속하면 `"flash":{"info":["Flash is back!"]` 정보가 세션에 추가된것을 확인할 수 있다.  

```json
{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"flash":{"info":["Flash is back!"]},"__lastAccess":1534461023919}
```

이 상태에서 코드를 수정해보자  

```javascript
// ~/{Project}/main.js

...

app.use(flash()); // 추가

app.get('/flash', function (req, res) {
  // Set a flash message by passing the key, followed by the value, to req.flash().
  req.flash('msg', 'Flash is back!!');
  res.send('flash');
});

...
```

그리고 다시 /flash 로 접속하면 `"flash":{"info":["Flash is back!"],"msg":["Flash is back!!"]` 정보가 세션에 추가된것을 확인할 수 있다.  

```json
{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"flash":{"info":["Flash is back!"],"msg":["Flash is back!!"]},"__lastAccess":1534461153359}
```

`req.flash(key, value)`를 통해서 세션에 flash 정보를 등록(set) 할 수 있다.  

그럼 등록한 flash 정보는 어떻게 읽을 수 있을까??  

우리 코드를 다음과 같이 수정하고, `GET /flash-message`로 접속을 해보자  

```javascript
// ~/{Project}/main.js

...

app.use(flash()); // 추가

app.get('/flash', function (req, res) {
  // Set a flash message by passing the key, followed by the value, to req.flash().
  req.flash('msg', 'Flash is back!!');
  res.send('flash');
});

app.get('flash-message', function(req, res){
  // Get an array of flash messages by passing the key to req.flash()
  const fmsg = req.flash();
  console.log(fmsg);
  res.send(fmsg);
});

...
```

화면에서는  `{ info: [ 'Flash is back!' ], msg: [ 'Flash is back!!' ] }` 정보가 출력되는것을 확인하였고  
세션에서는 flash 정보가 삭제되는것을 확인하였다.

```json
{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"flash":{},"__lastAccess":1534461501530}
```

`req.flash()` 메서드의 동작은 세션에 있는 flash에 모든 프로퍼티를 읽은 후, 삭제한다.  
(flash message는 일회성으로 사용하기 위해 만들어졌기 때문이다.)  

그럼 `req.flash(key)`는 어떻게 동작할까?

{Project}/main.js 코드를 다음과 같이 수정하고  
`GET /flash`로 접속해서 세션에 2개의 정보가 생성되는지 확인하고  
`GET /flash-message/msg`, `GET /flash-message/info`로 접속해서 화면에 출력되는 정보와 세션의 상태를 확인하자  

```javascript
// ~/{Project}/main.js

...

app.get('/flash', function (req, res) {
  req.flash('msg', 'Flash is msg!!');
  req.flash('info', 'Flash is info!!', 'Flash is info22!!', 'Flash is info33!!');
  res.send('flash');
});

app.get('/flash-message', function(req, res){
  // Get an array of flash messages by passing the key to req.flash()
  const fmsg = req.flash();
  console.log(fmsg);
  res.send(fmsg);
});

app.get('/flash-message/msg', function(req, res){
  const fmsg = req.flash('msg');
  console.log(fmsg);
  res.send(fmsg);
});

app.get('/flash-message/info', function(req, res){
  const fmsg = req.flash('info');
  console.log(fmsg);
  res.send(fmsg);
});

...
```

`GET /flash`로 접속해서 세션에 2개의 정보가 생성되는지 확인하였다.  
여기서 `req.flash(key, value, value, ...);`의 사용법도 확인하였다.  

```json
{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"flash":{"msg":["Flash is msg!!"],"info":["Flash is info!! Flash is info22!! Flash is info33!!"]},"__lastAccess":1534462258535}
```

`GET /flash-message/info`로 접속했을 때, 화면에는 다음과 같이 정보가 출력되었다.  

```bash
[ 'Flash is info!! Flash is info22!! Flash is info33!!' ]
```

세션은 다음과 같이 정보가 변경되었다.  

```json
{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"flash":{"msg":["Flash is msg!!"]},"__lastAccess":1534462363623}
```

그리고 이어서 `GET /flash-message/msg`로 접속했을 때, 화면에는 다음과 같이 정보가 출력되었다.  

```bash
[ 'Flash is msg!!' ]
```

세션은 다음과 같이 정보가 변경되었다.  

```json
{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"flash":{},"__lastAccess":1534462452136}
```

connect-flash 모듈 사용법을 정리해보면  
항상 세션 미들웨어 아래에 connect-flash 미들웨어를 적용해야 한다.  
그리고 사용법은  
`req.flash(key, value)`, `req.flash(key, value, value, ...)`를 이용해서 flash message를 세션에 저장(set)하고
`req.flash()`, `req.flash(key)`를 이용해서 flash message를 사용(get)하면서 삭제한다.  
여기까지는 connect-flash 미들웨어를 연습해보았다.  

이제 본격적으로 우리 프로젝트에 connect-flash 미들웨어를 적용해보자  

<br />

## passport.js 플래쉬 메시지 적용

기존 {Project}/main.js 작성했던 flash 관련 라우터 코드를 삭제한다.  
아래에 있는 코드들을 main.js에서 삭제한다.  

```javascript
// {Project}/main.js

app.get('/flash', function (req, res) {
  // Set a flash message by passing the key, followed by the value, to req.flash().
  req.flash('msg', 'Flash is msg!!');
  req.flash('info', 'Flash is info!!', 'Flash is info22!!', 'Flash is info33!!');
  res.send('flash');
});

app.get('/flash-message', function(req, res){
  // Get an array of flash messages by passing the key to req.flash()
  const fmsg = req.flash();
  console.log(fmsg);
  res.send(fmsg);
});

app.get('/flash-message/msg', function(req, res){
  const fmsg = req.flash('msg');
  console.log(fmsg);
  res.send(fmsg);
});

app.get('/flash-message/info', function(req, res){
  const fmsg = req.flash('info');
  console.log(fmsg);
  res.send(fmsg);
});

```

우리가 원하는 동작은 로그인에 실패를 했을 때,  
Flash Message를 통해 1회성 메시지로 사용자에게 무엇이 문제인지를 알려줄려고 한다.  

{Project}/main.js에서  
Local Strategy 미들웨어(`passport.use(new LocalStrategy({ ..`)가  
로그인 성공시 호출되는 done(verify callback)의 성공 메시지를 추가하고(`done(null, authData, {message: 'Login Success'});`)
로그인 인증 라우터에 `failureFlash: true`를 추가한다.  

```javascript
// ~/{Project}/main.js

...

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'pwd'
}, function (username, password, done) {
  if (username === authData.email) {
    if (password === authData.password) {
      return done(null, authData, {message: 'Login Success'}); // 수정
    } else {
      return done(null, false, {message: 'Incorrect password.'});
    }
  } else {
    return done(null, false, {message: 'Incorrect username.'});
  }
}));

...

app.post('/auth/login_process', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/auth/login',
    failureFlash: true // 추가
  })
);

...
```

동작을 확인해보자  
먼저 로그인시 아이디를 틀리면 세션에 다음과 같이 정보가 저장되는 것을 확인 할 수 있다.  

```json
{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"flash":{"error":["Incorrect username."]},"__lastAccess":1534463069023}
```

세션에 저장되어 있는 flash message를 사용해보자  

로그인 페이지 라우터 코드(`{Project}/route/auth.js`)를 변경하자

```javascript
// ~/{Project}/route/auth.js

...

router.get('/login', function (request, response) {
  const fmsg = request.flash();
  let feedback = '';
  if (fmsg.error) {
    feedback = fmsg.error[0];
  }
  const title = 'WEB - login';
  const list = template.list(request.list);
  const html = template.HTML(title, list, `
    <div style="color: red">${feedback}</div>
    <form action="/auth/login_process" method="post">
      <p><input type="text" name="email" placeholder="email"></p>
      <p><input type="password" name="pwd" placeholder="password"></p>
      <p>
        <input type="submit" value="login">
      </p>
    </form>
  `, '');
  response.send(html);
});

...

```

동작을 확인해보자  
아이디, 비밀번호를 순서대로 틀려보고 에러 메시지가 정상 출력되는 것을 확인하였다.  
그리고 1회성 동작을 확인하기 위해서 에러 메시지가 출력된 후 다시 리로드 했을 때, 메시지가 출력되지 않는 것을 확인하였다.  

그럼 로그인에 성공했을 때, flash message를 주고싶은 경우는 어떻게 해야할까?  
{Project}/main.js에서 로그인 인증 라우터에 `successFlash: true`를 추가한다.  

```javascript
// ~/{Project}/main.js

...

app.post('/auth/login_process', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/auth/login',
    failureFlash: true,
    successFlash: true // 추가
  })
);

...
```

그리고 로그인에 성공하면 호출되는 페이지인 index.js를 수정한다.  

```javascript
// ~/{Project}/routes/index.js

...

router.get('/', function (request, response) {
  const fmsg = request.flash();
  let feedback = '';
  if (fmsg.success) {
    feedback = fmsg.success[0];
  }

  const title = 'Welcome';
  const description = 'Hello, Node.js';
  const list = template.list(request.list);
  const html = template.HTML(title, list,
    `
      <div style="color: blue;">${feedback}</div>
      <h2>${title}</h2>${description}
      <img src="/images/hello.jpg" style="width:300px; display:block; margin-top:10px;">
      `,
    `<a href="/topic/create">create</a>`,
    auth.statusUI(request, response)
  );
  response.send(html);
});

...
```

동작을 확인해보자  
처음 로그인 시에만 로그인 성공 플래시 메시지를 보여주고 그 이후 부터는 보여 주지 않는것을 확인하였다.  

<br />

## 참고자료

- [생활코딩 > Node.js - passport.js]https://opentutorials.org/course/3402)
- [passportjs.org](http://www.passportjs.org/)

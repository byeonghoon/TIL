# Cookie

## 목차

- 쿠키의 생성
- 쿠키의 읽기
- Session Cookie vs Permanent Cookie
- 쿠키 옵션 (Secure & HttpOnly)
- 쿠키 옵션 (Path & Domain)
- 참고자료

<br />

웹 등장 이후 여러가지 불만족이 생겨났다. 그 중 하나가 `개인화`이다.

개인화는 모든 사람에게 똑같은 웹을 보여주는것이 아니라, 사람마다 그 사람에 적합한 페이지를 보여줘야 한다.

예를 들면 장바구니 페이지, 그리고 한번 로그인 후 그 다음에 웹 브라우저로 접속하면 로그인을 하지 않아도 되는 것

쿠키가 도입되면서  
웹 브라우저는 이전에 접속 했던 사용자의 정보를 웹 서버로 전송할 수 있게됬다.  
웹 서버는 이 정보를 바탕으로 현재 접속한 사용자가 누구인지 알 수 있게 됬다.  

쿠키가 무엇인지 살펴보고, 또 쿠키를 통해 할 수 있는 인증 기능을 구현해 보자.

쿠키는 `HTTP 쿠키`랑 같은 말이다.

HTTP 쿠키(웹 쿠키, 브라우저 쿠키)는  
서버가 사용자의 웹 브라우저에 전송하는 작은 데이터 조각이다.   
브라우저는 그 데이터 조각들을 저장할 수 있고 동일한 서버로 다음 요청 시 함께 전송한다.  

쿠키의 용도는 다음과 같다.

- `세션 관리`: Logins, shopping carts, game scores, ...
- `개인화`: User preferences, themes, and other settings
- `방문자들의 상태`: Recording and analyzing user behavior

쿠키는 일반적인 클라이언트 측 스토리지로도 사용된 적이 있었다.

쿠키가 클라이언트 측에 데이터를 저장하는 유일한 방법이었을 때는 적당한 방법이었지만,  
요즘에는 modern storage APIs를 선택하는 것이 권장된다.  

쿠키는 모든 요청과 함께 전송되기 때문에,  
(특히 mobile data connections에서) 성능 상의 부담이 될 수 있습니다.  

클라이언트 스토리지를 위한 Modern APIs는   
웹 스토리지 API (`localStorage`와 `sessionStorage`) 그리고 `IndexedDB`가 있다.  

Nodejs 예제를 통해서 쿠키의 생성, 사용, 활용은 예제를 보면서 이해해 보자

<br />

## 쿠키의 생성

서버에서 클라이어트로 쿠키를 전달할 때는 응답 헤더에 `Set-Cookie` 필드를 사용한다

다음과 같이  Set-Cookie 필드명에 `key=value` 형식으로 사용하면 된다.

```HTTP
HTTP/1.0 200 OK
Content-type: text/html
Set-Cookie: yummy_cookie=choco

[page content]
```

다음과 같이 `복수 개`의 Set-Cookie 필드를 전달 할 수도 있다.

```HTTP
HTTP/1.0 200 OK
Content-type: text/html
Set-Cookie: yummy_cookie=choco
Set-Cookie: tasty_cookie=strawberry

[page content]
```

클라이언트(브라우저)는 서버로 부터 응답을 받았을 때  
응답 헤더에 Set-Cookie 헤더가 있으면  
브라우저 Storage 중 하나인 Cookies에 쿠키들을 저장한다.

그리고 브라우저에서 서버로 요청을 보낼 때  
Storage - Cookies에 해당 서버 주소와 매칭되는 쿠키들이 있으면  
요청 헤더 `Cookie` 필드에 Cookies 저장소에 저장되어 있는 값들이 할당되서 서버로 전송된다.  

```HTTP
GET /sample_page.html HTTP/1.1
Host: www.example.org
Cookie: yummy_cookie=choco; tasty_cookie=strawberry
```

쿠키를 응답 헤더에 추가하는 코드를 작성해보자.

```javascript
const http = require('http');
http.createServer((req, res) => {
  res.writeHead(200, {
    'Set-Cookie': ['yummy_cookie=choco', 'tasty_cookie=strawberry']
  });
  res.end('cookie!');
}).listen(3000);
```

브라우저에서는 네트워크 탭에서 응답 헤더에서 Set-Cookie필드를 확인 하고, 쿠키 저장 여부를 확인한다.  
그리고 다음과 같이 쿠키를 응답 헤더에 추가하는 코드를 비활성화 한다.  

```javascript
const http = require('http');
http.createServer((req, res) => {
  // res.writeHead(200, {
  //  'Set-Cookie': ['yummy_cookie=choco', 'tasty_cookie=strawberry']
  // });
  res.end('cookie!');
}).listen(3000);
```

브라우저를 리로드하고 네트워크 탭에 있는 응답 헤더에서 Cookie 필드를 확인한다.  
마지막으로 Application 탭에서 Storage - Cookies 에서 저장되어 있는 쿠키들을 삭제하고 다시 리로드 한다.  
그럼 요청 해더에서 Cookie 필드가 삭제된것을 확인 할 수 있다.  

<br />

## 쿠키의 읽기

생성한 쿠키를 브라우저에서 서버로 전송 했을 때  
웹 애플리케이션(Node.js)에서 어떻게 쿠키를 읽을 수 있을까?  

요청 헤더에 Cookie가 있으면 Node.js에서는 다음과 같이 읽을 수 있다.

```javascript
const http = require('http');
http.createServer((req, res) => {
  console.log(req.headers.cookie); // 브라우저에서 전송한 쿠키 읽기
  res.writeHead(200, {
    'Set-Cookie': ['yummy_cookie=choco', 'tasty_cookie=strawberry']
  });
  res.end('cookie!');
}).listen(3000);
```

결과가 다음과 같이 문자열로 나오기 때문에  

```
“yummy_cookie=choco; tasty_cookie=strawberry”
```

문자열을 바로 사용하기 어렵기 때문에 문자열을 직접 파싱을 해서 객체로 만들어서 사용해야 한다.  
npm에서 쿠키를 파싱해주는 모듈을 사용하자.  

```bash
$ npm i cookie
```

cookie 모듈에서 제공하는 parse 메소드를 통해서 객체화된 쿠키들을 사용할 수 있다.

```javascript
const http = require('http');
const cookie = require('cookie');
http.createServer((req, res) => {
  console.log(req.headers.cookie);
  let cookies = {};
  if (req.headers.cookie !== undefined) {
    cookies = cookie.parse(req.headers.cookie);
  }
  console.log(cookies);
  console.log(cookies.yummy_cookie);
  console.log(cookies.tasty_cookie);
  res.writeHead(200, {
    'Set-Cookie': ['yummy_cookie=choco', 'tasty_cookie=strawberry']
  });
  res.end('cookie!');
}).listen(3000);

```

<br />

# Session Cookie vs Permanent Cookie

Session Cookie는 웹 브라우저가 켜져있는 동안에만 유효 하다.  
Permanent Cookie는 웹 브라우저를 껏다 켜도 유효하다.  

우리가 지금껏 사용한 것은 Session Cookie이다.  

여기다가 `Expires` 나 `Max-Age` 같은 값들을 추가하면 Permanent Cookie가 된다.  

`Expires`는 쿠키가 언제 죽을것인가(`절대적인 시간`)를 의미한다.  
`Max-Age`는 쿠키가 얼마 동안 살 것인가(`현재 시점을 기준으로 상대적인 시간`)를 의미한다.  

Max-Age가 적용된 Permanent Cookie를 사용해보자

```javascript
const http = require('http');
const cookie = require('cookie');
http.createServer((req, res) => {
  console.log(req.headers.cookie);
  let cookies = {};
  if (req.headers.cookie !== undefined) {
    cookies = cookie.parse(req.headers.cookie);
  }
  console.log(cookies);
  console.log(cookies.yummy_cookie);
  console.log(cookies.tasty_cookie);
  res.writeHead(200, {
    'Set-Cookie': [
      'yummy_cookie=choco',
      'tasty_cookie=strawberry',
      `permanent_cookie=value; Max-Age=${60*60*24*30}`
    ]
  });
  res.end('cookie!');
}).listen(3000);
```

코드를 작성 후 브라우저를 리로드하고  
그리고 Application 탭에서 Storage - Cookies 에서 저장되어 있는 쿠키들을 확인하면  
permanent_cookie 만 만료시간이 부여되어 있고  
나머지 쿠키들은 만료시간이 세션으로 되어있는것을 확인 할 수 있다.  

쿠키를 사용하는 코드를 다음과 같이 비활성화 하고  

```javascript
const http = require('http');
const cookie = require('cookie');
http.createServer((req, res) => {
  console.log(req.headers.cookie);
  let cookies = {};
  if (req.headers.cookie !== undefined) {
    cookies = cookie.parse(req.headers.cookie);
  }
  console.log(cookies);
  console.log(cookies.yummy_cookie);
  console.log(cookies.tasty_cookie);
  // res.writeHead(200, {
  //   'Set-Cookie': [
  //     'yummy_cookie=choco',
  //     'tasty_cookie=strawberry',
  //     `permanent_cookie=value; Max-Age=${60*60*24*30}`
  //   ]
  // });
  res.end('cookie!');
}).listen(3000);
```

서버를 실행시킨 후, 브라우저를 종료하고 다시 열어보면  
permanent_cookie만 남아있는것을 확인 할 수 있다.  

<br />

## 쿠키 옵션 (Secure & HttpOnly)

### Secure 옵션

`Secure 옵션은 브라우저와 서버가 HTTPS를 통해서 통신할 때만 쿠키를 전송한다는 의미를 가지는 필드이다.`  
Secure 옵션을 적용해보자  

```javascript
const http = require('http');
const cookie = require('cookie');
http.createServer((req, res) => {
  console.log(req.headers.cookie);
  let cookies = {};
  if (req.headers.cookie !== undefined) {
    cookies = cookie.parse(req.headers.cookie);
  }
  console.log(cookies);
  console.log(cookies.yummy_cookie);
  console.log(cookies.tasty_cookie);
  res.writeHead(200, {
    'Set-Cookie': [
      'yummy_cookie=choco',
      'tasty_cookie=strawberry',
      `permanent_cookie=value; Max-Age=${60*60*24*30}`,
      'secure_cookie=value; Secure'
    ]
  });
  res.end('cookie!');
}).listen(3000);
```

코드를 작성 후 브라우저를 리로드하고  
브라우저에서는 네트워크 탭을 확인 해보면  
응답 헤더에서는 서버에서 전송한 모든 쿠키들이 보인다.  

그런데 Application 탭에서 Storage - Cookies 에서 저장되어 있는 쿠키들을 확인하면  
서버에서 전송한 secure_cookie가 추가되어 있지 않은것을 확인할 수 있다.  
그렇기 때문에 브라우저가 서버로 요청을 보낼 때 요청 헤더에 secure_cookie 필드가 전송되지 않는다.  

이렇게된 이유는 Secure 옵션은  
서버와 브라우저가 HTTPS 통신을 할 때만 쿠키를 전송하게 하기 때문이다.

### HttpOnly

다음으로 HttpOnly 옵션을 확인해 보자  
`HttpOnly는 자바스크립트와 같은 프로그래밍 언어를 통해서 쿠키에 접근을 하지 못하게 하는 옵션이다.`
자바스크립트 아래와 같은 코드를 통해 웹 브라우저에 저장되어 있는 쿠키들을 확인 할 수 있는데  

```javascript
document.cookie
```

이때 HttpOnly 옵션이 적용되어 있는 쿠키들은 확인 할 수 없다.  

HttpOnly 옵션을 적용해보자

```javascript
const http = require('http');
const cookie = require('cookie');
http.createServer((req, res) => {
  console.log(req.headers.cookie);
  let cookies = {};
  if (req.headers.cookie !== undefined) {
    cookies = cookie.parse(req.headers.cookie);
  }
  console.log(cookies);
  console.log(cookies.yummy_cookie);
  console.log(cookies.tasty_cookie);
  res.writeHead(200, {
    'Set-Cookie': [
      'yummy_cookie=choco',
      'tasty_cookie=strawberry',
      `permanent_cookie=value; Max-Age=${60*60*24*30}`,
      'secure_cookie=value; Secure',
      'HttpOnly_cookie=value; HttpOnly',
    ]
  });
  res.end('cookie!');
}).listen(3000);
```

코드를 작성 후 브라우저를 리로드하고  
그리고 Application 탭에서 Storage - Cookies 에서 저장되어 있는 4개의 쿠키들을 확인 후  
해당 개발자 도구에서 제공하는 콘솔에서 아래와 같은 명령어를 입력하면 결과는 3개만 나온다.  

```javascript
document.cookie

// "yummy_cookie=choco; tasty_cookie=strawberry; permanent_cookie=value"
```

HttpOnly 옵션이 적용된 쿠키는 보이지 않는다.

HttpOnly 옵션이 있는 이유는 자바스크립트를 이용해서  
쿠키를 훔쳐서 나쁜짓을 하는 경우가 너무 많기 때문에  
이런 옵션을 통해서 sessionId와 같이 중요한 정보를  
자바스크립트를 통해서 접근을 못하게 하기 위해서 사용하는 옵션이다.  

<br />

## 쿠키 옵션 (Path & Domain)

### Path

`특정 경로에서만 쿠키가 활성화되도록 하고 싶을 때 사용하는 옵션이다.`

Path 옵션을 적용해보자

```javascript
const http = require('http');
const cookie = require('cookie');
http.createServer((req, res) => {
  console.log(req.headers.cookie);
  let cookies = {};
  if (req.headers.cookie !== undefined) {
    cookies = cookie.parse(req.headers.cookie);
  }
  console.log(cookies);
  console.log(cookies.yummy_cookie);
  console.log(cookies.tasty_cookie);
  res.writeHead(200, {
    'Set-Cookie': [
      'yummy_cookie=choco',
      'tasty_cookie=strawberry',
      `permanent_cookie=value; Max-Age=${60*60*24*30}`,
      'secure_cookie=value; Secure',
      'HttpOnly_cookie=value; HttpOnly',
      'Path_cookie=value; Path=/cookie',
    ]
  });
  res.end('cookie!');
}).listen(3000);
```

`localhost:3000` 에 접속 후  
Application 탭에서 Storage - Cookies 에서 저장되어 있는 쿠키들을 확인 해 보면  
Path_cookie를 제외한 4개의 쿠키가 저장되어 있는것을 확인 할 수 있다.  

다시

`localhost:3000/cookie` 에 접속 후  
Application 탭에서 Storage - Cookies 에서 저장되어 있는 쿠키들을 확인 해 보면
Path_cookie를 포함한 5개의 쿠키가 저장되어 있는것을 확인 할 수 있다.  

그리고 cookie에 하위 path인

`localhost:3000/cookie/1` 에 접속 후  
Application 탭에서 Storage - Cookies 에서 저장되어 있는 쿠키들을 확인 해 보면  
Path_cookie를 포함한 5개의 쿠키가 저장되어 있는것을 확인 할 수 있다.  

즉 Path라고 하는 옵션을 주고 경로(디렉토리)를 지정하면  
그 디렉토리와 그 디렉토리 아래에서만 Path가 활성화 되서,  
거기에 해당하는 쿠키만 활성화 된다.  

Path 설정을 주지 않으면 default 가 루트라서  
루트와 루트 하위 디렉토리안에서만 쿠키가 활성화 되기 때문에  
모든 곳에서 쿠키가 활성화 되어 있다.  

### Domain

먼저 Domain 옵션을 적용해보고 이해해 보자

```javascript
const http = require('http');
const cookie = require('cookie');
http.createServer((req, res) => {
  console.log(req.headers.cookie);
  let cookies = {};
  if (req.headers.cookie !== undefined) {
    cookies = cookie.parse(req.headers.cookie);
  }
  console.log(cookies);
  console.log(cookies.yummy_cookie);
  console.log(cookies.tasty_cookie);
  res.writeHead(200, {
    'Set-Cookie': [
      'yummy_cookie=choco',
      'tasty_cookie=strawberry',
      `permanent_cookie=value; Max-Age=${60*60*24*30}`,
      'secure_cookie=value; Secure',
      'HttpOnly_cookie=value; HttpOnly',
      'Path_cookie=value; Path=/cookie',
      'Domain_cookie=value; Domain=o2.org',
    ]
  });
  res.end('cookie!');
}).listen(3000);
```

o2.org:3000 에 접속 후  
Application 탭에서 Storage - Cookies 에서 저장되어 있는 쿠키들을 확인해 보면  
Domain 필드를 확인해보면 Domain_cookie 쿠키만 .o2.org 이고  
나머지 다른 쿠키들은 o2.org 을 확인할 수 있다.  

o2.org를 포함호고 있는 어떠한 `서브 도메인`에서도 쿠키가 살아남는다는 의미이다.  

예를들어 보면 브라우저에서 test.o2.org 에 접속하게 되면  
Domain_cookie 쿠키만 살아있고 나머지 쿠키들은 보이지 않는다.  

`Path는 어느 Path 에서 동작할 것인가를 제한하는 것이고`  
`Domain은 어떤 Domain에서 동작 할것 인가를 설정하는 것이다.`  

<br />

## 참고자료

- [생활코딩 > Node.js - 쿠키와 인증](https://opentutorials.org/course/3387)
- [MDN HTTP Cookie](https://developer.mozilla.org/ko/docs/Web/HTTP/Cookies)
# Session

쿠키가 등장하면서 요청과 응답이라는 단순한 의사소통만 가능한 웹이  
이전에 통신했던 내용을 기억하는게 가능해졌다.  
덕분에 개인화, 인증과 같은것의 중요한 발판이 마련되었다.  

하지만 쿠키만을 통해서 인증을 구현하는 것은 매우 위험하다.  
왜냐면 쿠키가 유출될 수 있고 조작될 수 있기 때문이다.  

현대적인 애플리케이션은 쿠키를 통해서 인증을 구현하지 않고  
쿠키는 사용자를 식별하는 정도로만 사용하고  
실제 데이터는 서버쪽에 파일이나 데이터베이스로 저장하는것이 세션 방법이다.  

<br />

다음으로 Nodejs - express에서 session을 적용하면서  
세션을 이해하고, express에서 세션의 동작 흐름을 살펴볼 것이다.  

서버를 동작 시키고, `localhost:3000`에 접속할 때 마다 View: 1(계속증가)  
숫자가 계속 증가 하는것을 확인 할 수 있다.  

```javascript
const express = require('express');
const session = require('express-session');

const app = express();

app.use(session({
  secret: '21312#Rewff@#df',
  resave: false,
  saveUninitialized: true
}));

app.get('/', function (req, res, next) {
  console.log(req.session);
  if (req.session.num === undefined) {
    req.session.num = 1;
  } else {
    req.session.num = req.session.num + 1;
  }

  res.send(`View: ${req.session.num}`);
});

app.listen(3000, function () {
  console.log('3000!');
});
```

<br />

먼저 session 미들웨어를 살펴보면  

```javascript
app.use(session({
  secret: '21312#Rewff@#df',
  resave: false,
  saveUninitialized: true
}));
```

- `secret`
    - session ID cookie를 만들 때 사용하는 암호이다.
    - 필수 옵션이다.
    - 쿠키를 만들 때 사용하는 암호이기 때문에 외부에 노출하면 안된다.
- `resave`
    - 세션 데이터가 바뀔때만 저장한다.
    - 특별한 일 없으면 false로 사용한다.
    - 만약 true로 설정하면 세션 정보가 같아도 요청이 들어올 때 마다 세션 정보를 갱신한다.
- `saveUninitialized`
    - 세션이 필요하지 않을 때는 구동하지 않는다.
    - 특별한 일 없으면 true로 사용한다.

<br />

다음으로 session 미들웨어에 동작 방식을 보면

```javascript
app.use(session({
  secret: '21312#Rewff@#df',
  resave: false,
  saveUninitialized: true
}));

app.get('/', function (req, res, next) {
  console.log(req.session);
  if (req.session.num === undefined) {
    req.session.num = 1;
  } else {
    req.session.num = req.session.num + 1;
  }

  res.send(`View: ${req.session.num}`);
});
```

session 미들웨어를 추가해줌으로써 요청 객체에서 `session 프로퍼티`가 생성된다.  
session 미들웨어 없이 사용하면 요청 객체에서 session 프로퍼티가 없는 것을 확인 할 수 있다.  

<br />

그럼 서버쪽에서는 session이 어디에 저장되고 있을까?  
`The default server-side session storage, MemoryStore`  
session 미들웨어는 default 세션 저장소가 `메모리`로 되어있다.  
그래서 서버를 껏다 켜면 세션 정보가 삭제된다.  
세션 정보가 모두 삭제되면 사용자들은 모두 로그아웃 될 수 있다.  

사용자의 세션 정보는 메모리 같은 휘발성 저장소가 아니라 비휘발성 저장소에 저장되어야 한다.  

<br />

session 미들웨어는 session을 저장 할 수 있는 다양한 방법을 제공한다.  
[https://www.npmjs.com/package/express-session](https://www.npmjs.com/package/express-session)에서
`Compatible Session Stores`를 확인하면 된다.  

우리가 사용할 것은 세션 정보를 파일에 저장하는 `session-file-store`를 사용할 것이다.  

먼저 session-file-store를 설치하고

```bash
$ npm i session-file-store
```

코드를 변경한다.

```javascript
const express = require('express');
const session = require('express-session');
const FileStore = require('session-file-store')(session);

const app = express();

app.use(session({
  secret: '21312#Rewff@#df',
  resave: false,
  saveUninitialized: true,
  store: new FileStore(),
}));

...
```

<br />

서버를 다시 동작시키고 생기는 파일을 확인한다.  

사용자가 세션 아이디를 가지고 있는 상태에서 서버에 요청하면 세션 아이디를 서버쪽으로 전달한다.  
그럼 세션 미들웨어가 세션 아이디에 대응되는 파일을(없으면 만든 후) 읽고 그리고  
요청 객체에 session이라고 하는 프로퍼티 객체를 추가한다.  
그리고 요청 객체에서 session 프로퍼티를 추가, 수정, 삭제를 하면  
세션 미들웨어가 해당 세션 아이디에 매칭되는 파일을 갱신시킨다.  
현재는 세션 스토어 `session-file-store`를 쓰기 때문에 세션 미들웨어가 파일을 갱신시켜 준다.  
만약 세션을 스토어 데이터베이스를 연동하면 세션 미들웨어가 데이터베이스를 갱신시켜 준다.  

<br />

추가적인 옵션을 더 살펴보면  

세션의 옵션으로 `secure: true`로 설정하면 `HTTPS`에서만 세션 정보를 주고 받을 수 있도록 설정할 수 있다.

```javascript
app.use(session({
  secure: true,  
  secret: '21312#Rewff@#df',
  resave: false,
  saveUninitialized: true
}));
```

또한 사용자가 전송한 데이터에 자바스크립트 코드가 포함되어 있고, 그리고 그 자바스크립트가 활성화 될 수 있는 상태라면  
사용자는 자바스크립트를 이용해서 세션 쿠키 아이디를 탈취해서 공격자에게 전송할 수 있다.  
이를 막기 위해서는 사용자가 전송한 데이터에서 자바스크립트를 사용할 수 없도록 하는것이 필요하다.  

세션의 옵션으로 `httpOnly: true`로 지정하면 자바스크립트를 통해서 세션 쿠키를 사용할 수 없도록 강제 할 수 있다.  

```javascript
app.use(session({
  httpOnly: true,  
  secret: '21312#Rewff@#df',
  resave: false,
  saveUninitialized: true
}));
```

<br />

다중 사용자를 인증하는 방법을 세션을 이용해서 구현해 보자.  

<br />


## 참고자료

- [생활코딩 > Node.js - 세션과 인증](https://opentutorials.org/module/3648)
- [express-session](https://www.npmjs.com/package/express-session)

# Today I Learned

[![Version](https://img.shields.io/badge/version-2018.27.0-red.svg)](./CHANGELOG)

* 좋은 개발자가 되기 위해 하루동안 학습한 내용이나 개발관련 경험들을 간단한 기록으로 남긴다.
* 탄탄한 기본기를 갖추고, 변화하는 기술에 대한 지속적인 관심과 학습을 통해 배움을 멈추지 않는 것이 목표이다.
* Today I Learned인데 날짜를 표시하지 않는 이유는 조급해지지 않고 꾸준하기 위함이다.


## 작성 규칙

* 폴더와 파일명은 영문으로 작성한다.
* 해당 문서를 다시 봤을 때, 추가적인 검색의 비용이 들지 않도록 자세히 기록한다.
* 쉴 땐 확실히 쉬고, contributions를 파랑색으로 채우는 것에 집착하지 않는다.
* reference를 명시하고, 원작자가 참고를 허용하는 자료만 사용한다.


## 분류

- Seminar & Conference
- C#
    - BASIC
        - [IComparable-IComarer-interface.md](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/Basic/IComparable-IComarer-interface.md)
        - [Basic-Type-and-Type-Conversion.md](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/Basic/Basic-Type-and-Type-Conversion.md)
    - BCL
        - [ADO.NET-data-provider.md](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/BCL/ADO.NET-data-provider.md)
        - [Collection.md](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/BCL/Collection.md)
    - ASP.NET-Core-MVC
        - inflearn
            - [01-Base-Main-And-Startup](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/01-Base-Main-And-Startup.md)
            - [02-Base-wwwroot](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/02-Base-wwwroot.md)
            - [03-MVC-Controller-And-View](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/03-MVC-Controller-And-View.md)
            - [04-MVC-ModelBinding](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/04-MVC-ModelBinding.md)
            - [05-MVC-Validation-1](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/05-MVC-Validation-1.md)
            - [06-MVC-Validation-2](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/06-MVC-Validation-2.md)
            - [07-MVC-ViewModel](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/07-MVC-ViewModel.md)
            - [08-Razor-Layout](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/08-Razor-Layout.md)
            - [09-Razor-ViewStart](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/09-Razor-ViewStart.md)
            - [10-Razor-ViewImport](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/10-Razor-ViewImport.md)
            - [11-Razor-PartialView](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/11-Razor-PartialView.md)
            - [12-MVC-EFC-Config](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/12-MVC-EFC-Config.md)
            - [13-MVC-EFC-DbContext](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/13-MVC-EFC-DbContext.md)
            - [14-MVC-EFC-Migration](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/14-MVC-EFC-Migration.md)
            - [15-MVC-EFC-SeedDatabase](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/15-MVC-EFC-SeedDatabase.md)
            - [16-Repository-ClassAndInterface](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/16-Repository-ClassAndInterface.md)
            - [17-Repository-Create](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/17-Repository-Create.md)
            - [18-Repository-Read](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/18-Repository-Read.md)
            - [19-Repository-Update](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/19-Repository-Update.md)
            - [20-Repository-Delete](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/20-Repository-Delete.md)
            - [21-Identity-Config](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/21-Identity-Config.md)
            - [22-Identity-RegisterPage](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/22-Identity-RegisterPage.md)
            - [23-Identity-LoginPage](https://gitlab.com/byeonghoon/TIL/blob/master/C%23/ASP.NET-Core-MVC/inflearn/23-Identity-LoginPage.md)   
- ECMAscript
    - basic
        - [Math-Decimal Method](https://gitlab.com/byeonghoon/TIL/blob/master/ECMAscript/basic/Math-method-about-decimal.md)
        - [Number Static Method](https://gitlab.com/byeonghoon/TIL/blob/master/ECMAscript/basic/Number-static-method.md)
- Algorithm
    - Programmers
        - [level1-Import-middle-letters](https://gitlab.com/byeonghoon/TIL/blob/master/Algorithm/programmers/level1-Import-middle-letters.md)
        - [level1-array-of-dividing-numbers](https://gitlab.com/byeonghoon/TIL/blob/master/Algorithm/programmers/level1-array-of-dividing-numbers.md)
        - [level1-sum-of-two-integers](https://gitlab.com/byeonghoon/TIL/blob/master/Algorithm/programmers/level1-sum-of-two-integers.md)
        - [level1-handling-strings-basic](https://gitlab.com/byeonghoon/TIL/blob/master/Algorithm/programmers/level1-handling-strings-basic.md)
- OOP
    - inflearn
        - [01-OOP](https://gitlab.com/byeonghoon/TIL/blob/master/OOP/inflearn/01-OOP.md)
        - [02-OOP](https://gitlab.com/byeonghoon/TIL/blob/master/OOP/inflearn/02-OOP.md)
- Nodejs
    - [cookie](https://gitlab.com/byeonghoon/TIL/blob/master/Nodejs/cookie.md)
    - [session](https://gitlab.com/byeonghoon/TIL/blob/master/Nodejs/session.md)
    - [passport-basic-and-local-strategy](https://gitlab.com/byeonghoon/TIL/blob/master/Nodejs/passport-basic-and-local-strategy.md)
- OAuth
    - [01-OAuth-intro](https://gitlab.com/byeonghoon/TIL/blob/master/OAuth/01-OAuth-intro.md)
    - [02-OAuth-role](https://gitlab.com/byeonghoon/TIL/blob/master/OAuth/02-OAuth-role.md)
    - [03-OAuth-register](https://gitlab.com/byeonghoon/TIL/blob/master/OAuth/03-OAuth-register.md)
    - [04-OAuth-resource-owner-acknowledgment](https://gitlab.com/byeonghoon/TIL/blob/master/OAuth/04-OAuth-resource-owner-acknowledgment.md)
    - [05-OAuth-resource-server-acknowledgment](https://gitlab.com/byeonghoon/TIL/blob/master/OAuth/05-OAuth-resource-server-acknowledgment.md)
    - [06-OAuth-access-token](https://gitlab.com/byeonghoon/TIL/blob/master/OAuth/06-OAuth-access-token.md)
    - [07-OAuth-refresh-token](https://gitlab.com/byeonghoon/TIL/blob/master/OAuth/07-OAuth-refresh-token.md)

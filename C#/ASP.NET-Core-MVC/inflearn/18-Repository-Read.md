# 18-Repository-Read

## CRUD - Read

Read에서 해야되는 작업은 현재 2가지이다.

첫 번째로 사용자가 폼을 통해서 사용자 등록을 요청 하였을 때  
현재 학생 정보를 폼에서 전송하면 테이블에 저장까지만 하였다.  
정보가 추가됬기 때문에, 다시 추가된 정보가 포함된 정보들을 테이블에서 다시 읽어와서 View에 전달해주는 코드로 변경한다.  

두 번째로 현재 HomeController에 Student Action 메소드에서  
더미 데이터 코드를 테이블에서 읽어오는 코드로 변경한다.  

StudentTeacherViewModel.cs을 수정한다.

```cs
// {Project}/ViewModels/studentTeacherViewModel.cs

public class StudentTeacherViewModel
{
  public Student Student { get; set; }
  public IEnumerable<Teacher> Teachers { get; set; }
  public IEnumerable<Student> Students { get; set; }
}
```

HomeController.cs를 수정한다.

```cs
// {Project}/Controllers/HomeController

public class HomeController : Controller
{
  ...
  
  // GET
  public IActionResult Student()
  {
    var students = _studentRepository.GetAllStudents();

    var viewModel = new StudentTeacherViewModel
    {
      Student = new Student(),
      Students = students
    };

    return View(viewModel);
  }

  [HttpPost]
  [ValidateAntiForgeryToken]
  public IActionResult Student(StudentTeacherViewModel model)
  {
    if (!ModelState.IsValid)
    {
      // 에러를 보여준다.
    }

    _studentRepository.AddStudent(model.Student);
    _studentRepository.Save();

    var students = _studentRepository.GetAllStudents();

    var viewModel = new StudentTeacherViewModel
    {
      Student = new Student(),
      Students = students
    };

    return View(viewModel);
  }
}
```

_Layout.cshtml를 수정한다.

```html
// {Project}Views/Shared/_Layout.cshtml

...

<div class=container-fluid>
  @RenderBody()
</div>

...

```

그리고 Student.cshtml 파일에서  
마지막 줄에 있는 `@await Html.PartialAsync("_TeacherTable", Model)` 코드를 삭제한다.  
그리고 학생들을 디스플레이하는 하고 어떤 열을 클릭하면 그 학생 정보를 보여주는 테이블을 만든다.

```html
// {Project}Views/Home/Student.cshtml

...

</form>

<hr/>

<h3>학생리스트</h3>
<table class="table">
  <thead>
    <tr>
      <th>이름</th>
      <th>나이</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach (var student in Model.Students)
    {
      <tr>
        <td>@student.Name</td>
        <td>@student.Age</td>
        <td>
          <a asp-controller="Home" asp-action="Detail" asp-route-id="@student.Id">Detail</a>
        </td>
      </tr>
    }
  </tbody>
</table>

```

a 링크를 클릭하면 Home 컨트롤러에 Detail 액션 메소드로 우리가 클릭한 열의 Id를 전송하도록 만들었다.

HomeController에 Detail 액션 메소드를 구현한다.

```cs
// {Project}/Controllers/HomeController.cs

public class HomeController : Controller
{
  private readonly ITeacherRepository _teacherRepository;
  private readonly IStudentRepository _studentRepository;

  public HomeController(ITeacherRepository teacherRepository,
    IStudentRepository studentRepository)
  {
    _teacherRepository = teacherRepository;
    _studentRepository = studentRepository;
  }

  ...

  public IActionResult Detail(int id)
  {
    var result = _studentRepository.GetStudent(id);

    return View(result);
  }
}
```

HomeController에 Detail 액션 메소드와 매칭되는 뷰를 구현한다.

```html
// {Project}/Views/Home/Detail.cshtml

@model Student

@{
  ViewBag.Title = "Detail";
}

<h2>학생정보</h2>
<hr/>
<h3>아이디: @Model.Id</h3>
<h3>이름: @Model.Name</h3>
<h3>국가: @Model.Country</h3>
```

그리고 HomeController에서 뷰에서 폼을 요청 후 입력한 값을 지우기 위해 다음과 같은 코드를 사용한다.  

```cs
ModelState.Clear();
```

```cs
// {Project}/Controllers/HomeController.cs

public class HomeController : Controller
{
  ...
  
  [HttpPost]
  [ValidateAntiForgeryToken]
  public IActionResult Student(StudentTeacherViewModel model)
  {
    if (!ModelState.IsValid)
    {
      // 에러를 보여준다.
    }

    _studentRepository.AddStudent(model.Student);
    _studentRepository.Save();

    var students = _studentRepository.GetAllStudents();

    var viewModel = new StudentTeacherViewModel
    {
      Student = new Student(),
      Students = students
    };

    ModelState.Clear(); // 폼 입력 값들 초기화

    return View(viewModel);
  }

  ...
}
```

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
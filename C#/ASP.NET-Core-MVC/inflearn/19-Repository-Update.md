# 19-Repository-Update

## CRUD - Update

Student 뷰에서 유효성 검사가 통과했을 때만 폼 입력 값들을 초기화 하도록 변경하자  

```cs
// {Project}/Controllers/HomeController.cs

public class HomeController : Controller
{
  ...
  
  [HttpPost]
  [ValidateAntiForgeryToken]
  public IActionResult Student(StudentTeacherViewModel model)
  {
  if (ModelState.IsValid)
  {
    _studentRepository.AddStudent(model.Student);
    _studentRepository.Save();

    ModelState.Clear(); // 폼 입력 값들 초기화
  }
  else
  {
    // 에러를 보여준다.
  }

    var students = _studentRepository.GetAllStudents();

    var viewModel = new StudentTeacherViewModel
    {
    Student = new Student(),
    Students = students
    };

    return View(viewModel);
  }

  ...
}
```

다음과 같은 작업을 할 것이다.  
학생 테이블에서 수정하고자 하는 학생의 열을 클릭하면  
학생의 정보를 상세하게 볼 수 있는 화면이 나온다.  
그리고 그곳에서 수정하고 싶은 것을 수정 후 폼을 요청해서 성공하면  
다시 Student 화면으로 redirect 되고 Student 화면에서 수정된 학생들 정보를 볼 수 있다.  

StudentRepository.cs와 IStudentRepository.cs Edit 메소드를 추가하자

```cs
// {Project}/Data/Repositories/StudentRepository

public class StudentRepository : IStudentRepository
{
  private readonly MyAppContext _context;

  public StudentRepository(MyAppContext context)
  {
    _context = context;
  }

  ...

  public void Edit(Student student)
  {
    _context.Update(student);
  }
}
```

```cs
// {Project}/Data/Repositories/IStudentRepository

public interface IStudentRepository
{
  void AddStudent(Student student);

  IEnumerable<Student> GetAllStudents();

  Student GetStudent(int id);

  void Save();

  void Edit(Student student);
}
```

그리고 Student 뷰에도 Edit 링크를 추가해주자

```html
<!-- {Project}/Views/Home/Student.cshtml -->
  ...

  <tbody>
  @foreach (var student in Model.Students)
  {
    <tr>
      <td>@student.Name</td>
      <td>@student.Age</td>
      <td>
        <a asp-controller="Home"
        asp-action="Detail"
        asp-route-id="@student.Id">
        Detail
        </a>
      </td>
      <td>
        <a asp-controller="Home"
        asp-action="Edit"
        asp-route-id="@student.Id">
        Edit
        </a>
      </td>
    </tr>
  }
  </tbody>
</table>
```

그리고 HomeController에서
Edit 페이지를 볼 수 있도록 하고, Edit 할 수 있는 기능을 추가 하자

```cs
// {Project}/Controllers/HomeController.cs

public class HomeController : Controller
  {
    ...

    public IActionResult Detail(int id)
    {
      var result = _studentRepository.GetStudent(id);

      return View(result);
    }

    public IActionResult Edit(int id)
    {
      var result = _studentRepository.GetStudent(id);

      return View(result);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Edit(Student student)
    {
      if (ModelState.IsValid)
      {
        _studentRepository.Edit(student);
        _studentRepository.Save();

        return RedirectToAction("Student");
      }

      return View(student);
    }
  }
```

마지막으로 Edit 페이지를 만들자

```html
<!-- {Project}/Views/Home/Edit.cshtml -->

@model Student

@{
  ViewBag.Title = "Edit";
}

<h3>학생정보 수정</h3>
<form class="form from-horizontal" method="post">
  <div class="form-group">
    <div class="col-md-3">
      <label>아이디:</label>
      <input type="text" class="form-control" asp-for="@Model.Id" disabled />
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>이름:</label>
      <input type="text" class="form-control" asp-for="@Model.Name"/>
      <span asp-validation-for="@Model.Name"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>나이:</label>
      <input type="text" class="form-control" asp-for="@Model.Age"/>
      <span asp-validation-for="@Model.Age"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>국가:</label>
      <input type="text" class="form-control" asp-for="@Model.Country"/>
      <span asp-validation-for="@Model.Country"></span>
     </div>
  </div>
  <button type="submit" class="btn btn-default">Update</button>
</form>
```

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
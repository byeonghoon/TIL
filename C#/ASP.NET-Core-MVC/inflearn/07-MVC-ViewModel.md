# 07-MVC-ViewModel

## 뷰모델

Models 폴더안에 있는 클래스는 데이터베이스에있는 테이블과 매칭된다.  

현재 Student 클래스는 데이터베이스에 있는 Student 테이블을 나타내고  
Student 클래스에 있는 필드들은 Student 테이블에 있는 열들을 나타낸다.  

나중에 Student 모델 클래스를 사용해서 데이터베이스 Student 테이블에  
새로운 Student를 추가하거나 수정하거나 삭젝하거나 조회할 수 있게 한다.  

그에 반해 `ViewModel`은
각각 다른 클래스에서 불러온 데이터를 한 페이지에 동시에 보여주거나  
뷰만을 위한 데이터 포맷 이라고 생각하면 될것 같다.  

현재 Student.cshtml 안에는 Student 모델 클레스만 사용되고 있다.  
여기에 Teacher 모델 클레스도 만들고, Student 모델 클레스와 함께 Student.cshtml 안에서 사용되도록 해보자.  

Models 폴더에 Teacher 모델 클래스를 만들자.

```cs
// {Project}/Models/Teacher.cs

public class Teacher
{
  public string Name { get; set; }
  public string Class { get; set; }
}
```

`{Project}/ViewModels` 폴더를 만들고  
`{Project}/ViewModels/StudentTeacherViewModel.cs`를 만들자

현재 Student화면에서 필요한 것은  

- 학생을 추가시키는 폼 하는 UI
- 선생님의 목록을 테이블로 디스플레이 하는 UI

그렇기 때문에 StudentTeacherViewModel 클래스의 구조는 다음과 같다.  

```cs
// {Project}/ViewModels/StudentTeacherViewModel.c

public class StudentTeacherViewModel
{
  public Student Student { get; set; }
  public List<Teacher> Teachers { get; set; }
}
```

뷰모델을 만들었으면 그 다음으로 설정 할 것은 컨트롤러이다.
Student 뷰에 선생님들 정보를 넘기기 위해서 임시로 리스트를 이용해서 선생님 정보를 만들것이다.  
컨트롤러에서 뷰를 리턴할 때, 뷰모델도 같이 View로 넘길 수 있다.(View에 오버로드 메소드를 확인하면된다.)  

```cs
// {Project}/Controllers/HomeController

public class HomeController : Controller
{
// GET
public IActionResult Student()
{
  List<Teacher> teachers = new List<Teacher>
  {
    new Teacher {Name = "세종대왕", Class = "한글"},
    new Teacher {Name = "이순신", Class = "해상전"},
    new Teacher {Name = "재갈량", Class = "지"},
    new Teacher {Name = "을지문덕", Class = "지상전략"}
  };

  var viewModel = new StudentTeacherViewModel
  {
    Student = new Student(),
    Teachers = teachers
  };

  // View를 리턴할 때 모델도 같이 넘길 수 있다.
  return View(viewModel);
}

...
```

뷰에서 컨트롤러에서 전달한 ViewModel을 받기위해서 해야될 작업이 있다.  

기존 로직에서는 Student.cshtml 파일에서 Student 클래스를 사용하기 위해 다음과 같은 정보를 임포트 하였다.

```html
// {Project}/Views/Home/Student.cshtml

@using WebApplication8.Models
@model Student
...
```

이 정보를 그냥 두면 컨트롤러에서 전송한 뷰모델 클래스와 타입이 맞지 않아 애러가 발생한다.  
항상 컨트롤러에서 전달한 모델과 뷰에서 사용하는 모델과 일치하도록 한다.

다음과 같이 수정한다.

```html
// {Project}/Views/Home/Student.cshtml

@using WebApplication8.ViewModels
@model StudentTeacherViewModel
...
```

`Razor 파일에서 @Model은 Controller에서 전송한 ViewModel을 의미한다.`

```html
// {Project}/Views/Home/Student.cshtml

@using WebApplication8.ViewModels
@model StudentTeacherViewModel

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<h3>Add New Student</h3>
<form class="form from-horizontal" method="post">
  <div class="form-group">
    <div class="col-md-3">
      <label>Name:</label>
      <input type="text" class="form-control" asp-for="@Model.Student.Name"/>
      <span asp-validation-for="@Model.Student.Name"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>Age:</label>
      <input type="text" class="form-control" asp-for="@Model.Student.Age"/>
      <span asp-validation-for="@Model.Student.Age"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>Country:</label>
      <input type="text" class="form-control" asp-for="@Model.Student.Country"/>
      <span asp-validation-for="@Model.Student.Country"></span>
    </div>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
```

Razor 파일에 매칭되는 컨트롤러에 있는 Action 메소드에서는 해딩 Razor 파일에서 정의한  
@model StudentTeacherViewModel 과 매칭되도록 model의 인자 타입을 선언해야한다.  
코드로 보면 다음과 같다.  

```cs
// {Project}/Controllers/HomeController

public class HomeController : Controller
{

...

  [HttpPost]
  [ValidateAntiForgeryToken]
  public IActionResult Student(StudentTeacherViewModel model) // 수정(Student -> StudentTeacherViewModel)
  {
    ...
    return View();
  }

...

}
```

사실상 전송되는 데이터 구조가 Student 클래스 같긴 한데  
asp.net core 에서 약속처럼 사용하는 구조 같다.  

마지막으로 `{Project}/Views/Home/Student.cshtml`에 선생님 정보를 디스플레이 하자  

```html
// {Project}/Views/Home/Student.cshtml

@using WebApplication8.ViewModels
@model StudentTeacherViewModel

<from>
...
</form>

<hr/>

<h3>Our Teachers</h3>
<table class="table">
  <thead>
    <tr>
      <th>Name</th>
      <th>Class</th>
    </tr>
</thead>
  <tbody>
    @foreach (var teacher in Model.Teachers)
    {
    <tr>
      <td>@teacher.Name</td>
      <td>@teacher.Class</td>
    </tr>
    }
  </tbody>
</table>
```

이렇게 페이지에 데이터를 이용하는 경우 뷰모델을 이용하면 된다.  
모델을 바로 이용하는 것은 좋은 방법이 아니다.  

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
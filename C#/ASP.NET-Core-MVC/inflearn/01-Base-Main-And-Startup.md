# 01-Base-Main-And-Startup

## Main

```cs
public class Program
{
  public static void Main(string[] args)
  {
    BuildWebHost(args).Run();
  }

  public static IWebHost BuildWebHost(string[] args) =>
    WebHost.CreateDefaultBuilder(args) // 웹 어플리케이션을 실행하기 위한 기초 설정
      .UseStartup<Startup>() // 웹 요청이 들어 왔을때 어떤 클래스를 사용할지
      .Build();
}
```

asp.net core를 동작시키면 프로젝트에 있는 Main 문이 시작된다.  
Main문안에 있는 `BuildWebHost 메소드`를 동작 시키고 그리고 이어서 `Run 메소드`를 호출한다.  

BuildWebHost 메소드에서는 `WebHost 클래스`에 있는 `CreateDefaultBuilder 메소드`를 통해서  
웹 어플리케이션을 실행하기 위한 기초를 설정한다.

그리고 이어서 `UseStartup 메소드`를 통해서 웹 요청이 들어 왔을때 어떤 클래스를 사용할지를 결정하고 빌드한다.  
`UseStartup<Startup>()`로 정의되어 있기 때문에 웹 요청이 들어왔을 때 `Startup 클래스`에서 요청을 처리한다.

<br />

## Startup 클래스

```cs
public class Startup
{
  public void ConfigureServices(IServiceCollection services)
  {
  }

  public void Configure(IApplicationBuilder app, IHostingEnvironment env)
  {
    if (env.IsDevelopment())
    {
      app.UseDeveloperExceptionPage();
    }

    app.Run(async (context) => { await context.Response.WriteAsync("Hello World!"); });
  }
}
```

Startup 클래스는 asp.net core 어플리케이션의 엔진 역할을 한다.  
Startup 클래스에는 2개의 메소드가 정의 되어 있다.

- `ConfigureServices`
- `Configure`

`ConfigureServices` 및 `Configure`는 앱 시작 시 런타임에 의해 호출된다.


### ConfigureServices

`ConfigureServices 메소드`는 `serviceCollection을 파라미터`로 받는다.  
이것은 `애플리케이션 전역에서 사용하는 서비스 컨테이너`이다.  
이것에 서비스를 추가하면 애플리케이션 어디서든 주입된 서비스를 사용할 수 있다.  

### Configure

`Configure 메소드`는 `HTTP 프로세스 파이프 라인`을 설정해주는 함수이다.  
HTTP 프로세스 파이프 라인에 의미는 HTTP 요청이 들어왔을 때,  
이것을 어떻게 듣고, 어떤식으로 진행되면서 처리를 해줄지 정의해주는 함수이다.  

HTTP 프로세스 파이프라인에 미들웨어들을 정의해서 HTTP 요청 처리 흐름을 제어할 수 있다.  
미들웨어는 요청 및 응답을 처리하는 응용 프로그램 파이프라인으로 어셈블리되는 소프트웨어이다.

- 요청을 파이프라인의 다음 구성 요소로 전달할지 여부를 선택한다.
- 파이프라인의 다음 구성 요소가 호출되기 전과 후에 작업을 수행할 수 있다.


```cs
// 개발 모드인 경우, 예외가 발생했을 때, 개발자 예외 페이지를 사용하겠다는 미들웨어 설정이다.
if (env.IsDevelopment())
{
  app.UseDeveloperExceptionPage();
}

// `HTTP 요청이 파이프 라인`을 통해서 해당 메소드까지 도달 할 경우
// 무조건 "Hello World!"를 응답하는 로직이다.
app.Run(async (context) => { await context.Response.WriteAsync("Hello World!"); });
```

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/)
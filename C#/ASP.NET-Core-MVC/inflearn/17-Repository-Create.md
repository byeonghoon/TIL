# 17-Repository-Create

## CRUD - Create

StudentRepository.cs 파일을 생성한다.  
`{Project}/Data/Repositories/StudentRepository.cs`  
이 파일에서는 MyAppContext 서비스를 주입받고  
이 서비스를 통해서 Student 테이블에 CRUD를 할 수 있는 메소드들을 만들것이다.  

StudentRepository.cs의 코드는 다음과 같다.  

```cs
// {Project}/Data/Repositories/StudentRepository.cs

public class StudentRepository
{
  private readonly MyAppContext _context;

  public StudentRepository(MyAppContext context)
  {
    _context = context;
  }

  public void AddStudent(Student student)
  {
    _context.Students.Add(student);
  }

  public void Save()
  {
    _context.SaveChanges();
  }

  public IEnumerable<Student> GetAllStudents()
  {
    var result = _context.Students.ToList();

    return result;
  }

  public Student GetStudent(int id)
  {
    var result = _context.Students.Find(id);

    return result;
  }
}
```

다음 코드는 엔티티 프레임워크 코어에서 Student 엔티티가 데이터베이스에 추가될 준비가 되었다는 변동사항만 추적하고 있다.  

```cs
_context.Students.Add(student);
```

실제적으로 아래 코드가 우리가 MyAppContext에서 일어난 변동사항을 데이터베이스에 적용하는 코드이다.  

```cs
_context.SaveChanges()
```

이제 인터페이스로 이 메소드를 노출시키는 작업을 한다.  
IStudentRepository.cs 인터페이스 파일을 생성한다.  
`{Project}/Data/Repositories/IStudentRepository.cs`

IStudentRepository.cs의 코드는 다음과 같다.  

```cs
// {Project}/Data/Repositories/IStudentRepository.cs

public interface IStudentRepository
{
  void AddStudent(Student student);

  IEnumerable<Student> GetAllStudents();

  Student GetStudent(int id);

  void Save();
}
```

IStudentRepository 인터페이스를 만들었으면 StudentRepository에서 이를 구현하도록 코드를 변경한다.  

```cs
// {Project}/Data/Repositories/StudentRepository.cs

public class StudentRepository : IStudentRepository
{
  private readonly MyAppContext _context;

  ...
}
```

다음으로 Startup 클래스에서 StudentRepository를 서비스 컨테이너에 등록한다.  

```cs
// {Project}/Startup.cs

public class Startup
{
  ...

  public void ConfigureServices(IServiceCollection services)
  {
    services.AddDbContext<MyAppContext>(options =>
    {
      options.UseNpgsql(_config.GetConnectionString("MyAppConnection"));
    });

    services.AddTransient<DbSeeder>();
    services.AddScoped<ITeacherRepository, TeacherRepository>();
    services.AddScoped<IStudentRepository, StudentRepository>();
    services.AddMvc();
  }
  ...
}
```

HomeController 에서 레파지토리 서비스를 의존성 주입받고 사용한다.

```cs
// {Project}/Controllers/HomeController.cs

public class HomeController : Controller
{
  private readonly ITeacherRepository _teacherRepository;
  private readonly IStudentRepository _studentRepository;

  public HomeController(ITeacherRepository teacherRepository,
                        IStudentRepository studentRepository)
  {
    _teacherRepository = teacherRepository;
    _studentRepository = studentRepository;
  }

  ...

  [HttpPost]
  [ValidateAntiForgeryToken]
  public IActionResult Student(StudentTeacherViewModel model)
  {
    if (!ModelState.IsValid)
    {
      // 에러를 보여준다.
    }

    _studentRepository.AddStudent(model.Student);
    _studentRepository.Save();

    return View();
  }
}
```

<br />

## 참고자료
- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
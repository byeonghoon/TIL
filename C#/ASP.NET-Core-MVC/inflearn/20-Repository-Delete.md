# 20-Repository-Delete

## CRUD - DELETE

다음로 Student 테이블에서 Delete 링크를 클릭하면 바로 삭제하는 기능을 구현 할 것이다.

우선 IStudentRepository.cs와 StudentRepository.cs Delete 메소드를 만든다.

```cs
// {Project}/Data/Repositories/IStudentRepository.cs

public interface IStudentRepository
{
  void AddStudent(Student student);

  IEnumerable<Student> GetAllStudents();

  Student GetStudent(int id);

  void Save();

  void Edit(Student student);

  void Delete(Student student);
}
```

```cs
// {Project}/Data/Repositories/StudentRepository.cs

public class StudentRepository : IStudentRepository
{
  ...

  public void Edit(Student student)
  {
    _context.Update(student);
  }

  public void Delete(Student student)
  {
    _context.Remove(student);
  }
}
```

다음으로 Student 페이지에서 테이블에 새로운 컬럼을 만든다.

```html
<!-- {Project}/Views/Home/Student.cshtml -->

...
<h3>학생리스트</h3>
<table class="table">
    <thead>
        <tr>
            <th>이름</th>
            <th>나이</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach (var student in Model.Students)
    {
        <tr>
            <td>@student.Name</td>
            <td>@student.Age</td>
            <td>
                <a asp-controller="Home"
                   asp-action="Detail"
                   asp-route-id="@student.Id">
                    Detail
                </a>
            </td>
            <td>
                <a asp-controller="Home"
                   asp-action="Edit"
                   asp-route-id="@student.Id">
                    Edit
                </a>
            </td>
            <td>
                <a asp-controller="Home"
                   asp-action="Delete"
                   asp-route-id="@student.Id">
                    Delete
                </a>
            </td>
        </tr>
    }
    </tbody>
</table>
```

그리고 HomeController에서 Delete 액션 메소드를 만든다.

```cs
// {Project}/Controllers/HomeController.cs

public class HomeController : Controller
  {
    ...

    public IActionResult Delete(int id)
    {
      var result = _studentRepository.GetStudent(id);

      if (result != null)
      {
        _studentRepository.Delete(result);
        _studentRepository.Save();
      }

      return RedirectToAction("Student");
    }
  }
```

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
# 15-MVC-EFC-SeedDatabase

## Seed 데이터베이스

### 초기 데이터를 데이터베이스에 입력하는 방법에 관하여

다시 Teacher, Student 모델을 현재 테이블과 매칭되도록 원복하자.  

```cs
// {Project}/Models/Student.cs

public class Student
{
  public int Id { get; set; }
  [Required]
  [MaxLength(50)]
  public string Name { get; set; }
  [Range(15, 70)]
  public int Age { get; set; }
  [Required, MinLength(5)]
  public string Country { get; set; }
}
```

```cs
// {Project}/Models/Teacher.cs

public class Teacher
{
  public int Id { get; set; }
  public string Name { get; set; }
  public string Class { get; set; }
}
```

지금부터 초기 데이터를 데이터베이스에 넣어보는 작업을 할 것이다.
초기 데이터를 넣으는것을 영어로 `seeding`이라고 한다.  

데이터가 담겨져있는 파일을 Startup에서 실행시켜서 데이터베이스에 초기 데이터가 입력되도록 할 것이다.  

Data폴더에 DbSeeder라는 클래스 파일을 만든다.  
`{Project}/Data/DbSeeder.cs`  

DbSeeder라는 클래스에서 MyAppContext를 private으로 사용할 수 있도록 설정한다.  
그리고 Teacher 테이블에 데이터가 없다면 Teacher 리스트를 만들고 테이블에 저장한다.  

만약 방대한 데이터를 초기에 입력하고 싶으면 json이나 cs 파일을 읽어와서 맵핑 후 사용하면 된다.  
리스트이면 `AddRange 메소드`를, `한 개이면 Add 메소드`를 사용한다.  
그리고 마지막으로 데이터베이스에 변경사항을 반영하기 위해서 `Save 메소드`를 호출한다.  

```cs
// {Project}/Data/DbSeeder

public class DbSeeder
{
  private MyAppContext _context;

  public DbSeeder(MyAppContext context)
  {
    _context = context;
  }

  public async Task SeedDatabase()
  {
    if (!_context.Teachers.Any())
    {
      List<Teacher> teachers = new List<Teacher>
      {
        new Teacher {Name = "세종대왕", Class = "한글"},
        new Teacher {Name = "이순신", Class = "해상전"},
        new Teacher {Name = "재갈량", Class = "지"},
        new Teacher {Name = "을지문덕", Class = "지상전략"}
      };

      await _context.AddRangeAsync(teachers);
      await _context.SaveChangesAsync();
    }
  }
}
```

이제 함수를 만들었으므로 이것을 Startup 클래스에서 불러오고 설정하겠다.  

먼저 우리가 만든 DbSeeder 클래스를 서비스 컨테이너 추가한다.  
`services.AddTransient<DbSeeder>();`
`Transient 메소드`는 `필요 할 때 마다 생성`되는 서비스이다.  
`캐쉬내에 머물러있지 않고 보존되지 않는 형태이다.`  
DbSeeder 클래스 같은 경우 웹 어플리케이션 생명주기에서 딱 한번만 필요한 서비스이다.  
그래서 Transient 메소드가 적합하다.  

이 외에도 `Scope`와 `SingleTon`와 같은 메소드들이 있다.  
서비스 컨테이너에 `services.AddTransient<DbSeeder>();`를 추가하면  
이것을 아래 Configure메소드에 매개변수로 넘겨서 사용할 수 있다.

Configure메서드 안에서 `DbSeeder seeder` 인자를 추가한다.
`public void Configure(IApplicationBuilder app, IHostingEnvironment env, DbSeeder seeder)`  
그리고 Configure메서드 안에서 seeder를 사용한다.  
`seeder.SeedDatabase().Wait();`  
Wait()은 Task 함수가 끝날때 까지 기다린다는 의미이다.

```cs
// {Project}/Startup.cs

public class Startup
{
  ...

  public void ConfigureServices(IServiceCollection services)
  {
    services.AddDbContext<MyAppContext>(options =>
    {
      options.UseNpgsql(_config.GetConnectionString("MyAppConnection"));
    });

    services.AddTransient<DbSeeder>();

    ...
  }

  public void Configure(IApplicationBuilder app, IHostingEnvironment env, DbSeeder seeder)
  {
    if (env.IsDevelopment())
    {
      app.UseDeveloperExceptionPage();
    }

    ...

    seeder.SeedDatabase().Wait();
  }
}
```

이제 애플리케이션을 실행시키면 Configure 메소드에서 마지막 라인에서  
seeder.SeedDatabase().Wait(); 이 실행되고  
SeeDatabase 메소드에서는 만약에 Teachers 테이블에 데이터가 아무것도 없으면  
우리가 만든 정보를 테이블에 넣고 마지막에 저장을 시킨다.  

애플리케이션을 실행시킨 후 테이블에 정보가 생기는지 확인한다.

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
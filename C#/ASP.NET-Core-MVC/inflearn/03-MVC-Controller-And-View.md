# 03-MVC-Controller-And-View

## MVC란?

MVC는 모델, 뷰, 컨트롤러의 약자이다.  
이 3가지의 역할은 각자 다르지만 결국 서로 협력하는 관계이다.  

모델은 데이터를 다룬다.  
컨트롤러는 로직을 다룬다.  
뷰는 사용자들에게 실제적으로 보여지는 부분을 다룬다.  

<br />

## 컨트롤러와 뷰

`{Project}/Controllers/HomeController.cs`  생성한다.  
컨트롤러 클래스를 만들때 는 항상 `접미사 Controller`를 붙여야한다.

예를들면  
Home 컨트롤러를 만들고 싶으면 => {Project}/Controllers/HomeController.cs  
About 컨트롤러를 만들고 싶으면 => {Project}/Controllers/AboutController.cs  

컨트롤러 안에 있는 메소드를 `Action` 이라고 한다.

```cs
namespace WebApplication8.Controllers
{
  public class HomeController : Controller
  {
    // GET
    public IActionResult Student() // Student Action 메소드
    {
      return View();
    }
  }
}
```

컨트롤러와 액션 메소드를 만들었으면 액션 메소드와 매칭되는 뷰를 만든다.  

{Project}/Views 라는 폴더를 생성한다.  
Views 폴더안에 컨트롤러와 매칭되는 폴더를 생성한다.  

예를들면  
HomeController를 만들어 두었으면 => {Project}/Views/Home  
AboutController를 만들어 두었으면 => {Project}/Views/About  

마지막으로 Home 폴더안에 컨트롤러에 정의 되어있는 Action 메소드와 매칭되는 `Razor 파일`을 생성한다.  
`{Project}/Views/Home/Student.cshtml`  

Razor 파일을 Action 메소드와 매칭되는 이름으로 작성한다.  

예를들면  
HomeController 파일안에 Student라는 액션 메소드가 정의되어 있으면  
=> {Project}/Views/Home/Student.cshtml  

레이저 파일은 확장자(`.cshtml`)에서도 알 수 있듯이 `c#코드가 html파일과 같이 렌더링되는 뷰 파일`이다.  

여기까지가 컨트롤러에서 뷰까지 도달하는 ASP.NET core MVC 규칙이다.  

다시 복습하면  
컨트롤러를 만들고 액션 메서드를 만든다.    
그리고 Views라는 폴더안에 컨트롤러와 매칭되는 폴더를 만들어야 한다.  
또 그 안에 Action 메소드와 매칭되는 Razor 파일을 만들어야 한다.  

<br />

## MVC 환경설정

asp.net core에서 MVC를 적용하기 위해서  
Startup class - `Configure 메소드`에 추가하는 `미들웨어`는 다음과 같다.  

```cs
app.UseMvc();
```

MVC에서는 url패턴을 보고 라우팅을 결정한다.  

코드를 다음과 같이 수정한다.

```cs
app.UseMvc(routes =>
{
  // /Home/Student 도 입력가능하다.  ({id?} 여기서 id 뒤에 ?를 붙임으로써 선태적 매개변수가 됬기 때문이다.)
  routes.MapRoute(name: "default", template: "{controller=Home}/{action=Student}/{id?}");
});
```

다음과 같이 name필드에 "default"를 입력하면, default 라우팅을 설정할 수 있다.  
default 라우팅은 첫 화면을 의미한다.  

예를들면 주소창에 http://localhost:5000을 입력한 경우  
http://localhost:5000/Home/Student로 이동되도록 설정한것이다.

미들웨어 설정을 완료했으면 해당 어플리케이션에서 전역으로 mvc 미들웨어가 적용되도록  
`ConfigureServices에 인자로 있는 services 컨테이너`에 mvc 미들웨서 서비스 의존성을 주입하여야 한다.  

```cs
public void ConfigureServices(IServiceCollection services)
{
  services.AddMvc();
}
```

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/)
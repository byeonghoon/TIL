# 12-MVC-EFC-Config

## Entity Framework Core

- 객체 관계형 매퍼(ORM: Object Relational Mapping)
- SQL 문법 탈피
- 독립형 데이터베이스
- Lightweight(가볍고), Extensible(확장가능), Cross Platform

`객체 관계형 매핑`은  
데이터베이스와 객체 지향 프로그래밍 언어간 호환되지 않는 데이터를 변환하는 프로그래밍 기법  

ORM이 없었을 때, 개발시 데이터베이스에 접근하기 위해서 SQL 문법을 꼭 알아야 했다.  
또한 데이터베이스에 굉장히 종속적이었다.  

ORM 등장이후 객체 지향적인 기법으로 데이터베이스와 커뮤니케이션이 가능해져서 SQL을 숙지않아도 되는것이 가능해졌다.  

그리고 구현 방법도 독립적으로 작성할 수 있게 되서  
객체에 집중함으로써 개발이 데이터베이스에 종속적이지 않게 되었다.  

정리를 하자면 객체지향 프로그래밍에서 데이터베이스와 연동할 때  
SQL 쿼리로 인한 코드 가독성 저하 문제 그리고 데이터베이스 관리를 독립하기위해 만들어진것이 ORM이다.  

Entity Framework Core은 이 ORM을 사용하였고  
전 버전인 Entity Framework보다 가볍고 확장가능하며 크로스 플랫폼 지원이 가능한 프레임워크이다.  

<br />

## Entity Framework Core를 위한 환경설정

프로젝트 환경 설정 파일을 연다.  
`{Project}.csproj`

```csproj
// {Project}.csproj

<Project Sdk="Microsoft.NET.Sdk.Web">
  <PropertyGroup>
    <TargetFramework>netcoreapp2.0</TargetFramework>
  </PropertyGroup>
  <ItemGroup>
    <Folder Include="wwwroot\" />
  </ItemGroup>
  <ItemGroup>
    <PackageReference Include="Microsoft.AspNetCore.All" Version="2.0.8" />
  </ItemGroup>
</Project>
```

`<TargetFramework>netcoreapp2.0</TargetFramework>`  
타겟하는 프레임워크는 .NET core 2.0을 사용한다는 의미이다.  

`<PackageReference Include="Microsoft.AspNetCore.All" Version="2.0.8" />`  
패키지는 Microsoft.AspNetCore.All을 사용한다는 의미이다.  

`<Folder Include="wwwroot\" />`
"wwwroot폴더가 루트 폴더이다"라는 의미이다.  

AspNetCore.All는 {Project}/Dependencies안에 위치하고 있으며  
클릭해보면 AspNetCore.All안에 수많은 패키지들이 인스톨되어 있는것을 확인 할 수 있다.  

Microsoft.AspNetCore.All 한 개만 참조하면 수많은 패키지들이 인스톨된다.  
설치되어 있는 패키지들 중에 Microsoft.EntityFramworkCore.SqlServer는  
Entitiy Framwork에서 SqlServer와 커뮤니케이션이 가능하게 해주는 패키지이다.  

SqlServer는 Microsoft에서 만들었기 때문에 default로 설치되어 있는 패키지이다.  
그 외 데이터베이스와 연동할 때는 `NuGet`을 통해 추가적으로 설치를 해줘야한다.  

우리는 SqlServer가 아닌 PostgreSQL을 사용할 것이기 때문에  
NuGet 패키지 관리자에서 아래 4개의 패키지를 인스톨한다.

- `"Npgsql.EntityFrameworkCore.PostgreSQL"`
- `"Microsoft.EntityFrameworkCore.Design"`
- `"Npgsql"`
- `"Npgsql.EntityFrameworkCore.PostgreSQL"`

그리고 {Project}.csproj를 확인하고 다음과 같이 안되어있으면 수정한다.

```csproj
// {Project}.csproj

<Project Sdk="Microsoft.NET.Sdk.Web">
  <PropertyGroup>
    <TargetFramework>netcoreapp2.0</TargetFramework>
  </PropertyGroup>
  <ItemGroup>
    <Folder Include="wwwroot\" />
  </ItemGroup>
  <ItemGroup>
    <PackageReference Include="Microsoft.AspNetCore.All" Version="2.0.8" />
    <PackageReference Include="Microsoft.EntityFrameworkCore.Design" Version="2.1.1" />
    <PackageReference Include="Npgsql" Version="4.0.2" />
    <PackageReference Include="Npgsql.EntityFrameworkCore.PostgreSQL" Version="2.1.1.1" />
  </ItemGroup>
</Project>
```

해당 프로젝트에서 터미널을 연다.

```bash
// ~/Developments/tutorial/c#/MyApp/WebApplication8

$ dotnet ef
```

여기서 `dotnet ef`를 입력해보면 현재 에러가 발생한다.  

dotnet ef 커맨드가 필요한 이유는 dotnet ef 커맨드를 이용해서  
데이터베이스를 만들고 그리고 데이터 관리도 할 수 있기 떄문이다.  
해당 명령어를 사용하게 하기 위해서는  
해당 프로젝트에서 닷넷 커맨드 라인 툴을 참조할 수 있도록 설정을 해줘야한다.  
{Project}.csproj 에 정의를 해주면된다.  

```csproj
// {Project}.csproj

<Project Sdk="Microsoft.NET.Sdk.Web">
  <PropertyGroup>
    <TargetFramework>netcoreapp2.0</TargetFramework>
  </PropertyGroup>
  <ItemGroup>
    <Folder Include="wwwroot\" />
  </ItemGroup>
  <ItemGroup>
    <PackageReference Include="Microsoft.AspNetCore.All" Version="2.0.8" />
    <PackageReference Include="Microsoft.EntityFrameworkCore.Design" Version="2.1.1" />
    <PackageReference Include="Npgsql" Version="4.0.2" />
    <PackageReference Include="Npgsql.EntityFrameworkCore.PostgreSQL" Version="2.1.1.1" />
  </ItemGroup>

  <!-- 추가 -->
  <ItemGroup>
    <DotNetCliToolReference Include="Microsoft.EntityFrameworkCore.Tools.DotNet" Version="2.0.0" />
  </ItemGroup>
</Project>
```

설정을 했으면 다시 컴맨드 라인에서 dotnet-ef를 실행시켜 보자  

```bash
// ~/Developments/tutorial/c#/MyApp/WebApplication8

$ dotnet ef
```

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
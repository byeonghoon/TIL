# 11-Razor-PartialView

## Partial View

만약 Index.cshtml과 Student.cshtml에서 공통적으로 사용하는 코드가 있으면 어떻게 해야 할까?  
이렇게 `공통으로 사용하는 뷰 코드가 있을 때 partial 뷰를 사용`한다.  

Student.cshtml에서 선생님 목록을 보여줬는데  
Index.cshtml에서도 선생님 목록을 보여줄려고 하면 어떻게 해야 할까??  
단순히 복사해서 붙여도 문제는 없다.  
하지만 동일한 코드를 재사용 하지 않으면 코드가 낭비된다.  
그래서 `재사용이 필요한 뷰를 따로 분리해서 만든 뷰가 partial 뷰`이다.  

`{Project}/Views/Home/_TeacherTable.cshtml` 파일을 만들자  
partial 뷰를 만들 때 `"_"` 언더바를 꼭 추가해주자  
이것은 partial 뷰를 의미하는 암묵적인 룰이다.  

Student.cshtml에서 선생님 목록을 보여주는 코드를 잘라내서 _TeacherTable.cshtml에 붙여넣자

```html
// {Project}/Views/Home/_TeacherTable.cshtml

@model StudentTeacherViewModel

<h3>Our Teachers</h3>
<table class="table">
  <thead>
    <tr>
      <th>Name</th>
      <th>Class</th>
    </tr>
  </thead>
  <tbody>
    @foreach (var teacher in Model.Teachers)
    {
      <tr>
        <td>@teacher.Name</td>
        <td>@teacher.Class</td>
      </tr>
    }
  </tbody>
</table>
```

Index.cshtml 에서도 선생님 목록을 보여줘야 되기 때문에  
StudentTeacherViewModel을 임포트 해줘야 한다.  

```html
// {Project}/Views/Home/Index.cshtml

@model StudentTeacherViewModel

@{
  ViewBag.Title = "Home";
}

<div class="container">
  <div class="jumbtron">
    <h1>환영합니다.</h1>
    <p>we are learning asp.net core</p>
  </div>
</div>
```

또한 Index.cshtml 에서 ViewModel을 사용할 수 있도록  
Home 컨트롤러에 있는 Index 액션 메소드에서  
StudentTeacherViewModel을 View로 전달할 수 있는 로직도 추가해야한다.  

```cs
// {Project}/Controllers/HomeController.cs

...

  public IActionResult Index()
  {
    List<Teacher> teachers = new List<Teacher>
    {
      new Teacher {Name = "세종대왕", Class = "한글"},
      new Teacher {Name = "이순신", Class = "해상전"},
      new Teacher {Name = "재갈량", Class = "지"},
      new Teacher {Name = "을지문덕", Class = "지상전략"}
    };
    
    var viewModel = new StudentTeacherViewModel
      {
        Student = new Student(),
        Teachers = teachers
      };

    return View(viewModel);
  }

  public IActionResult Student()
  {

...
```

이제 Index.cshtml과 Student.cshtml에서 Partial 뷰를 사용하겠다는 것을 정의해준다.

```html
// {Project}/Views/Home/Index.cshtml

@model StudentTeacherViewModel

@{
  ViewBag.Title = "Home";
}

<div class="container">
  <div class="jumbtron">
  <h1>환영합니다.</h1>
<p>we are learning asp.net core</p>
</div>
</div>

@await Html.PartialAsync("_TeacherTable", Model)
```

`Html.PartialAsync("_TeacherTable", Model)`를 사용해서 Partial 뷰를 보여준다.  

인자로 `내가 사용할 Partial 뷰`와 현재 뷰에서 사용하고 있는 `모델`을 넘긴다.  
모델은 옵션이라서 Partial 뷰에서 모델을 사용하고 있지 않으면 사용할 Partial 뷰만 정의해주면 된다.  

async await 키워드를 사용하면 혹시 모를 데드락을 막을 수 있다.

```html
// {Project}/Views/Home/Student.cshtml

@model StudentTeacherViewModel

...

@await Html.PartialAsync("_TeacherTable", Model)
```

이처럼 여러 페이지에서 동일하게 사용하는 뷰가 있으면  
해당 부분을 따로 때어내서 Partial 뷰를 만들고 사용한다.  
코드 재사용성과 유지보수성을 향상시킨다.  

현재 첫 화면은 Student 페이지로 렌더링 된다.
Home 페이지로 세팅되도록 변경을 해보자

Startup 클래스에서 Configure 메소드안에 있는 라우터 설정에서 "default" 설정을 변경하자.  

```cs
// {Project}/Startup.cs

...
app.UseMvc(routes =>
{
  routes.MapRoute(name: "default", template: "{controller=Home}/{action=Index}/{id?}");
});
```

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
# 09-Razor-ViewStart

## 뷰 스타트

`레이저 뷰 엔진이 뷰 파일을 렌더링하기전에 항상 먼저 하는것이 있다.`  

바로 뷰 스타트라는 파일이 존재하는지 안하는지 확인하고  
만약 존재하면 그 안에 있는 코드를 먼저 실행시킨다.  

뷰 스타트 파일을 사용하면 다른 뷰 파일에서 꼭 넣어야 될 코드를 안넣어도 된다.  
예를들어 현재 각각에 뷰 파일마다 레이아웃을 사용하는 코드가 정의되어있다.  
그런데 매번 뷰를 만들때 마다 레이아웃을 사용하는 코드를 사용하면 번거롭다.  
그래서 대신에 뷰 스타트파일에 레이아웃을 사용하는 설정을 넣어주면  
앞으로 뷰 파일에서 레이아웃을 사용하는 코드를 넣어줄 필요가 없다.  

뷰 스타트 파일을 만들어 보자

Views 폴더안에 `_ViewStart.cshtml` 파일을 만들자  
`{Project}/Views/_ViewStart.cshtml`  

```html
// {Project}/Views/_ViewStart.cshtml

@{
  Layout = "_Layout";
}
```

이 코드의 의미는 `모든 뷰 파일에서 앞으로 레이아웃 뷰를 사용하겠다는 의미`이다.  

이 코드를 사용했기 때문에
앞에서 모든 뷰파일에서 레이아웃 뷰를 사용하겠다고 추가해놨던 선언들을 모두 지워주면된다.  
Student.cshtml과 Index.cshtml 파일에 있는 선언들을 지워주자  

정상 동작 확인

`뷰 스타트를 통해 뷰에서 중복되게 사용하는 코드들을 방지 할 수 있다.`

질문  
Index.cshtml / Student.cshtml 파일에서 레이아웃 파일을 지정해줄 때는  
"../Shared/Layout.cshtml 로 지정을 했어야하는데, 왜 레이저 뷰 스타트 파일  
_ViewStart.cshtml 파일에서 레이아웃 파일을 지정할 때에는 확장자도 뺀  
"_Layout" 만으로도 지정이 되는 것인가요?  

답변  
Razor 뷰엔진에서는 전체경로(/Views/Shared/_Layout.cshtml)나  
부분이름(_Layout) 둘 중 하나를 인식할 수 있는데요.  
부분이름이 쓰였을 때는 뷰엔진이 알아서 해당 파일을 찾고 렌더링하게됩니다.  
그래서 뷰스타트파일에서 _Layout만 써도 지정이 되는거였구요.  
우리가 Home/Index.cshtml에서 파샬뷰를 썼을 때  
첫 번째 매개변수에 _TeacherTable만 명시해도 알아서 렌더링됐던 이유가 같은 이유에요.  
뷰엔진이 해당 이름의 파일을 알아서 찾는거죠.

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
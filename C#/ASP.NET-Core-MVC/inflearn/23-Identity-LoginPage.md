# 23-Identity-LoginPage

## Login Page

로그인 뷰도 회원가입 뷰와 똑같은 방식으로 구현한다.  

먼저 애플리케이션이 실행됬을 때 로그인 뷰가 첫 화면되도록 Startup 파일에서 default 뷰를 수정한다.  

```cs
public class Startup
{
  ...

  public void Configure(IApplicationBuilder app, IHostingEnvironment env, DbSeeder seeder)
  {
    ...

    app.UseMvc(routes => { routes.MapRoute(name: "default", template: "{controller=Account}/{action=Login}/{id?}"); });

    seeder.SeedDatabase().Wait();
  }
}
```

네비게이션 바에서 로그인 탭 추가한다.  

```html
 <!-- {Project}/Views/Shared/_Layout.cshtml -->

...

<ul class="nav navbar-nav pull-right">
  <li><a asp-controller="Account" asp-action="Register">회원가입</a></li>
  <li><a asp-controller="Account" asp-action="Login">로그인</a></li>
</ul>

...

```

AccountController에서 Login 액션 메소드를 만든다.  

```cs
// {Project}/Controllers/AccountController.cs

public class AccountController : Controller
{
  public IActionResult Register()
  {
    return View();
  }

  public IActionResult Login()
  {
    return View();
  }
}
```

로그인 뷰에서 사용할 뷰모델 클래스를 만든다.  

```cs
// {Project}/ViewModels/LoginViewModel.cs

public class LoginViewModel
{
  [Required(ErrorMessage = "작성이 필요한 필드입니다.")]
  [EmailAddress]
  public string Email { get; set; }

  [Required(ErrorMessage = "작성이 필요한 필드입니다.")]
  [DataType(DataType.Password)]
  public string Password { get; set; }
}
```

마지막으로 Login.cshtml을 만든다.  

```html
 <!-- {Project}/Views/Account/Login.cshtml -->

@model LoginViewModel

<div class="row">
  <div class="col-md-2 col-md-offset-5">
    <h2 style="text-align: center">로그인</h2>
    <hr />
    <form method="post" class="form-horizontal">
      <div class="form-group">
      <label asp-for="Email">이메일</label>
    <input asp-for="Email" class="form-control"/>
      <span asp-validation-for="Email" class="text-danger"></span>
    </div>
    <div class="form-group">
      <label asp-for="Password">비밀번호</label>
    <input asp-for="Password" class="form-control"/>
      <span asp-validation-for="Password" class="text-danger"></span>
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-info">로그인</button>
    </div>
    </form>
  </div>
</div>
```

앱을 실행시켜서 동작을 확인하자.  

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
# 14-MVC-EFC-Migration

## Migration

EntityFrameworkCore에서는 커맨드라인 인터페이스를 사용 해서 데이터베이스와 데이터베이스 스키마를 생성할 수 있다.  

다음 코드를 프로젝트 경로에서 실행하면 DbContext를 참조해서
c# 으로 작성된 데이터베이스 스키마를 만드는 코드를 생성한다. => 마이그레이션 코드를 생성

```bash
<!-- {Project} -->

$ dotnet ef migrations add {마이그레이션 이름}
```

만약 나중에 엔티티를 더 추가하거나 엔티티 안에있는 속성이 변경되었을 때  
다시 `dotnet ef migrations add`를 사용하면 변경된 마이그레이션 코드가 생성된다.(작성한 마이그레션 이름으로)  ㅊ

생성된 마이그레이션 코드를 사용해서 실제적으로 데이터베이스와 테이블을 만드는 코드는 다음과 같다.

```bash
<!-- {Project} -->

$ dotnet ef database update
```

위 코드를 실행시키면 최신으로 작성된 마이그레션 파일을 기반으로 데이터베이스와 테이블을 만든다.

작성한 마이그레이션 코드가 여러개 이면
update 뒤에 마이그레이션 파일 이름을 넣어서 해당 마이그레션에 맞는 데이터베이스와 테이블을 만들도록 할 수 있다.(스키마 변경)
코드는 다음과 같다.  

```bash
<!-- {Project} -->

$ dotnet ef database update {마이그레이션 파일 이름}
```

마이그레이션 커맨드를 사용하기 앞서 json파일을 만들것이다.  
`{Project}/appsettings.json` 파일을 생성한다.  
여기에 작성한 코드는 PostgreSQL 데이터베이스에 연결하는 필드 값이다.  
만약에 MS SQL 서버나, 다른 데이터베이스를 사용하는 경우 필드 값을 다르게 해야한다.  

```json
// {Project}/appsettings.json

{
  "ConnectionStrings": {
    "MyAppConnection": "Host=techand.io;Port=18319;Username=bhlee;Password=slow1234;Database=bhlee_test"
  }
}
```

이제 appsettings.json 설정해놓은 문자열을 Startup 클래스로 전달해야 한다.  
appsettings.json 파일을 불러오는 방법은 `IConfiguration 클래스`를 통해서 할 수 있다.  

Startup 클래스에 생성자를 생성하고 매개변수로 IConfiguration을 넘긴다.  
그리고 Startup 클래스내에서만 사용가능 하도록 private 필드에 할당한다.

MS SQL 서버를 사용하는 경우는 다음과 같이 설정한다.

```cs
services.AddDbContext<MyAppContext>(options =>
{
  options.UseSqlServer(_config.GetConnectionString("MyAppConnection"));
});
```

Postgresql 을 사용하는 경우는 다음과 같이 설정한다.

```cs
services.AddDbContext<MyAppContext>(options =>
{
  options.UseNpgsql(_config.GetConnectionString("MyAppConnection"));
});
```

GetConnectionString의 인자는 GetConnectionString 구조안에 있는 키를 통해서 값을 읽는다.  

ConfigureServices 함수안에서 안에서는 MyAppContext를 전역에서 사용하기 위한 설정을 해준다.  
현재 우리가 만든 DbContext는 MyAppContext 이다.  
MyAppDbContext를 애플리케이션 전역에서 사용할 수 있도록 서비스 컨테이너에 추가한다.  

```cs
{Project}/Startup.cs

public class Startup
{
  private readonly IConfiguration _config;

  public Startup(IConfiguration config)
  {
    _config = config;
  }

  public void ConfigureServices(IServiceCollection services)
  {
    services.AddDbContext<MyAppContext>(options =>
    {
      options.UseNpgsql(_config.GetConnectionString("MyAppConnection"));
    });

    services.AddMvc();
  }

  public void Configure(IApplicationBuilder app, IHostingEnvironment env)
  {
    if (env.IsDevelopment())
    {
      app.UseDeveloperExceptionPage();
    }

    app.UseStaticFiles();

    app.UseMvc(routes => { routes.MapRoute(name: "default", template: "{controller=Home}/{action=Index}/{id?}"); });
  }
}
```

이제 명령어를 실행해보자

```bash
<!-- {Project} -->

$ dotnet ef migrations add Initial
```

Initial 접미사를 가지고 있는 마이그레이션 파일을 생성한다는 의미이다.  

명령어를 수행하면 2개의 파일이 생성되는것을 확인 할 수 있다.  
`MyAppContextModelSnapshot.cs`는 현재 데이터베이스 스키마가 어떤 형식인지를 snapshot을 통해 기록한다.  

마이그레이션을 처음 만들었을 때만 이 파일이 생성되고  
`dotnet ef migrations add {마이그레이션 이름}` 커맨드가 실행되서  
스키마가 변경될 때 마다 MyAppContextModelSnapshot.cs파일은 갱신된다.

그다음 `20180723005307_Initial.cs`는 우리가 이름 붙인 Initial 파일이다.  
MyAppContext.cs에 추가해놓은 모델(Student, Teacher)가 맵핑된다.  

`모델에 선언해 놓은 데이터 애노테이션이 테이블에 제약으로 반영된다.`  
ex)

- [MaxLength(50)] => maxLength: 50
- [Required] => nullable: false

20180723005307_Initial.cs에는 두 개의 메소드가 정의되어 있다.  

- `Up메소드`는 데이터베이스 스키마를 upgrade 한다.
- `Down메소드`는 데이터베이스 스키마를 되돌리는 역할을 한다.  

우리가 두 번째 마이그레이션 커맨드를 실행했을 때  
`Up과 Down메서드를 보고 엔티티 프레임워크가 데이터베이스 스키마를 알맞게 변경시킨다.`  

지금부터 데이터베이스를 실제로 생성해본다.

이제 명령어를 실행해보자

```bash
<!-- {Project} -->

$ dotnet ef database update
```

`dotnet ef database update` 커맨드는 최신 버전의 마이그레이션 파일을 통해서 데이터베이스와 그리고 테이블을 생성한다.  
테이블이 생성된것을 확인하면 여러개의 테이블이 생성된것을 확인 할 수 있다.  
그 중 `_EFMi..History 테이블`은 `가장 최근에 갱신한 마이그레이션 파일`을 저장하는 테이블이다.  

때때로 데이터베이스 스키마를 변경해야되는 경우가 생긴다.  
예를들어 Teacher 엔티티에서 Country 필드를 추가하고, Student 엔티티에서 Country 필드가 필요없어진 경우  
다음과 같이 작업을 하면된다.  

먼저 모델을 변경한다.  
그리고 DbContext가 적용된 MyAppContext를 확인하고,  
`dotnet ef migrations add {파일이름}` 커맨드를 통해서 최신 버전의 마이그레이션 파일을 만들고  
`dotnet ef database update` 커맨드를 통해서 최신버전의 마이그레션 파일을 바탕으로 데이터베이스를 갱신한다.  

```bash
<!-- {Project} -->

$ dotnet ef migrations add Second
```

Done을 커맨든 화면에서 확인 후  
Migrations 폴더를 보면 두 번째 마이그레이션 파일이 생성된 것을 확인 할 수 있다.  
`20180723012557_Second.cs`  

먼저 Up 메소드를 보면  
이전 버전의 테이블에서 새롭게 작성된 모델을 바탕으로 한 테이블로  
변경되기 위해서 사용해야할 C#으로 작성된 SQL 문법이 있다.  

Down 메소드를 보면  
새롭게 작성된 모델을 바탕으로 한 테이블에서 이전 버전의 테이블로  
변경되기 위해서 사용해야할 C#으로 작성된 SQL 문법이 있다.  

마지막으로 MyAppContextModelSnapshot.cs를 보면 최신 상태로 갱신된 것을 확인 할 수 있다.  
자 이제 새롭게 작성된 모델을 테이블에 적용을 해보자  

```bash
<!-- {Project} -->

$ dotnet ef database update
```

테이블이 변경된 것을 확인 할 수 있었다.  

만약에 무슨일이 생겨서 그 전 스키마로 돌아가고 싶으면  
두번째 마이그레이션 코드에 해당 버전의 마이그레이션 이름을 붙여주면 된다.  

```bash
<!-- {Project} -->

$ dotnet ef database update 20180723005307_Initial
```

테이블이 이 전 스키마로 변경된것을 확인 할 수 있었다.  

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
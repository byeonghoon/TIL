# 02-Base-wwwroot

## wwwroot

### wwwroot의 역할

wwwroot의 폴더는 `웹서버의 루트`가 되는곳이다.  
`모든 정적 파일이담긴다.`  
(static 파일들(.html, .js, .css, .jpg, .png, ...)  
c#, Razor 등과 같은 파일이 담기면 안된다.

wwwroot 폴더에 index.html 파일을 작성한다.

```html
// {project}/wwwroot/index.html

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
  <h1>Hello World!</h1>
</body>
</html>
```

작성 후 서버를 실행시킨다.  

그리고 http://localhost:5000/index.html 에 접속하면 index.html이 보이지 않는다.

그 이유는 asp.net core에서는 정적 파일을 사용하는것이 default로 설정되어 있지 않기 때문이다.

이 문제를 해결하기 위해서는 StartUp 클래스 - `Configure 메소드`에 다음과 같은 미들웨어를 추가해줘야 한다.  

```cs
app.UseStaticFiles();
```

이 코드로 인하여 asp.net core는 wwwroot 폴더안에 있는 정적인 파일들을 인지하기 시작한다.  

다시 http://localhost:5000/index.html 에 접속하면 화면이 로드되는것을 확인할 수 있다.  
css, bootstrap, jquery 같은 파일들을 사용할 떄 모두 wwwroot안에 넣고 사용을 해야한다.

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/)
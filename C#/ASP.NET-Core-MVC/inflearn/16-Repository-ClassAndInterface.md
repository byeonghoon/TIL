# 16-Repository-ClassAndInterface

## Repository 패턴

- 많이 사용하는 디자인 패턴중 하나이다.  
- 비지니스 로직과 데이터 접근 레이어를 분리를 통해서 느슨한 결합을 추구한다.
- 데이터 접근 레이어를 인터페이스를 통해서 추상화 시키며 사용한다.  

이 패턴을 사용하지 않으면 애플리케이션 안에서 SQL 쿼리가 이곳 저곳 흩어져 있는 경우가 발생하고  
나중에 어떤 테이블에 필드가 추가되서, 쿼리를 수정하게 되면, 해당 일일히 쿼리문을 찾아서 수정하는 번거로운 일이 생긴다.  

하지만 Repository 패턴을 사용하면 데이터 접근 로직들을 한곳에 집중시킬 수 있어 유지보수가 용이하다.

Data폴더안에 Repositories 폴더를 만들고 그 안에 TeacherRepository 클래스 파일을 만든다.  
`{Project}/Data/Repositories/TeacherRepository.cs`  
TeacherRepository.cs 파일에 MyAppContext 서비스를 주입한다.  

```cs
// {Project}/Data/Repositories/TeacherRepository.cs

public class TeacherRepository
{
  private readonly MyAppContext _context;

  public TeacherRepository(MyAppContext context)
  {
    _context = context;
  }
}
```

TeacherRepository 클래스에서는 2가지의 메소드가 있다.  

- TeacherList를 불러오는 메소드
- id에 매칭되는 Teacher를 불러오는 메소드

TeacherList 메서드에서는 리턴 타입으로 List가 아닌 `IEnumerable`를 사용한다.  
IEnumerable과 List의 차이점은 어떤 `데이터를 읽어올 때 List보다 효율성면에서 뛰어나다`.

StudentTeacherViewModel.cs도 기존 List에서 IEnumerable로 수정한다.  

```cs
// {Project}/ViewModels/StudentTeacherViewModel.cs

public class StudentTeacherViewModel
{
  public Student Student { get; set; }
  public IEnumerable<Teacher> Teachers { get; set; }
}
```

```cs
// {Project}/Data/Repositories/TeacherRepository.cs

public class TeacherRepository
{
  private readonly MyAppContext _context;

  public TeacherRepository(MyAppContext context)
  {
    _context = context;
  }

  public IEnumerable<Teacher> GetAllTeachers()
  {
    var result = _context.Teachers.ToList();

    return result;
  }

  public Teacher GetTeacher(int id)
  {
    var result = _context.Teachers.Find(id);

    return result;
  }
}
```

코드에서 보면 알수 있듯이 데이터베이스에서 값을 읽어오면 엔티티로 맵핑되서 애플리케이션 코드에서 바로 사용 할 수 있다.  

여기까지 간단한 데이터를 읽어오는 함수를 만들었다.  
이제 이 레포지터리를 인터페이스로 노출 시키는 단계가 필요하다.

구현된 인터페이스는 다음과 같다.

```cs
// {Project}/Data/Repositories/ITeacherRepository.cs

public interface ITeacherRepository
{
  IEnumerable<Teacher> GetAllTeachers();

  Teacher GetTeacher(int id);
}
```

레파지토리 클래스에서도 해당 인터페이스를 구현하도록 코드를 변경한다.  

```cs
// {Project}/Data/Repositories/TeacherRepository.cs

public class TeacherRepository : ITeacherRepository
{
  private readonly MyAppContext _context;

  public TeacherRepository(MyAppContext context)
  {
    _context = context;
  }

  public IEnumerable<Teacher> GetAllTeachers()
  {
    var result = _context.Teachers.ToList();

    return result;
  }

  public Teacher GetTeacher(int id)
  {
    var result = _context.Teachers.Find(id);

    return result;
  }
}
```

인터페이스를 사용하는 이유는  
인터페이스에 정의된 메소드들을 통해 쉽게 레파지토리에 구현되어 있는 메소드들을 확인 할 수 있다.  
또한 레포지터리를 테스팅 할 때, 해당 인터페이스를 상속받으면 레포지토리를 쉽게 테스트 할 수 있다.  

이제 이 레포지터리를 제대로 사용하기 위해서 Startup에 서비스 컨테이너에 등록한다.  

서비스 컨테이너에 의존성을 주입하는 다음과 같은 3가지 방법이 있다.  

- `AddTransient`
- `AddScoped`
- `AddSingleton`

먼저 `AddTransient` 살펴보면  
AddTransient는 `필요할 때 마다 생성되고, 캐쉬내에 머물러있지 않은 서비스`를 이용할 때 사용하는 의존성 주입방법이다.
Transient는 새로운 HTTP 요청이 있을 때 마다 새로운 인스턴스를 매번 생성한다.  
AddTransient가 호출되는 시점에 인스턴스가 동작하고 바로 삭제된다.

다음으로 `AddScoped` 살펴보면  
AddScoped<ITeacherRepository, TeacherRepository>는
HTTP 요청이 들어왔을 때, ITeacherRepository를 사용하는 곳을 만나면
TeacherRepository 인스턴스를 생성하고 그리고 HTTP 요청이 끝나면 이 인스턴스를 버린다.
그런데 HTTP 요청이 끝나기 전이면 만들었던 TeacherRepository 인스턴스를 재사용 할 수 있다.
Scoped는 데이터 접근 컴포넌트에서 많이 사용된다.  
services.AddScoped<ITeacherRepository, TeacherRepository>();에 의미는  
첫번째 인자 ITeacherRepository는 앞으로 서비스로서 애플리케이션 안에서 쓸 수 있게하고  
두번째 인자 TeacherRepository는 TeacherRepository가 ITeacherRepository의 구현화된 클래스로서 사용하라는 의미이다  

마지막으로 `AddSingleton` 살펴보면  
`Singlton은 애플리케이션에 생명주기 동안 단 한번만 인스턴스를 생성한다.`  
HTTP 요청이 있을때 마다 똑같은 인스턴스를 계속 사용하게 한다.  
그래서 Singleton은 주로 속성이 변하지 않는 정적인 데이터를 메모리 내에 저장해서 의존성주입이 필요할 때 사용하게 된다.  

결국에는 내가 필요로 하는 서비스를 의존성 주입으로 등록할 때  
`Transient`, `Scoped`, `Singleton` 중 어떤것이 맞는지 결정해서 사용하면 된다.  

```cs
// {Project}/Startup.cs

public class Startup
{
  private readonly IConfiguration _config;

  public Startup(IConfiguration config)
  {
    _config = config;
  }

  public void ConfigureServices(IServiceCollection services)
  {
    services.AddDbContext<MyAppContext>(options =>
    {
      options.UseNpgsql(_config.GetConnectionString("MyAppConnection"));
    });

    services.AddTransient<DbSeeder>();
    services.AddScoped<ITeacherRepository, TeacherRepository>();
    services.AddMvc();
  }
  ...
}
```

서비스 컨테이너에 TeacherRepository를 등록 하였으므로 컨트롤러에서 레파지토리를 사용해 보자  
컨토럴러에서 의존성 주입 후 레파지토리를 사용하자  

```cs
// {Project}/Controllers/HomeController.cs

public class HomeController : Controller
{
  private readonly ITeacherRepository _teacherRepository;

  public HomeController(ITeacherRepository teacherRepository)
  {
    _teacherRepository = teacherRepository;
  }

  public IActionResult Index()
  {
    var teachers = _teacherRepository.GetAllTeachers();

    var viewModel = new StudentTeacherViewModel
    {
      Student = new Student(),
      Teachers = teachers
    };

    return View(viewModel);
  }
  ...
}
```

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
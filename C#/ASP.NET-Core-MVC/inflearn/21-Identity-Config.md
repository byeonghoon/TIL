# 21-Identity-Config

## Identity 환경설정

닷넷 코어 아이덴티티는 `맴버쉽 시스템`이다.  
계정을 만들고 아이디와 비밀번호를 통해 로그인하거나  
페이스북 또는 구글과 같은 외부 로그인 제공자를 통해 로그인 할 수 있게 해주는 시스템이다.  

우리가 일일히 회원가입 시스템을 작성하지 않아도  
닷넷 코어 아이덴티티 시스템을 통하여 쉽게 맴버쉽 시스템을 구축할 수 있다.  

이번 단원에서는 아이덴티티 시스템을 사용할 수 있도록 기본적인 환경설정을 진행한다.  

먼저 Startup 클래스 파일에 있는 ConfigureServices 메소드 안에 `AddIdentity` 메소드를 추가한다.  
AddIdentity는 2개의 제네릭 인자 형식이 필요하다.  
첫 번째 앞으로 우리가 만들 엔티티 클래스(User 테이블) => ApplicationUser  
두 번째 인자는 Role 여기는 Identity 클래스에서 제공하는 Role 클래스를 사용할 것이다.  

AddIdentity 메소드는 옵션을 줄 수 있다.  
예를들어 페스워드 설정, 계정 잠금 설정, 쿠키 설정등  

지금 우리는 패스워드의 길이를 6글자로 제한하는 설정만 한다.  
그리고 User 데이터를 어디서 가지고 올 것인가를 할 수 있는 메소드를 추가한다.  
`.AddEntityFrameworkStores<MyAppContext>()`  의미는  
MyAppContext에서 User 데이터를 가지고 온다는 의미이다.  

그러기 위해서는 MyAppContext에서 IdentityContext를 상속을 받아야한다.  

이 부분은 조금 이따가 작업을 진행한다.  

```cs
services.AddIdentity<ApplicationUser, IdentityRole>(options => {
       options.Password.RequiredLength = 6;
     }).AddEntityFrameworkStores<MyAppContext>();
```

다음르로 Startup 클래스 파일에 있는 Configure 메소드 안에 `app.UseAuthentication()` 를 추가한다.  
의미는 `애플리케이션에서 인증 서비스를 활성화시키겠다`는 의미이다.  
중요한것은 UseMvc() 위에 설정을 해줘야한다.  
Configure 메소드의 파이프라인의 동작이 위에서 아래순으로 진행되기 때문이다.  
순서가 바뀌면 에러가 발생한다.  

```cs
public class Startup
{
  ...

  public void ConfigureServices(IServiceCollection services)
  {
    services.AddDbContext<MyAppContext>(options =>
    {
      options.UseNpgsql(_config.GetConnectionString("MyAppConnection"));
    });

    services.AddIdentity<ApplicationUser, IdentityRole>(options => {
       options.Password.RequiredLength = 6;
     }).AddEntityFrameworkStores<MyAppContext>();

    services.AddTransient<DbSeeder>();

    ...
  }

  public void Configure(IApplicationBuilder app, IHostingEnvironment env, DbSeeder seeder)
  {
    ...

    app.UseStaticFiles();

    app.UseAuthentication();
  
    app.UseMvc(routes => { routes.MapRoute(name: "default", template: "{controller=Home}/{action=Index}/{id?}"); });

    ...
  }
}
```

다음으로 User 엔티티를 작성한다. 이름은 ApplicationUser로 한다.  

```cs
// {Project}/Models/ApplicationUser.cs

public class ApplicationUser: IdentityUser
{
  public string FullName { get; set; }
  public string Gender { get; set; }
}
```

`IdentityUser`는 .NET Identity 에서 제공하는 User Class 이다.  
IdentityUser 클래스에는 User와 관련된 대부분의 필드가 있다.  
하지만 내가 원하는 필드가 IdentityUser 클래스에 없는 경우도 있기 때문에  
그래서 ApplicationUser 클래스를 만들고 IdentityUser를 상속받고  
내가 사용하는 필드를 ApplicationUser에 작성하는 것이다.  

마지막으로 MyAppContext에서 IdentityContext를 상속 받는 작업을 할 것이다.  
전에는 DbContext를 상속받았다. 이제는 `IdentityContext`를 상속받을 것이다.  

DbContext를 IdentityDbContext로 변경한다.  
이렇게하면 MyAppContext 하나에 클래스로 유저와 유적의 역할 관리까지 다 할 수 있다.  

```cs
// {Project}/Data/MyAppContext.cs

public class MyAppContext : IdentityDbContext<ApplicationUser>
{
  public MyAppContext(DbContextOptions options) : base(options)
  {
  }

  public DbSet<Student> Students { get; set; }

  public DbSet<Teacher> Teachers { get; set; }
}
```

대규모 시스템에서는 DbContext와 IdentityDbContext 상속받는 파일을 분리해서 관리하는 경우도 있다.  
하지만 작은 시템에서는 IdentityDbContext 한 개로 관리하는게 더 편리하다.  

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
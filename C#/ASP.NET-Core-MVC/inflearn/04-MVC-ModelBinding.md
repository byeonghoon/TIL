# 04-MVC-ModelBinding

## 모델 바인딩

뷰에서 작성한 데이터를 컨트롤러로 보낼 때 `모델 바인딩`을 통해서 쉽게 할 수 있다.  
뷰에 있는 폼에서 HTTP Post 요청한것을 Action 메소드에서  
`복합 유형 매개변수 속성`으로 MVC 프레임워크가 자동으로 바꿔준다.  

`{Project}/Models/Student.cs` 파일을 생성한다.  

Student.cs 파일안에 Student.cshtml에 있는 `input 필드와 매칭되는 속성`들을 넣어준다.

```cs
// {project}/Models/Student.cs

public class Student
{
  public string Name { get; set; }
  public int Age { get; set; }
  public string Country { get; set; }
}
```

컨트롤러에서 HTTP post 특성이 있는 Action 함수를 만들자

```cs
// {project}/Controllers/HomeController.cs

...

  public IActionResult Student()
  {
    return View();
  }

  [HttpPost]
  public IActionResult Student(Student model) // 디버거 모드로 실행 후 model을 확인해 보면 "복합 유형 매개변수"로 바뀐것을 확인 할 수 있다.
  {
    return View();
  }

...
```

`모델 바인딩`을 통해서 할 수 있는것들 중에 하나는  
허가되지 않는 데이터를 서버로 전송하는 것을 막을 수 있다.  
`Bind`라는 어트리뷰트를 이용하는 것이다.

```cs
// {project}/Controllers/HomeController.cs

...

  [HttpPost]
  public IActionResult Student([Bind("Name", "Age")] Student model)
  {
    return View();
  }

...
```

위 예제를 확인하기 위해서 Html 폼에서 3개 필드 모두 작성하고 전송하였다.  
디버거 모드로 model 인자에 잡힌 복합 유형 매개변수를 확인해보면 결과 다음과 같다.  
`(Age: 15, Country: null, Name: "Kim")`  

서버에서 `[Bind]`를 통해 받고 싶은 값만 받을 수 있다.

반대로 제외하고 싶은 데이터만 설정할 때는
`BindNever`라는 어트리뷰트를 사용한다.  

Student 모델 클래스에 다음과 같이 추가하면 된다.

```cs
// {project}/Models/Student.cs

public class Student
{
  [BindNever]
  public string Name { get; set; }
  public int Age { get; set; }
  public string Country { get; set; }
}
```

```cs
// {project}/Controllers/HomeController.cs

...

  [HttpPost]
  public IActionResult Student(Student model)
  {
    return View();
  }

...
```

위 예제를 확인하기 위해서 Html 폼에서 3개 필드 모두 작성하고 전송하였다.  
디버거 모드로 model 인자에 잡힌 복합 유형 매개변수를 확인해보면 결과 다음과 같다.  
`(Age: 15, Country: "Korea", Name: null)`  
서버에서 `[BindNever]`를 통해 제외하고 싶은 데이터만 설정할 수 있다.

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/)
# 22-Identity-RegisterPage

## Register Page

회원가입 페이지 만들기 전에 `Controller/HomeController.cs` 에서  
public IActionResult Student() 약션 메소드에 `[Authorize]` 추가를 한다.  

```cs
public class HomeController : Controller
  {
    ...

    [Authorize]
    public IActionResult Student()
    {
      var students = _studentRepository.GetAllStudents();

      var viewModel = new StudentTeacherViewModel
      {
        Student = new Student(),
        Students = students
      };

      return View(viewModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Student(StudentTeacherViewModel model)
    {
      ...
    }
    ...
```

앱을 잠깐 실행시켜 보자  
Student 탭을 클릭했을 때 이상한 페이지로 Redirect 되는 것을 확인했다.  
Student 액션 메소드에 [Authorize]를 붙임으로써  
허가 되지 않은 User는 Student에 접근 할 수 없게 됬다. 오직 로그인된 유저만 접근할 수 있게 됬다.  

네비게이션바 오른쪽 끝편에 회원가입 탭을 추가하자

```html
 <!-- {Project}/Views/Shared/_Layout.cshtml -->

...
<ul class="nav navbar-nav">
  <li><a asp-controller="Home" asp-action="Index">Home</a></li>
  <li><a asp-controller="Home" asp-action="Student">Student</a></li>
</ul>
<ul class="nav navbar-nav pull-right">
  <li><a asp-controller="Account" asp-action="Register">회원가입</a></li>
</ul>

...
```

다음으로 AccountController를 추가하자

```cs
// {Project}/Controllers/AccountController.cs

public class AccountController : Controller
{
  public IActionResult Register()
  {
    return View();
  }
}
```

그리고 AccountController에 있는 Register 액션 메소드와 매칭되는 Razor 파일을 만든다.  
Views에 Account 폴더를 만들고 Register.cshtml 파일을 추가한다.  

```html
<!-- {Project}/Views/Account/Register.cshtml -->

@model ApplicationUser

<div class="row">
  <div class="col-md-2 col-md-offset-5">
    <h2 style="text-align: center">회원 가입</h2>
    <hr />
    <form method="post" class="form-horizontal">
      <div class="form-group">
        <label asp-for="Email">이메일</label>
        <input asp-for="Email" class="form-control"/>
        <span asp-validation-for="Email" class="text-danger"></span>
      </div>
      <div class="form-group">
        <label asp-for="FullName">이름</label>
        <input asp-for="FullName" class="form-control"/>
        <span asp-validation-for="FullName" class="text-danger"></span>
      </div>
      <div class="form-group">
        <label asp-for="Gender">성별</label>
        <input asp-for="Gender" class="form-control"/>
        <span asp-validation-for="Gender" class="text-danger"></span>
      </div>
      <div class="form-group">
        <label asp-for="Password">비밀번호</label>
        <input asp-for="Password" class="form-control"/>
        <span asp-validation-for="Password" class="text-danger"></span>
      </div>
      <div class="form-group">
        <label asp-for="ConfirmPassword">비밀번호 확인</label>
        <input asp-for="ConfirmPassword" class="form-control"/>
        <span asp-validation-for="ConfirmPassword" class="text-danger"></span>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-info">등록</button>
      </div>
    </form>
  </div>
</div>
```

먼저 각 필드에서 ApplicationUser에 있는 속성을 사용 할 때  
@Model.FullName 과 같은 방법을 사용하지 않고 바로 ApplicationUser 모델에 있는 필드(FullName)를 사용하였다.  
`요즘은 @Model.FullName 이렇게 사용하지 않고 바로 필드를 사용하는 추세이다.`

그리고 뷰안에서 엔티티를 바로 사용하면 좋은 Practice가 아니다.  
엔티티에 있는 속성들이 최대한 뷰안에서 밝혀지면 안된다.  
왜냐하면 회원가입 요청을 통해서 서버로 데이터를 전송할 테이블에 필드가 다 노출 될 수 있다.  
`최대한 뷰에서 사용하는 필드만 뽑아서 ViewModel로 만들고 뷰에서는 ViewModel을 사용해야 한다.`  

그래서 Register.cshtml 파일에서 ApplicationUser 모델을 쓰지 않고  
RegisterViewModel을 따로 만들어서 사용할 것이다.  
Register.cshtml 에서 작성한 @model ApplicationUser를  @model RegisterViewModel 로 변경한다.  

```html
<!-- {Project}/Views/Account/Register.cshtml -->

@model RegisterViewModel

...
```

그리고 {Project}/ViewModels/RegisterViewModel.cs 파일을 만든다.  

```cs
// {Project}/ViewModels/RegisterViewModel.cs

public class RegisterViewModel
{
  // Required가 실패했을 때는 여태까지 deafult ErrorMessage를 사용하였다. 그런데 필드를 통하여 원하는 ErrorMessage를 설정할 수 있다.
  [Required(ErrorMessage = "작성이 필요한 필드입니다.")]
  [EmailAddress] // Email 포맷으로 사용할 수 있도록 설정
  public string Email { get; set; }

  [Required(ErrorMessage = "작성이 필요한 필드입니다.")]
  public string FullName { get; set; }

  [Required(ErrorMessage = "작성이 필요한 필드입니다.")]
  public string Gender { get; set; }

  [Required(ErrorMessage = "작성이 필요한 필드입니다.")]
  [DataType(DataType.Password)] //Html 파일에서 input 태그에 input 타입을 Password 타입으로 하느것과 동일하다. 이렇게 하면 Html 파일에서 input 태그에 input 타입을 안적어 줘도 된다.
  public string Password { get; set; }

  [DataType(DataType.Password)]
  [Compare("Password", ErrorMessage = "비밀번호가 서로 일치하지 않습니다.")] //위에 Password 필드와 비교를 해줌. 
  public string ConfirmPassword { get; set; }
}
```

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
# 10-Razor-ViewImport

## 뷰 임포트

런타임 때 뷰 엔진이 뷰 파일을 컴파일하기 이전에  
뷰 안에있는 html, c#, TagHelpers들이 올바르게 동작 할지를 쭉 훌터본다.  
그러면서 찾아보는 파일이 `_ViewImports.cstml` 파일이다.

_ViewImports.cstml 파일을 확인해보면 예전에 작성해둔 코드가 있다.

```html
// {Project}/View/_ViewImports.cshtml

@addTagHelper *, Microsoft.AspNetCore.Mvc.TagHelpers
```

`@addTagHelper`를 사용함으로써 모든 레이저 파일에서 TagHelpers 를 쓸 수 있게해준다.

Student.cshtml을 보면

```html
// {Project}/Views/Home/Student.cshtml

@using WebApplication8.ViewModels
@model StudentTeacherViewModel

@{
  ViewBag.Title = "Student";
}

<h3>Add New Student</h3>
<form class="form from-horizontal" method="post">
...
```

StudentTeacherViewModel 클래스를 사용하기 위해서  
@using WebApplication8.ViewModels 와 같은 네임스페이스를 임포트하는 코드를 추가하였다.  

만약에 어떤 뷰 파일을 추가했을 때  
그 뷰 파일에서 StudentTeacherViewModel 클래스를 또 사용한다면  
StudentTeacherViewModel 클래스가 어느 네임스페이스에 정의되어 있는지  
다음 코드를 또 추가해줘야 한다.  

```cs
@using WebApplication8.ViewModels  
```

그런데 매번 이러면 굉장히 번거럽다

그래서 `_ViewImports.cshtml` 파일에  
StudentTeacherViewModel 클래스가 어느 네임스페이스에 정의되어 있는지  
네임스페이스를 추가해놓면 해당 뷰에서 따로 네임스페이스를 따로 정의할 필요가 없다.  

_ViewImports.cshtml 파일에 `@using WebApplication8.ViewModels 를 추가`하고  
Student.cshtml 파일에서 `@using WebApplication8.ViewModels 를 삭제`하자  

```html
// {Project}/Views/_ViewImports.cshtml

@addTagHelper *, Microsoft.AspNetCore.Mvc.TagHelpers
@using WebApplication8.ViewModels
```

`_ViewImports.cshtml` 파일에서는  
Razor 뷰에서 TagHelpers를 사용할 수 있게, ViewModel들을 사용할 수 있도록  
네임스페이스를 임포트 해놓으는 것이다.  

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
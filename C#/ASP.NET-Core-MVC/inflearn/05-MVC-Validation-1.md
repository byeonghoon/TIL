# 05-MVC-Validation-1

## 유효성 검사 1

사용자가 입력합 값이 유효한 값인지 검사하는것을 `Validation` 이라고 한다.  
asp.net core에서는 `데이터 에노테이션을 사용해서 유효성을 확인`할 수 있다.  

`데이터 에노테이션은 모델 클래스에 적용할 수 있다.`  

Name 필드는  

- 반드시 있어야한다.  
- 최대 길이를 50으로 한다.  
  
Age 필드는

- 15 ~ 70 사이여야 한다.  

Country 필드는

- 반드시 있어야한다.
- 최소 길이를 5로 한다.

다음과 같이 Student 모델 클래스에 데이터 에노테이션을 적용하면 된다.

```cs
// {project}/Models/Student.cs

public class Student
{
  [Required]
  [MaxLength(50)]
  public string Name { get; set; }
  [Range(15, 70)]
  public int Age { get; set; }
  [Required, MinLength(5)]
  public string Country { get; set; }
}
```

데이터 에노테이션을 적용할 때

다음과 같이 따로 나눠서 사용할 수도 있고  

```cs
[Required]
[MaxLength(50)]
public string Name { get; set; }
```

다음과 같이 연결해서 사용할 수 도 있다.

```cs
[Required, MinLength(5)]
public string Country { get; set; }
```

따로 나눠서 사용하는것이 더 직관적 이다.

모델(데이터 애노테이션이 적용된)이 바인딩된 값이 컨트롤러로 넘어왔을 때  
컨트롤러에서 해당 모델의 유효성을 체크해주는 코드는 다음과 같다.  

```cs
ModelState.IsValid
```

모델의 값이 유효성을 통과했으면 ModelState.IsValid는 True를 리턴하고  
모델의 값이 유효성을 통과하지 못 했으면 ModelState.IsValid는 False를 리턴한다.  

컨트롤러에서는 다음과 같은 방법으로 ModelState.IsValid를 사용한다.

```cs
// {project}/Controllers/HomeController.cs

...

  [HttpPost]
  public IActionResult Student(Student model)
  {
    if (!ModelState.IsValid)
    {
      // 에러를 보여준다.
    }

    // model 데이터를 데이터베이스에 저장한다.

    return View();
  }

...

```

그런데 Model을 유효성을 통과하지 못했을 때  
그 페이지로 돌아가서 사용자가 무엇을 잘못했는지 화면에 보여줘야 한다.  

이럴때 asp.net core MVC 프레임워크에서 제공되는 `TagHelpers`라는 기능을 사용하면  
TagHelpers가 ModelState 데이터 구조와 같이 일을 하면서  
어떤 필드에서 에러가 났는지 사용자에게 메시지를 보여줄 수 있다.  

Student.cshtml 파일로 이동해서 각 인풋 필드마다 TagHelpers 기능을 적용해 보자  

각 인풋 필드 밑에 아래 코드를 적용하자

```html
<span asp-validation-for=""></span>
```

모델 클래스에 있는 필드와 매칭되는 이름 값으로 적용한다.  

```html
// {project}/Views/Home/Student.cshtml

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<h3>Add New Student</h3>
<form class="form from-horizontal" method="post">
  
  <div class="form-group">
    <div class="col-md-3">
      <label>Name:</label>
      <input type="text" class="form-control" name="name"/>
      <span asp-validation-for="Name"></span>
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-3">
      <label>Age:</label>
      <input type="text" class="form-control" name="age"/>
      <span asp-validation-for="Age"></span>
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-3">
      <label>Country:</label>
      <input type="text" class="form-control" name="country"/>
      <span asp-validation-for="Country"></span>
    </div>
  </div>

  <button type="submit" class="btn btn-default">Submit</button>
</form>
```

`asp-validation-for` 를 사용하기 위해서는 또 다른 파일 한개를 추가해야 한다.  
`{project}/Views/_ViewImports.cshtml` 파일을 추가한다.  
_ViewsImports.cshtml 안에 TagHelpers를 사용 가능하게 하는 로직을 작성한다.  

```cshtml
// {project}/Views/_ViewImports.cshtml

@addTagHelper *, Microsoft.AspNetCore.Mvc.TagHelpers
```

이렇게 추가하면 Views 폴더안에 있는 Razor 파일에서 TagHelpers를 사용가능하게 해준다.  

이렇게 적용하면 Student.cshtml에서 `asp-validation-for`필드에 Value들이 인식안되는 문제가 발생한다.  
왜냐하면 `해당 Value들이 어떤 모델 클래스에 들어있는 필드인지를 모르기 때문이다.`  

이때 해당 모델을 사용하는 Razor 파일에  
다음과 같이 `모델이 있는 경로를 설정해주고 사용할 모델을 정의해주면된다.`  

소스코드를 보면 다음과 같다.

```html
// {project}/Views/Home/Student.cshtml

@using {Project}.Models
@model Student

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<h3>Add New Student</h3>
<form class="form from-horizontal" method="post">
  <div class="form-group">
    <div class="col-md-3">
      <label>Name:</label>
      <input type="text" class="form-control" name="name"/>
      <span asp-validation-for="Name"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>Age:</label>
      <input type="text" class="form-control" name="age"/>
      <span asp-validation-for="Age"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>Country:</label>
      <input type="text" class="form-control" name="country"/>
      <span asp-validation-for="Country"></span>
    </div>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
```

여기서 사용한 `@` 기호는  
`Razor 뷰 파일에서 c# 코드를 사용하게 해주는 접두사`이다.  

그래서 Razor 파일에서 c# 코드를 사용할 떄는 @ 기호를 붙여야 한다.

기존 코드에서 `TagHelpers`를 적용해서 컨트롤러로 값을 보내보자.  

Student.cshtml 에서 모든 인풋 필드에 name 속성을 `asp-for` 로 변경하였다.

```html
// {project}/Views/Home/Student.cshtml

@using WebApplication8.Models
@model Student

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<h3>Add New Student</h3>
<form class="form from-horizontal" method="post">
  <div class="form-group">
    <div class="col-md-3">
      <label>Name:</label>
      <input type="text" class="form-control" asp-for="name"/>
      <span asp-validation-for="Name"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>Age:</label>
      <input type="text" class="form-control" asp-for="age"/>
      <span asp-validation-for="Age"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>Country:</label>
      <input type="text" class="form-control" asp-for="country"/>
      <span asp-validation-for="Country"></span>
    </div>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
```

그런데 변경 이후에 `asp-for=""` 부분이 모두 빨간색으로 변하였다.  
이유는 Student 모델 클래스와 필드명이 일치하지 않아서 그렇다.  

Student 모델 클래스와 동일한 필드명으로 변경하자  

```html
// {project}/Views/Home/Student.cshtml

@using {Project}.Models
@model Student

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<h3>Add New Student</h3>
<form class="form from-horizontal" method="post">
  <div class="form-group">
    <div class="col-md-3">
      <label>Name:</label>
      <input type="text" class="form-control" asp-for="Name"/>
      <span asp-validation-for="Name"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>Age:</label>
      <input type="text" class="form-control" asp-for="Age"/>
      <span asp-validation-for="Age"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>Country:</label>
      <input type="text" class="form-control" asp-for="Country"/>
      <span asp-validation-for="Country"></span>
    </div>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
```

asp-for TagHelpers를 사용하면 인풋 테그에 있는 id와 name 속성을 자동으로 만들어 준다.  
동작시키고 크롬 개발자 도구에서 인풋 테그에 속성을 확인해 보자.  

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/)
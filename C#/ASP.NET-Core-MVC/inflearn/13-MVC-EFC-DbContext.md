# 13-MVC-EFC-DbContext

## DbContext

엔티티 프레임워크에서는 `DbContext`라는 베이스 클래스를 제공한다.  
DbContext라는 클래스를 상속받아서 사용하면 데이터베이스에 접근할 수 있게 도와준다.  
이전에 Student와 Teacher 모델 클레스가  
데이터베이스에 있는 Student와 Teacher 테이블을 나타낸다고 했다.  
이 두 모델은 엔티티라고도 한다.  
이 두 엔티티를 DbCotext를 상속받은 클래스에 추가하면 각각에 테이블로 만들어진다.  

Data라는 폴더를 만들고 그 안에 MyAppContext.cs 라는 클래스 파일을 만든다.  

여기서 MyAppContext 클래스가 DbContext를 상속 받을 수 있도록 설정한다.  
그리고 생성자를 만든다. 생성자의 매개변수로 `DbcontextOptions`라는 것을 설정하고  
이 매개변수를 상속받은 DbContext의 생성자로 넘긴다.  
base(DbContext)클래스에서 데이터베이스 커넥션 담당 및 기타 환경 설정을 하게 해준다.

```cs
// {Project}/Data/MyAppContext.cs

public class MyAppContext : DbContext
{
  public MyAppContext(DbContextOptions options) : base(options)
  {
  }
}
```

다음으로 우리가 만든 Student와 Teacher 클래스를  
데이터데이스에 테이블과 매핑시키는 작업을 한다.  

그전에 우리 Student와 Teacher 클래스에 Id 속성을 추가한다.  
추가하는 이유는 테이블에서 id가 존재하기 때문이다.  
모델 클래스에 있는 각각의 속성들이 DbContext를 통해서 데이터베이스 테이블에 필드가 된다.  
필드가 매칭되지 않으면 에러가 발생한다.  

```cs
// {Project}/Models/Teacher.cs

public class Teacher
{
  public int Id { get; set; }
  public string Name { get; set; }
  public string Class { get; set; }
}
```

```cs
// {Project}/Models/Student.cs

public class Student
{
  public int Id { get; set; }
  [Required]
  [MaxLength(50)]
  public string Name { get; set; }
  [Range(15, 70)]
  public int Age { get; set; }
  [Required, MinLength(5)]
  public string Country { get; set; }
}
```

다시 MyAppContext.cs 파일로 와서 모델 클래스를 테이블로 매핑하는 작업을 한다.  
`DbSet`이라는 속성을 만들고 타입을 해당 모델 클래스를 적어준다.  
DbSet을 통해 데이터베이스에 해당 테이블로 만들어진다.  

```cs
// {Project}/Data/MyAppContext.cs

public class MyAppContext : DbContext
{
  public MyAppContext(DbContextOptions options) : base(options)
  {
  }

  public DbSet<Student> Students { get; set; }

  public DbSet<Teacher> Teachers { get; set; }
}
```

이와 같이 어떤 모델 클래스가 데이터베이스에서 테이블이 되고  
또 그 `모델 클래스를 통해서 테이블과 소통이 가능할 때 엔티티`라고 부른다.  
이 엔티티를 통해서 테이블과 1대1 혹은 1대다 같은 것을 정의할 수 있다.  
현재는 간단히 사용하는 방법만 익히고 나중에 효율적으로 사용하는것을 살펴볼것이다.  

마이그레이션을 통해 실제 데이터베이스와 테이블도 만들 수 있다.

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
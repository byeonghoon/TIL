# 06-MVC-Validation-2

## 유효성 검사 2

사이트가 요청 위조 정리??

사이트간 요청 위조를 예방하는 Validation 코드를 Controller 넣어야 한다.  

영어로 `cross site request forgery(CSRF)`  

사이트간 요청 위조는 웹 사이트의 취약점을 이용해서  
사용자가 의도하지 않은 요청을 송신 하는 공격 방법 이다.  

사이트간 요청 위조의 여러가지 방법중 하나가 HTML 폼을 이용하는 방법이 있다.  

asp.net core 에서 default로 사용하는 예방 방법을 확인하면

현재 폼화면에 들어가서 크롬 개발자 도구에서 inspect를 클릭해서  
태그들을 확인해보면 `히든 타입으로 인풋 필드가 한개가 추가 생성되어 있는것을 확인` 할 수 있다.  

name 필드에 `__RequestVerificationToken`  
value 필드에 `CfDJ8I_u42eATa9LiFqdczoxsZ4HbEqVx0cKzgZBBfjMc9oil1Ein...`값이 들어있다.  
이것은 `사이트간 요청 위조를 예방하기 위해 사용하는 토큰`이다.  

`이 토큰을 서버에서도 항상 확인 해야 사이트간 요청 위조를 예방 할 수 있다.`  

그래서 서버에서 확인하는 방법은 다음과 같다.

해당 Controller - Action 메소드에 `[ValidateAntiForgeryToken]` 애노테이션을 추가해줌으로써  
사용자가 서버로 Form을 전송할 때, 해당 요청이 사용자에게 제공한 Form으로 부터온 요청이 맞는지를 확인해준다.  

```cs
// {project}/Controllers/HomeController.cs


public class HomeController : Controller
{
  // 레이저 파일을 디스플레이 하는 역할을 한다.
  // GET
  public IActionResult Student()
  {
    return View();
  }

  [HttpPost]
  [ValidateAntiForgeryToken]
  public IActionResult Student(Student model)
  {
    if (!ModelState.IsValid)
    {
      // 에러를 보여준다.
    }

    // model 데이터를 데이터베이스에 저장한다.

    return View();
  }
}
```

asp.net core 에서는 form 테그에 있는 method="POST"가 있으면 자동으로  
사이트간 요청 위조를 예방하는 토큰을 히든 필드가 적용된다.  

[ValidateAntiForgeryToken]가 정상적으로 동작하는지 확인하기 위해서  
form 테그에 사이트간 요청 위조를 예방하는 토큰을 생성하지 않는 테그 헬퍼를 적용 후 확인해 보자

asp-antiforgery="false"를 form 테그에 적용해보자  

```html
// {Project}/Views/Home/Student.cshtml

@using {Project}.Models
@model Student

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<h3>Add New Student</h3>

<form class="form from-horizontal" asp-antiforgery="false" method="post">
  <div class="form-group">
    <div class="col-md-3">
      <label>Name:</label>
      <input type="text" class="form-control" asp-for="Name"/>
      <span asp-validation-for="Name"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>Age:</label>
      <input type="text" class="form-control" asp-for="Age"/>
      <span asp-validation-for="Age"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-3">
      <label>Country:</label>
      <input type="text" class="form-control" asp-for="Country"/>
      <span asp-validation-for="Country"></span>
    </div>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
```

서버를 다시 로드 후 폼 화면에 들어가서 크롬 개발자 도구에서 inspect를 클릭한다.  
태그들을 확인해보면 아까 있었던 히든 타입 __RequestVerificationToken 필드가 없는것을 확인 할 수 있다.  

이 상태에서 폼 인풋값을 서버로 전송해보면 서버에서 받아주지 않고 차단한다.  

그래서 폼을 서버로 요청할 때 서버에서 `[ValidateAntiForgeryToken] 애노테이션`을 사용하는  
것은 아주 좋은 예이구 Practice 이다.

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
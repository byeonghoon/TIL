# 08-Razor-Layout

## 레이아웃

웹 사이트에서 어느 페이지를 가도 항상 동일하게 보이는 부분이 있다.  

예를들면 `네비게이션`, `푸터`와 같은  
그런데 이 2개를 만들기 위해 페이지를 추가할 때 마다 구현하는 것은 굉장히 번거롭고 코드의 낭비이다.  

그래서 `레이아웃`이라는 마스터 뷰를 만들고  
그 마스터 뷰안에서만 네비게이션과 푸터를 추가하면 나머지 어느 페이지를 가도 보이게 설정 할 수 있다.  

Views 폴더안에 Shared라는 폴더를 만들자  
`{Project}/Views/Shared`  

Views/Shared라는 Views 폴더에서의 공유의 의미가 있는데
asp.net MVC 프레임워크에서  
애플리케이션 어느곳에서든지 보이고 쓸 수 있게 설정되어 있다.  

Shared 폴더안에 `_Layout.cshtml` 파일을 추가하자  
`{Project}/Views/Shared/_Layout.cshtml`

```html
// {Project}/Views/Shared/_Layout.cshtml

<!DOCTYPE html>

<html>
  <head>
    <meta name="viewport" content="width=device-width"></meta>
    <title>@ViewBag.Title</title>
  </head>
  <body>
    <div>
      @RenderBody()
    </div>
  </body>
</html>
```

여기서 중요하게 봐야할 부분은 `@RenderBody()` 부분이다.  

RenderBody는 레이아웃 뷰에 꼭 필요한 코드이다.  
`RenderBody있는 위치에서 우리가 만든 뷰들을 보여주는 부분`이다.  

레이아웃에 네비게이션바와 푸터를 추가해보자

```html
// {Project}/Views/Shared/_Layout.cshtml

<!DOCTYPE html>

<html>
  <head>
    <meta name="viewport" content="width=device-width"></meta>    
    <title>@ViewBag.Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" asp-controller="Home" asp-action="Index">Home</a>
        </div>
        <ul class="nav navbar-nav">
          <li><a asp-controller="Home" asp-action="Index">Home</a></li>
          <li><a asp-controller="Home" asp-action="Student">Student</a></li>
        </ul>
      </div>
    </nav>

    <div>
      @RenderBody()
    </div>

    <div class="navbar navbar-default navbar-fixed-bottom">
      <div class="container-fluid">
        <span class="navbar-text">
          My Footer
        </span>
      </div>
    </div>
  </body>
</html>
```

작성한 코드에서 보면 추가된 TagHelpers 들이 있다.

- `asp-controller`
- `asp-action`

다음 코드는  
"Home이라는 하이퍼링크를 클릭하면 Home 컨트롤러에 있는 Index Action 메소드로 이동하겠다." 라는 의미이다.

```html
<a asp-controller="Home" asp-action="Index">Home</a>
```

뷰에서 필요한 js, css 라이브러리  
예를들면 bootstrap, jquery 같은 것들을 레이아웃 뷰에 CDN을 추가하면  
레이아웃에 있는 CDN을 공동으로 사용할 수 있기 때문에 다른 뷰에서 추가할 필요가 없다.  

기존 Student.cshtml 에서 설정한 CDN을 삭제하자

이제 추가적으로 해줄일이 있는데 각 뷰에 타이틀을 설정해 주는것이다.  
뷰에 타이틀을 해당 View에서 설정해주고, 그 설정 값을 레이아웃 뷰로 전달 할 수 있다.

```html
// {Project}/Views/Home/Student.cshtml

@using WebApplication8.ViewModels
@model StudentTeacherViewModel

@{
  ViewBag.Title = "Student";
}

<h3>Add New Student</h3>
<form class="form from-horizontal" method="post">
...
```

ViewBag은 다이나믹 타입이다. 그래서 String, int 등 다 사용 할 수 있다.  

HomeController에서 Student 뷰를 방문했을 때,  
레이아웃 뷰에서 Student 뷰 파일에 있는 ViewBag.Title을 읽고 다시 그 값을 보여준다.

현재 Student 뷰 파일은 레이아웃 뷰에 존재를 모른다.  
Student 뷰 파일에 레이아웃 뷰를 사용하겠다는것을 정의해줘야 한다.

```html
// {Project}/Views/Home/Student.cshtml

@using WebApplication8.ViewModels
@model StudentTeacherViewModel

@{
  ViewBag.Title = "Student";
  Layout = $"../Shared/_Layout.cshtml"; // 추가
}

<h3>Add New Student</h3>
<form class="form from-horizontal" method="post">
...
```

현재 Home 컨트롤러에 Index 액션이 정의되어 있지 않다.  
Home 컨트롤러에 Index 액션을 추가하고, Index 액션에 해당하는 Razor 뷰도 추가하자.  

```cs
// {Project}/Controllers/HomeController.cs

public class HomeController : Controller
{
  public IActionResult Index()
  {
    return View();
  }

...
```

```html
{Project}/Views/Home/Index.cshtml

@{
  ViewBag.Title = "Home";
  Layout = $"../Shared/_Layout.cshtml";
}

<div class="container">
  <div class="jumbtron">
    <h1>환영합니다.</h1>
    <p>we are learning asp.net core</p>
  </div>
</div>
```

동작을 확인하자.  

<br />

## 참고자료

- 인프런 해외취업 ASP.NET Core 웹개발 기본 강좌(https://www.inflearn.com/course/asp-net-core/
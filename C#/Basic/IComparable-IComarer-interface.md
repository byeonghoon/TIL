# IComparable, IComparer interface

## 목차

- 개요
- IComparable
- IComparer
- 참고자료
- ToDo

<br />

## 개요

본 문서는 `IComparable 인터페이스`, `IComparer 인터페이스`를 학습하며 정리한 문서입니다.

<br />

## IComparable

`IComparable, IComparer 인터페이스`는 개체의 값 비교를 제공하기 위해 정의되었다.  

C#에서 컬렉션은 대부분 Sort 메서드를 제공하는데  
IComparable 인터페이스기반의 요소를 보관하고 있을 때 정상적으로 동작하고 그렇지 않으면 에외를 발생한다.  

그리고 IComparer 개체를 입력 인자로 받아 Sort 메서드가 중복 정의되어 있는데  
정렬하는 과정에서 입력 인자로 받은 IComparer 개체를 이용한다.  

또한 C#의 기본 형식들은 IComparable 인터페이스를 기반으로 정의되어 있어서  
모두 동일한 기본 형식을 보관한 컬렉션은 Sort 메서드를 이용하여 정렬할 수 있다.  

배열은 Array 추상 클래스의 정적 Sort 메서드가 있다.


```c#
int[] arr = new int[10] { 20, 10, 5, 9, 11, 23, 8, 7, 6, 13 };

Array.Sort(arr); //정적 메서드 Sort를 이용, 입력 인자는 1차원 배열

foreach (int i in arr)
{
  Console.Write("{0} ", i);
}
```


컬렉션은 컬렉션 인스턴스 Sort 메서드가 있다.


```c#
ArrayList ar = new ArrayList();
ar.Add(4);
ar.Add(8);
ar.Add(2);

ar.Sort(); //개체의 멤버 메서드 Sort를 이용

foreach (int i in ar)
{
  Console.Write("{0} ", i);
}
```


사용자 형식을 정의할 때 IComparable 인터페이스를 구현하면 사용자 정의 형식도 정렬할 수 있다.  
IComparable 인터페이스는 자신과 입력 인자를 비교하여 결과를 반환하는 `CompareTo 메서드`를 약속하고 있다.  


```c#
public interface IComparable
{
  int CompareTo(object obj);
}
```


사용자 정의 형식은 다음 예제에서 확인한다.  


```c#
class Member : IComparable
{
  private readonly string name;
  private readonly string addr;

  public Member(string name, string addr)
  {
    this.name = name;
    this.addr = addr;
  }

  public int CompareTo(object obj) // //IComparable에서 약속한 메서드 구현
  {
    Member member = obj as Member;

    if (member == null)
    {
      throw new NotImplementedException();
    }

    return name.CompareTo(member.name);
  }

  public override string ToString()
  {
    return string.Format("이름: {0}, 주소: {1}", name, addr);
  }
}

class Program
{
  static void Main(string[] args)
  {
    Member[] members = new Member[3];
    members[0] = new Member("다병훈", "청주");
    members[1] = new Member("가병훈", "대전");
    members[2] = new Member("나병훈", "서울");

    Array.Sort(members);

    foreach (Member member in members)
    {
      Console.WriteLine(member);
    }
  }
}


// 이름: 가병훈, 주소: 대전
// 이름: 나병훈, 주소: 서울
// 이름: 다병훈, 주소: 청주
```


<br />


## IComparer

프로그램에서 하나 이상의 정렬 기능을 제공할 때는 어떻게 할까?  
기본이 되는 값으로 비교하는 것은 IComparable 인터페이스에서 약속한 CompareTo 에서 정의 하면 된다.  
그리고 다른 값으로 비교를 원한다면 IComparer 인터페이스 기반의 형식을 정의하면 된다.  

Sort 메서드는 IComparer 개체를 입력 인자로 받는 메서드도 지원하고 있다.  


```c#
public interface IComparer
{
  int Compare(object x, object y);
}
```


IComparer 인터페이스에는 두 개의 개체를 입력 인자로 받는 `Compare`메서드를 약속하고 있다.  
결국 비교를 담당하는 부분을 별도의 형식으로 정의하는 것이다.  


```c#
class AddrComparer : IComparer
{
  public int Compare(object x, object y)
  {
    Member mx = x as Member;
    Member my = y as Member;

    if (mx == null || my == null)
    {
      throw new NotImplementedException();
    }

    return mx.Addr.CompareTo(my.Addr);
  }
}

class Member : IComparable
{
  private string name;

  public string Addr { get; }

  public Member(string name, string addr)
  {
    this.name = name;
    this.Addr = addr;
  }

  public int CompareTo(object obj) // IComparable에서 약속한 메서드 구현
  {
    Member member = obj as Member;

    if (member == null)
    {
      throw new NotImplementedException();
    }

    return name.CompareTo(member.name);
  }

  public override string ToString()
  {
    return string.Format("이름: {0}, 주소: {1}", name, Addr);
  }
}

class Program
{
  static void Main(string[] args)
  {
    Member[] members = new Member[3];
    members[0] = new Member("홍길동", "율도국");
    members[1] = new Member("강감찬", "대한민국");
    members[2] = new Member("장언휴", "이에이치");

    Array.Sort(members);

    foreach (Member member in members)
    {
      Console.WriteLine(member);
    }

    Array.Sort(members, new AddrComparer());

    Console.WriteLine("-------------------------");

    foreach (Member member in members)
    {
      Console.WriteLine(member);
    }
  }
}

// 이름:강감찬 주소:대한민국
// 이름:장언휴 주소:이에이치
// 이름:홍길동 주소:율도국
// -----------------------------
// 이름:강감찬 주소:대한민국
// 이름:홍길동 주소:율도국
// 이름:장언휴 주소:이에이치
```


<br />


## 참고자료

- [개인 블로그/언제나 휴일](http://ehpub.co.kr/c-8-2-5-icomparable-%EC%9D%B8%ED%84%B0%ED%8E%98%EC%9D%B4%EC%8A%A4%EC%99%80-icomparer-%EC%9D%B8%ED%84%B0%ED%8E%98%EC%9D%B4%EC%8A%A4/)

<br />


## ToDo
- Sort 메서드와 연결해서 IComparable, IComparer 인터페이스 추가 정리 필요
- IComparable을 구현한 타입이 아니여도 IComparer 인스턴스를 넣고 Sort가 가능할까??
- IComparable을 구현한 타입을 IComparer 인스턴스를 넣은 Sort 메서드를 호출하면 IComparable을 구현한 타입에 정의되어 있는 CompareTo 메서드는 무시되나??
# BCL - ADO.NET 데이터 제공자

## 목차

- 개요
- ADO.NET 데이터 제공자
  - 실습용 Postgresql SQL
  - Npgsql.NpgsqlConnection
  - Npgsql.NpgsqlCommand
  - Npgsql.NpgsqlDataReader
  - Npgsql.NpgsqlParameter
  - Npgsql.NpgsqlDataAdapter
- 데이터 컨테이너
  - 일반 닷넷 클래스(POCO)
  - System.Data.DataSet
- 데이터베이스 트랜젝션
- 참고자료
    
<br />

## 개요

본 문서는 `ADO.NET 데이터 제공자`를 `posgresql`과 연동하여 학습하며 정리한 문서입니다.

<br />

## ADO.NET 데이터 제공자

- `ADO.NET 데이터 제공자`은 닷넷 프레임워크용 데이터 접근 라이브러리이다.
- MS SQL 서버용 `ADO.NET 데이터 제공자`는 BCL(Base Class Library)에 포함되어 있다.
- 일반적으로 개발자들이 DBMS에 저장되어 있는 데이터에 접근하거나 수정할 때 사용한다.


모든 `ADO.NET 데이터 제공자`는 MS에서 미리 정의해 둔 `공통 인터페이스를 상속받아 구현한다.`

|  인터페이스  |  설명 |
| ------------- | ------------- |
| System.Data.IDbConnection | 데이터베이스 서버와의 연결을 담담하는 클래스가 구현해야할 인터페이스 정의 |
| System.Data.IDbCommand | 데이터베이스 서버 측으로 실행될 SQL 문을 전달하는 클래스가 구현해야할 인터페이스 정의 |
| System.Data.IDataReader | 실행된 SQL 문으로부터 반환받은 데이터를 열람하는 클래스가 구현해야할 인터페이스 정의 |
| System.Data.IDbDataParameter | IDbCommand에 전달되는 SQL 문의 인자 값을 보관하는 클래스가 구현해야 할 인터페이스 정의 |
| System.Data.IDbDataAdapter | System.Data.DataTable 개체와 상화작용하는 Data Adapter 클래스가 구현해야 할 인터페이스 정의 |


ADO.NET 데이터 제공자는 보통 데이터베이스 서버를 만든 업체에서 배포한다.

하지만 `MS SQL 서버`의 경우 MS에서 만들었기 때문에 `MS SQL 서버`에 대한 ADO.NET 데이터제공자는  
BCL에 포함시켜서 배포되고 있어 별도로 내려받을 필요는 없다.  

해당 문서에서는 `Postgresql`을 사용하기 때문에    
별도로 `Postgresql`전용 ADO.NET 데이터 제공자를 내려받아야 한다.  
`Nuget`에서 `Npgsql`(`Postgresql`전용 ADO.NET 데이터 제공자)를 내려받는다.


MS SQL 서버용 ADO.NET 데이터 제공자는   
BCL에서 System.Data.SqlClinet 네임스페이스 아래에 각각 다음과 같이 구현돼 있다.  

|  인터페이스  |  MS SQL 서버용 ADO.NET 구현 클래스 |
| ------------- | ------------- |
| System.Data.IDbConnection | System.Data.SqlClient.SqlConnection |
| System.Data.IDbCommand | System.Data.SqlClient.SqlCommand |
| System.Data.IDataReader | System.Data.SqlClient.SqlDataReader |
| System.Data.IDbDataParameter | System.Data.SqlClient.SqlParameter |
| System.Data.IDbDataAdapter | System.Data.SqlClient.SqlDataAdapter |


`Npgsql`(`Postgresql`전용 ADO.NET 데이터 제공자)는   
`Npgsql` 네임스페이스 아래에 각각 다음과 같이 구현돼 있다.

|  인터페이스  |  Postgresql 서버용 ADO.NET 구현 클래스 (Npgsql) |
| ------------- | ------------- |
| System.Data.IDbConnection | Npgsql.NpgsqlConnection |
| System.Data.IDbCommand | Npgsql.NpgsqlCommand |
| System.Data.IDataReader | Npgsql.NpgsqlDataReader |
| System.Data.IDbDataParameter | Npgsql.NpgsqlParameter |
| System.Data.IDbDataAdapter | Npgsql.NpgsqlDataAdapter |

이론상 적어도 데이터베이스 수만큼 ADO.NET 데이터 제공자가 있을 테지만  
모두 System.Data에서 제공되는 인터페이스를 상속받아 구현하고 있으므로  
사용법을 한번 익혀두면 다른 데이터 제공자도 어렵지 않게 사용할 수 있다.

<br />

## 실습용 Postgresql SQL


```sql
DROP TABLE member_info;

CREATE TABLE IF NOT EXISTS member_info (
  name           VARCHAR(16) NOT NULL,
  birth          VARCHAR(10) NOT NULL,
  email          VARCHAR(32) NOT NULL unique,
  family         INTEGER     NOT NULL
);

INSERT INTO member_info (name, birth, email, family)
     VALUES ('Jason', '1967-12-03', 'jason@gamil.com', 0);
INSERT INTO member_info (name, birth, email, family)
     VALUES ('Mark', '1998-03-02', 'mark@naver.com', 1);
```


<br />


### Npgsql.NpgsqlConnection

데이터베이스를 사용하기 위해 맨 먼저 해야 할 일은 데이터베이스에 연결하는 것이다.  
ADO.NET 데이터 제공자마다 정형화된 "연결 문자열(connection string)"이 정해져 있다.

MS SQL 서버에 연결할 때 사용하는 연결 문자열은 다음과 같다.  

```
Data Source=[서버]\[인스턴스명];Initial Catalog=[DB명];User Id=[계정명];Password=[비밀번호]
```

Postgresql 서버에 연결할 때 사용하는 연결 문자열은 다음과 같다.  

```
Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]
```


데이터베이스 연결 문자열이 마련되면 다음과 같이 `NpgsqlConnection` 개체를 사용해 DB에 접속하고 해제할 수 있다.


```c#
NpgsqlConnection conn = new NpgsqlConnection();
conn.ConnectionString = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

// DB에 연결하고
conn.Open();

// .... [DB에 연결된 동안 DB 쿼리 수행]

// 연결을 닫는다.
conn.Close();
```


연결 문자열의 경우 `NpgsqlConnection`을 사용할 때 마다 중복해서 사용하기보다는   
공통 변수에 넣어두고 재사용하는 핀이 더 선호된다.  

보통 연결 문자열은 `app.config`에 넣는 것이 관례다.


```xml
<?xml version="1.0"?>
<configuration>

  <connectionStrings>
    <add name="TestDB" 
      connectionString="Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]">
  </connectionStrings>
</configuration>
```


이렇게 app.config에 설정된 connectionString 값을 코드에서 사용하려면  
`System.Configuration`어셈블리를 참조 추가한 다음 그것에 포함된 `ConfigurationManager` 타입을 이용해 가져올 수 있다.


```c#
string connStr = ConfigurationManager.connectionStrings["TestDB"].connectionString
using (NpgsqlConnection conn = new NpgsqlConnection())
{
  conn.connectionString = connStr
  conn.Open();
  // .... [DB에 연결된 동안 DB 쿼리 수행]
}
```


<br />


### Npgsql.NpgsqlCommand

데이터베이스에 연결됐으면 이제부터 DB가 소유한 모든 자원을 `NpgsqlCommand` 타입을 이용해 조작할 수 있다.  
CRUD(INSERT, SELECT, UPDATE, DELETE)를 NpgsqlCommand를 잉용해 실행할 수 있다.  

NpgsqlCommand는 쿼리를 실행하기 위해 3개의 메서드를 제공한다.  
- `ExecuteNonQuery`
- `ExecuteScalar`
- `ExecuteReader`

어떤 메서드를 사용하느냐는 쿼리 문의 수행 결과가 반환하는 값의 종류에 따라 달라진다.


#### ExecuteNonQuery

- 쿼리 종류: INSERT, UPDAET, DELETE
- 설명: 영향받은 Row의 수를 반환


```c#
//INSERT

string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";
using (NpgsqlConnection conn = new NpgsqlConnection())
{
  conn.ConnectionString = connStr;
  conn.Open();

  NpgsqlCommand cmd = new NpgsqlCommand
  {
    Connection = conn,
    CommandText = 
      "INSERT INTO member_info (name, birth, email, family) VALUES ('Fox', '1970-01-25', 'fox@gamil.com', 5);"
  };

  int affectedCount = cmd.ExecuteNonQuery();
  Console.WriteLine(affectedCount); // 출력 결과: 1
}
```


- SQL문을 `CommandText`에 넣어두고 ExecuteNonQuery 메서드를 호출한다.
- `affectedCount`는 해당 SQL 문을 실행했을 때 테이블에 영향을 받은 레코드의 수가 구해진다.
    - 여기서는 INSERT 구문으로 1개의 레코드가 추가됐으므로 1을 반환한 것이다.


```C#
// UPDATE

string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";
using (NpgsqlConnection conn = new NpgsqlConnection())
{
  conn.ConnectionString = connStr;
  conn.Open();

  NpgsqlCommand cmd = new NpgsqlCommand
  {
    Connection = conn,
    CommandText = "UPDATE member_info SET family=3 WHERE email='fox@gamil.com'"
  };

  int affectedCount = cmd.ExecuteNonQuery();
  Console.WriteLine(affectedCount);
}
```


```c#
// DELETE

string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  conn.Open();
  NpgsqlCommand cmd = new NpgsqlCommand
  {
    Connection = conn,
    CommandText = "DELETE FROM member_info WHERE email='fox@gamil.com'"
  };

  int affectedCount = cmd.ExecuteNonQuery();
  Console.WriteLine(affectedCount);
}
```


#### ExecuteScalar

- 쿼리 종류: SELECT
- 설명: 단일 레코드(1개의 값)을 반환하는 쿼리를 수행(예를들면 count(*))


```c#
string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  conn.Open();

  NpgsqlCommand cmd = new NpgsqlCommand
  {
     Connection = conn,
     CommandText = "SELECT COUNT(*) FROM member_info WHERE family >= 2"
  };

  object objValue = cmd.ExecuteScalar();
  Int64 countOfMember = (Int64) objValue;
  Console.WriteLine(countOfMember);
}
```


#### ExecuteReader

- 쿼리 종류: SELECT
- 설명: 다중 레코드를 반환하는 쿼리를 수행
  
SELECT가 단일 값을 반환하지 않는 그 밖의 모든 쿼리를 실행할 때는 ExecuteReader 메서드 사용  
ExecuteReader 메서드는 SELECT 결괏값을 직접 반환하지 않고, 대신 레코드를 차례대로 읽는 목적으로 별도 제공되는  
`NpgsqlDataReader` 타입의 인스턴스를 반환한다.


```c#
string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  conn.Open();

  NpgsqlCommand cmd = new NpgsqlCommand
  {
     Connection = conn,
     CommandText = "SELECT * FROM member_info"
  };
  
  NpgsqlDataReader reader = cmd.ExecuteReader();
  // ... [reader를 이용해 레코드를 하나씩 조회]
}
```


`NpgsqlDataReader`는 나중에 살펴보자.

`NpgsqlCommand`는 결국 `NpgsqlConnection` 객체가 맺어놓은 연결 위에서 실행된다.

따라서 성능상의 이유로 `NpgsqlConnection.Open과 Close 구간`은  
가능한 한 빨리 실행될 수 있게 쿼리 실행 이외의 지연이 발생하는  
다른 코드는 넣지 않는 것이 권장사항이다.  

<br />


### Npgsql.NpgsqlDataReader

ADO.NET 데이터 제공자는 명령을 실행할 수 있는 `IDbCommand`    
레코드를 읽어낼 수 있는 `IDataReader` 인터페이스를 별도로 구분해 놓았다.  

따라서 이를 상속받는 모든 ADO.NET 데이터 제공자는 동일한 규칙을 따르고 있으며 `NpgsqlDataReader`도 예외가 아니다.  
NpgsqlDataReader는 SELECT문을 수행한 결과가 한 행씩 차례대로 읽어내는 기능을 한다.


```c#
string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  conn.Open();

  NpgsqlCommand cmd = new NpgsqlCommand
  {
    Connection = conn,
    CommandText = "SELECT * FROM member_info"
  };

  NpgsqlDataReader reader = cmd.ExecuteReader();

  // NpgsqlDataReader.Read()메소드는 읽어야 할 데이터가 남아 있다면 true, 없다면 false를 반환
  while (reader.Read()) 
  {
    long id = reader.GetInt64(0);
    string name = reader.GetString(1);
    string birth = reader.GetString(2);
    string email = reader.GetString(3);
    int family = reader.GetInt32(4);
    Console.WriteLine("{0}, {1}, {2}, {3}, {4}", id, name, birth, email, family);
  }
        
  reader.Close();
}
```


위의 코드에서 `NpgsqlDataReader`는 데이터베이스 서버로부터 테이블의 내용을 `한 행씩 차례대로 끝까지 읽어내는 역할을 한다.`

`NpgsqlDataReader`를 이용한 조회 코드는 전형적은로 while 루프로 처리된다.  
`변수 reader는 최초 아무것도 가리키지 않다가 Read를 호출하면서 다음 행의 레코드를 가리킨다.`  
더는 읽을 행이 없을 때 false를 반환하면서 while 루프를 벗어난다.  
 
내부적으로 레코드를 가르키는 것을 `커서(cursor)`라고 부르며, 이 경우 커서가 `전방향`으로 이동한다고 말한다.  

기억해야 할 것은, `NpgsqlDataReader`가 SELECT 결과물을 담고 있는 상태는 아니라는 점이다.  
즉, 커서가 가리키고 있는 위치는 `데이터베이스와 연결된 상태에서 테이블로부터 반환될 데이터의 행이 된다.`  
`따라서 Read동작은 반드시 NpgsqlConnection 객체가 데이터베이스에 연결된 상태에서만 가능하다.`  

Read 메서드를 한번 호출하면 커서가 다음 행으로 이동하며,  
각 행에 속한 칼럼별 데이터는 NpgsqlDataReader에서 제공되는 `Get.....` 메서드를 이용해 구할 수 있다.  

NpgsqlDataReader를 사용하는 while 루프는 가능한 빨리 끝내야한다.  
왜냐하면 NpgsqlDataReader가 열려 있는 동안 데이터베이스와 연결이 유지돼야 하고  
이 시간이 길수록 데이터베이스의 처리 성능은 낮아지기 때문이다.  

또한 NpgsqlDataReader를 사용한 후 반드시 Close 메서드를 호출하는 것을 잊지 말아야한다.  
그렇지 않으면 같은 연결 개체에서는 어떤 명령도 실행할 수 없다.

<br />


### Npgsql.NpgsqlParameter

보통 쿼리 문은 사용자로부터 입력된 데이터로 구성된다.  
예를들어, 웹 사이트에서 회원 정보를 입력받은 내용이 INSERT 쿼리 문의 인자로 전달되는 것이다.  
이때 쿼리를 만드는 방법은 2가지다.  

우선 다음과 같이 문자열을 연결해서 구성할 수 있다.  


```c#
// 웹 사이트에서 입력받은 회원 정보  
string name = "Cooper";
string birth = "1970-01-25";
string email = "cooper@hotmail.com";
int family = 5;

string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  conn.Open();

  // 웹 사이트에서 입력받은 회원 정보를 인자로 전달해서 만든 쿼리 문
  string text = string.Format("INSERT INTO member_info (name, birth, email, family) VALUES ('{0}', '{1}', '{2}', {3});", name,
            birth, email, family);

  NpgsqlCommand cmd = new NpgsqlCommand
  {
    Connection = conn,
    CommandText = text
   };

   cmd.ExecuteNonQuery();
}
```


위의 예제에서와 같이 이렇게 문자열 연산을 하는 데는 크게 두 가지 약점이 있다.  

첫번째, 보안 취약    
SQL 문법에 해당하는 문자열을 사용자가 입력하는 경우 수행되는 쿼리가 의도하지 않는 결과를 낳을 수 있다.   
이름 "SQL 주입"이라고 하며 심각한 보안 결함에 해당한다.    

두번째, 서버 측의 쿼리 수행 성능 저하  
데이터베이스는 수행되는 쿼리를 내부적인 컴파일 과정을 거쳐 실행 계획을 생성한다.  
그리고 한번 수행된 쿼리의 경우 실행 계획을 캐싱해서 다음에 동일한 쿼리가 수행되면  
빠르게 수행할 수 있게 한다. 하지만 단일 쿼리 문으로 수행되는 경우  
동일한 쿼리가 발생할 확률이 낮아지므로 캐시로 인한 성능이 좋지 않다.  


이런 문제점이 `매개화된 쿼리(parameterized query)`를 사용하면 해결된다.  
즉, `실행될 쿼리 문 중에서 변수처럼 사용될 영역을 별도로 구분해서 쿼리를 전달하는 것이다.`


```c#
// 웹 사이트에서 입력받은 회원 정보  
string name = "Cooper";
string birth = "1970-01-25";
string email = "cooper@hotmail.com";
int family = 5;

string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  conn.Open();

  NpgsqlCommand cmd = new NpgsqlCommand();
  cmd.Connection = conn;
        
  // @Name 파라미터 준비
  NpgsqlParameter paramName = new NpgsqlParameter("Name", NpgsqlDbType.Varchar, 16) {Value = name};
        
  // @Birth 파라미터 준비
  NpgsqlParameter paramBirth = new NpgsqlParameter("Birth", NpgsqlDbType.Varchar, 10) {Value = birth};
        
  // @Email 파라미터 준비
  NpgsqlParameter paramEmail = new NpgsqlParameter("Email", NpgsqlDbType.Varchar, 32) {Value = email};
        
  // @Family 파라미터 준비
  NpgsqlParameter paramFamily = new NpgsqlParameter("Family", NpgsqlDbType.Smallint) {Value = family};

        
  // cmd.Parameter 컬렉션에 NpgsqlParameter 개체를 추가
  cmd.Parameters.Add(paramName);
  cmd.Parameters.Add(paramBirth);
  cmd.Parameters.Add(paramEmail);
  cmd.Parameters.Add(paramFamily);
        
  string text = "INSERT INTO member_info (name, birth, email, family) VALUES (@Name, @Birth, @Email, @Family);";
  cmd.CommandText = text;
  int affectedCount = cmd.ExecuteNonQuery();
  Console.WriteLine(affectedCount);
}
```


CommandText에 지정된 쿼리에 기존의 값을 직접 포함하는 대신 `@`접두사와 함께 변수 이름을 지정했다.  
마치 C# 코드에 사용된 변수라도 봐도 무방하다.  

이 쿼리가 NpgsqlCommand에 의해 수행되면  
각 변수는 `NpgsqlCommand에.Parameters 컬렉션`에 포한된 같은 이름의  
NpgsqlParameter 객체의 값이 대응되어 처리된다.

이 방법은 문자열 쿼리에 비해 코드가 복잡해지는 단점이 있다.  
`그래도 최근 들어 보안이 점점 중요해지고 있기 때문에`  
`반드시 문자열 쿼리가 아닌 매개변수화된 쿼리를 사용해서 응용프로그램을 만들어야 한다.`

<br />


### Npgsql.NpgsqlDataAdapter

데이터베이스 연동 프로그램은 대부분의 작업이 SELECT에 집중된다.  
그렇게 많이 사용되는 쿼리이면서도 INSERT/UPDATE/DELETE에 비하면 더 복잡하다.  
`또한 개발자의 실수로 NpgsqlConnection 객체의 연결이 장시간 지속되는 경우가 발생하기도 한다.`  
`이러한 문제점을 해결함과 동시에 일부 편의성 기능을 포함한 NpgsqlDataAdapter라는 타입을 만들어 뒀다.`  


```c#
DataSet ds = new DataSet();
string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  // 개발자가 따로 Open, Close 하지 않음
  NpgsqlDataAdapter sda = new NpgsqlDataAdapter("SELECT * FROM member_info", conn);
  sda.Fill(ds, "member_info");
}
      
ds.WriteXml(Console.Out); // DataSet이 가진 내용이 콘솔 화면에 출력한다.
```


NpgsqlDataAdapter의 Fill 메서드는 이전 코드에서 while 루프의 코드를 대신 수행해 준다.  
`즉, 데이터를 최대한 빨리 읽어내어 DataSet 개체에 채워 넣고 곧바로 연결 개체의 사용을 중지한다.`

<br />


## 데이터 컨테이너

### 일반 닷넷 클래스

데이터 컨테이너로서 "단순한 유형의 닷넷 클래스(POCO: Plain Old CLR Object)"를 사용할 수 있다.  
데이터베이스에서 정의하는 테이블은 칼럼의 집합에 불과하기 때문에 이를 닷넷 클래스로 표현하는 방법은 매우 쉽다.  
예를 들면 member_info 테이블에 해당하는 데이터 컨테이너를 POCO로 정의하면 다음과 같다.  


```c#
public class MemberInfo
{
  public string Name;
  public string Birth;
  public string Email;
  public int Family;
}
```


이를 기반으로 CRUD를 수행하는 데이터베이스 조작 클래스를 만들어 보자  


```c#
public class MemberInfoDAC
{
  private NpgsqlConnection _npgsqlConn;
    
  public MemberInfoDAC(NpgsqlConnection npgsqlConn)
  {
    _npgsqlConn = npgsqlConn;
  }
    
  private void FillParameters(NpgsqlCommand cmd, MemberInfo memberInfo)
  {
    NpgsqlParameter paramName = new NpgsqlParameter("Name", NpgsqlDbType.Varchar, 16) {Value = memberInfo.Name};
    NpgsqlParameter paramBirth = new NpgsqlParameter("Birth", NpgsqlDbType.Varchar, 10) {Value = memberInfo.Birth};
    NpgsqlParameter paramEmail = new NpgsqlParameter("Eamil", NpgsqlDbType.Varchar, 32) {Value = memberInfo.Email};
    NpgsqlParameter paramFamily = new NpgsqlParameter("Family", NpgsqlDbType.Smallint) {Value = memberInfo.Family};
    
    cmd.Parameters.Add(paramName);
    cmd.Parameters.Add(paramBirth);
    cmd.Parameters.Add(paramEmail);
    cmd.Parameters.Add(paramFamily);
  }
    
  public int Insert(MemberInfo memberInfo)
  {
    string queryTxt = "INSERT INTO member_info (name, birth, email, family) VALUES (@Name, @Birth, @Email, @Family);";
    NpgsqlCommand cmd = new NpgsqlCommand(queryTxt, _npgsqlConn);
    FillParameters(cmd, memberInfo);
    return cmd.ExecuteNonQuery();
  }
    
  public int Update(MemberInfo memberInfo)
  {
    string queryTxt = "UPDATE member_info SET name=@Name, birth=@Birth, family=@Family WHERE email=@Email;";
    NpgsqlCommand cmd = new NpgsqlCommand(queryTxt, _npgsqlConn);
    FillParameters(cmd, memberInfo);
    return cmd.ExecuteNonQuery();
  }
    
  public int Delete(MemberInfo memberInfo)
  {
    string queryTxt = "DELETE FROM member_info WHERE Email=@Email;";
    NpgsqlCommand cmd = new NpgsqlCommand(queryTxt, _npgsqlConn);
    FillParameters(cmd, memberInfo);
    return cmd.ExecuteNonQuery();
  }
     
  public MemberInfo[] SelectAll()
  {
    string queryTxt = "SELECT * FROM member_info";
    ArrayList list = new ArrayList();
        
    NpgsqlCommand cmd = new NpgsqlCommand(queryTxt, _npgsqlConn);
  
    using (NpgsqlDataReader reader = cmd.ExecuteReader())
    {
      while (reader.Read())
      {
        MemberInfo item = new MemberInfo
        {
          Name = reader.GetString(0),
          Birth = reader.GetString(1),
          Email = reader.GetString(2),
          Family = reader.GetByte(3)
        };
  
        list.Add(item);
      }
    }
  
    return list.ToArray(typeof(MemberInfo)) as MemberInfo[];
  }
}
```


MemberInfoDAC 타입은 일반 닷넷 클래스로 정의된 MemberInfo 타입을 기반으로  
데이터베이스 테이블에 CRUD 연산을 모두 수행한다.  
이렇게 정의된 데이터 컨테이너 타입과 그에 따른 DAC(Data Access Component) 클래스를 이용하면  
매우 쉽게 데이터베이스와 연동 할 수 있다.  


```c#
MemberInfo item = new MemberInfo
{
  Name = "Jennifer",
  Birth = "1985-05-06",
  Email = "Jennifer@jennifer.com",
  Family = 0
};

string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";
  
using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  conn.Open();
  
  MemberInfoDAC dac = new MemberInfoDAC(conn);

  int result;
  result = dac.Insert(item);
  Console.WriteLine("Insert AffectedCount: {0}", result);

  item.Name = "Jennifer2";
  result = dac.Update(item);
  Console.WriteLine("Update AffectedCount: {0}", result);
  
  MemberInfo[] list = dac.SelectAll();
  foreach (MemberInfo member in list)
  {
    Console.WriteLine(member);
  }

  result = dac.Delete(item);
  Console.WriteLine("Delete AffectedCount: {0}", result);
}
```


이 구조를 기억하는 것이 중요하다.  
실무 프로젝트에서는 데이터베이스 연동을 할 때 이런 방식을 사용한다.  

즉, 응요 프로그램에서 직접 `NpgsqlCommand`를 이용해 데이터베이스 조작을 하지 않고  
테이블 단위로 CRUD 작업을 담담하는 DAC 클래스를 만들어 그것을 이용해 간접적으로 연동한다.  
어떻게 보면 응용 프로그램과 데이터베이스 사이에 층을 하나 두는 것과 같다.  

실제로 MemberInfoDac 클래스는 EXE 어셈블리를 생성하는 프로젝트로부터 분리해서  
별도의 어셈블리에 담는 것이 일반적이다. 심지어 배포조차도 다른 컴퓨터로 분리하기도 한다.  
이런 식으로 프로그램의 로직을 논리적/물리적으로 분리하는것을 계층을 나눈다고 표현한다.  

층을 나눴을 때의 가장 큰 장정은 변화에 대한 수용력이 높아진다는 것이다.  

예를들어, MemberInfo 테이블의 이름을 Members로 바꿨다고 가정해 보자.  
응용 프로그램에서 NpgsqlCommand를 직접 사용하는 방식으로 코드를 만들었다면  
테이블명을 바꾸기 위해 전체 프로그램을 검토해야 한다.  
하지만 DAC 계층을 나눠서 코드를 작성했다면  
단순히 MemberInfoDac 클래스의 쿼리 중에서 테이블 이름만 수정하면 된다.  

나눠진 층의 수에 따라 2층, 3층라는 이름이 붙는다.  
몇 단계롤 나누는지에 대한 기준은 프로그램의 구조와 복잡성에 따라 달라진다.  
너무 많이 나누는 경우 쓸데없는 코드의 중복 현상이나 호출되는 메서드 수의 증가로 서능 저하가 발생할 수 있다.  
반대로 너무 작게 나누면 나중에 프로그램의 규모가 커졌을 때 유지보수하기가 힘들어질 수 있다.  
해당 프로젝트를 이끄는 리더급 개발자가 갖춰야 할 소양 중 하나가 바로 이러한 규모를 산정할 수 있는 경험이다.  


### System.Data.DataSet

DataSet은 닷넷 프레임워크에 포함되어 있는 `범용 데이터 컨테이너`이다.  
닷넷의 System.Data 네임스페이스에는 데이터베이스의 표현을 일대일로 대응시킨 클래스가 정의돼 있다.  

데이터베이스에 테이블을 정의하면서 칼럼(coloumn)을 지정한 것을  
`System.Data.DataColumn` 타입을 이용해 동등하게 표현 할 수 있다.  

member_info 테이블에 정의된 칼럼을 예로들면 다음과 같다.


```c#
DataColumn nameCol = new DataColumn("Name", typeof(string));
DataColumn birthCol = new DataColumn("Birth", typeof(string));
DataColumn emailCol = new DataColumn("Email", typeof(string));
DataColumn familyCol = new DataColumn("Family", typeof(byte));
```


칼럼이 준비되면 이제 데이터베이스의 테이블도 정의할 수 있다.  
칼럼이 모인 것이 테이블이므로 `System.Data.DataTable` 타입을 이용해  
member_info 테이블과 동일한 상황을 재현할 수 있다.  


```c#
DataTable table = new DataTable();
table.Columns.Add(nameCol);
table.Columns.Add(birthCol);
table.Columns.Add(emailCol);
table.Columns.Add(familyCol);
```


테이블이 정의됐으니 이제 데이터 조작(CRUD)을 하는 것도 가능하다.   
DataTable 객체가 SQL 문을 지원하지는 않지만 나름의 방법으로   
Insert, Update, Delete, Select 하는 수단을 제공한다.  


```c#
// Insert
table.Rows.Add("Anderson", "1950-05-20", "anderson@gmail.com", 2);
table.Rows.Add("Jason", "1967-12-03", "jason@gmail.com", 0);
table.Rows.Add("Mark", "1998-03-02", "mark@naver.com", 1);
table.Rows.Add("Jennifer", "1985-05-06", "jennifer@jennifersoft.com", 0);

// SELECT
DataRow[] members = table.Select("Family >= 1");
foreach (DataRow member in members)
{
  Console.WriteLine("{0}, {1}, {2}, {3}", member["Name"], member["Birth"], member["Email"], member["Family"]);
}

// UPDATE: 4번째 레코드의 Name 칼럼의 값을 "Jenny"로 변경
table.Rows[3]["Name"] = "Jenny";

// DELETE: 4번째 레코드를 삭제
table.Rows.Remove(table.Rows[3]);
```


마지막으로, 테이블도 정의했다면 해당 테이블이 모인 데이터베이스도 나타낼 수 있다.  
그것이 바로 `System.Data.DataSet`이다.  
DataSet은 단지 DataTable의 묵음을 보관하는 컨테이너 역할만 한다.  

다음과 같이 사용한다.


```c#
DataSet ds = new DataSet();
ds.Tables.Add(table);
```


DataSet의 구조를 알았으니, `NpgsqlDataAdapter` 코드를 사용했던 ds 변수의 내용을 열람할 수 있다.


```c#
DataSet ds = new DataSet();

string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  NpgsqlDataAdapter sda = new NpgsqlDataAdapter("SELECT * FROM member_info", conn);
  sda.Fill(ds, "MemberInfo");
}

// DataSet에 포함된 테이블 중에서 "MemberInfo"를 찾고
DataTable dt = ds.Tables["MemberInfo"];

// SELECT로 반환된 데이터 레코드를 열람한다.
foreach (DataRow row in dt.Rows)
{
  Console.WriteLine("{0}, {1}, {2}, {3}", row["name"], row["birth"], row["email"], row["family"]);
}
```


DataSet의 이런 융통성 있는 구조는 기존 데이터 컨테이너로써 POCO를 사용하던 것을 대체할 수 있다.  
DataSet을 데이터 컨테이너로 사용하면 테이블마다 일일이 정의해야 했던 POCO 클래스를 생략할 수 있는 장점이 있다.  

다음은 DataSet 기반의 MemberInfoDAC 이다.  


```c#
public class MemberInfoDAC
{
  private NpgsqlConnection _npgsqlConn;
  private DataTable _table;

  public MemberInfoDAC(NpgsqlConnection npgsqlConn)
  {
    _npgsqlConn = npgsqlConn;
    
    DataColumn nameCol = new DataColumn("Name", typeof(string));
    DataColumn birthCol = new DataColumn("Birth", typeof(string));
    DataColumn emailCol = new DataColumn("Email", typeof(string));
    DataColumn familyCol = new DataColumn("Family", typeof(byte));

    _table = new DataTable("MemberInfo");
    _table.Columns.Add(nameCol);
    _table.Columns.Add(birthCol);
    _table.Columns.Add(emailCol);
    _table.Columns.Add(familyCol);
  }

  public DataRow NewRow()
  {
    return _table.NewRow();
  }

  private void FillParameters(NpgsqlCommand cmd, DataRow item)
  {
    NpgsqlParameter paramName = new NpgsqlParameter("Name", NpgsqlDbType.Varchar, 16) {Value = item["Name"]};
    NpgsqlParameter paramBirth = new NpgsqlParameter("Birth", NpgsqlDbType.Varchar, 10) {Value = item["Birth"]};
    NpgsqlParameter paramEmail = new NpgsqlParameter("Email", NpgsqlDbType.Varchar, 32) {Value = item["Email"]};
    NpgsqlParameter paramFamily = new NpgsqlParameter("Family", NpgsqlDbType.Smallint) {Value = item["Family"]};

    cmd.Parameters.Add(paramName);
    cmd.Parameters.Add(paramBirth);
    cmd.Parameters.Add(paramEmail);
    cmd.Parameters.Add(paramFamily);
  }

  public int Insert(DataRow item)
  {
    string queryTxt = "INSERT INTO member_info (name, birth, email, family) VALUES (@Name, @Birth, @Email, @Family);";
    NpgsqlCommand cmd = new NpgsqlCommand(queryTxt, _npgsqlConn);
    FillParameters(cmd, item);
    return cmd.ExecuteNonQuery();
  }

  public int Update(DataRow item)
  {
    string queryTxt = "UPDATE member_info SET name=@Name, birth=@Birth, family=@Family WHERE email=@Email;";
    NpgsqlCommand cmd = new NpgsqlCommand(queryTxt, _npgsqlConn);
    FillParameters(cmd, item);
    return cmd.ExecuteNonQuery();
  }

  public int Delete(DataRow item)
  {
    string queryTxt = "DELETE FROM member_info WHERE email=@Email;";
    NpgsqlCommand cmd = new NpgsqlCommand(queryTxt, _npgsqlConn);
    FillParameters(cmd, item);
    return cmd.ExecuteNonQuery();
  }

  public DataSet SelectAll()
  {
    DataSet ds = new DataSet();
    
    NpgsqlDataAdapter sda = new NpgsqlDataAdapter("SELECT * FROM member_info", _npgsqlConn);
    sda.Fill(ds, "MemberInfo");

    return ds;
  }
}
```


마찬 가지로 Main 문도 수정한다.


```c#
string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  conn.Open();

  MemberInfoDAC dac = new MemberInfoDAC(conn);

  DataRow newItem = dac.NewRow();
  newItem["Name"] = "Jennifer";
  newItem["Birth"] = "1985-05-06";
  newItem["Email"] = "jennifer@jennifersoft.com";
  newItem["Family"] = 0;

  int result;
  
  result = dac.Insert(newItem);
  Console.WriteLine("Insert AffectedCount: {0}", result);

  newItem["Name"] = "Jenny";
  result = dac.Update(newItem);
  Console.WriteLine("Update AffectedCount: {0}", result);
  
  DataSet ds = dac.SelectAll();
  foreach (DataRow row in ds.Tables["MemberInfo"].Rows)
  {
    Console.WriteLine(row["Email"]);
  }

  result = dac.Delete(newItem);
  Console.WriteLine("Delete AffectedCount: {0}", result);
}
```


POCO를 정의하지 않아도 DAC 계층과 데이터를 주고받는 데 아무런 문제가 없다.  
물론, DataSet에는 단점도 있다.  

메모리 증가  
  
POCO로 정의된 클래스는 정확히 그 데이터를 표현하기 위한 용량만큼의 메모리를 사용하지만  
범용적인 목적의 DataSet은 내부적으로 유지되는 메모리 용량이 증가한다.  

형식 안전성 미지원

DataRow에서 값을 보관하는 단위는 object가 된다.  
예를들면, byte 타입인 Famil 칼럼도 DataRow 객체에 보관될때는  
object로 변환되기 때문에 박싱/언박싱 문제가 발생한다.  

또한 DataRow에 지정하는 칼럼명에 오타가 발생하면 컴파일할 때 그 사실을 알 수 없다.  
마찬가지로 DataRow에 어떤 칼럼이 있는지 알기 힘들다.  
반면 POCO의 경우 해당 클래스 정의를 찾는 것도 직관적이고,  
도구의 도움으로 해당 객체에 포함된 맴버를 점(dot)을 찍으면  
나열되는 기능이 제공되므로 오타가 생길 위험도 거의 없다.  

이 중에서 메모리 증가 문제는 DataSet의 범용성으로 인해 어쩔 수 없지만  
형식 안정성에 대해서는 "강력한 형식(strong-typed)의 DataSet"을 구현한  
`Typed DataSet`을 사용하면 해결된다.  


### Typed DataSet은 생략..  


<br />


## 데이터베이스 트랜잭션

트랙잭션의 처음과 끝을 소스코드에서 `NpgsqlTransaction` 객체를 이용해 지정할 수 있다.


```c#
string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  conn.Open();
  using (NpgsqlTransaction transaction = conn.BeginTransaction())
  {
    // NpgsqlCommand를 이용해 쿼리 수행
    
    transaction.Commit();
  }
}
```


Connection 개체를 열고(Open), `BeginTransaction` 메서드를 호출하는 순간부터 트랜잭션은 시작된다.  
이후 수행되는 NpgsqlCommand는 트랜잭션에 소속되고 Commit 메서드를 호출해야만 데이터베이스에 모두 반영된다.  

만약 Commit 메서드를 호출하지 않거나 NpgsqlTransaction.Abort 메서드를 호출한다면  
해당 트랜잭션을 실패한 것이 되고 그 상이에 수행된 NpgsqlCommand로 인한 데이터베이스 변경은 모두 원복(rollback) 된다.  


```c#
string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  conn.Open();
  using (NpgsqlTransaction transaction = conn.BeginTransaction())
  {
    string query = "INSERT INTO member_info (name, birth, email, family) VALUES ('{0}', '{1}', '{2}', {3});";
    
    NpgsqlCommand cmd = new NpgsqlCommand
    {
      Connection = conn,
      Transaction = transaction,
      CommandText = string.Format(query, "Fox", "1970-01-25", "fox@gamil.com", "5")
    };
    
    cmd.ExecuteNonQuery();
    
    cmd.CommandText = string.Format(query, "Dana", "1972-01-25", "fox@gamil.com", "1");
    cmd.ExecuteNonQuery(); // 중복으로 예외 발생
    
    transaction.Commit();
  }
}
```


Email 칼럼이 중복됨으로써 두 번째 ExecuteNonQuery를 실행할 때 예외가 발생한다.  
따라서 Commit 메서드가 호출되지 않게 되고 트랜잭션은 실패한다.  

그런데 NpgsqlTransaction을 사용할 때는  
트랜잭션에 포함되는 NpgsqlCommand마다 일일이 트랜잭션에 변수를 대입해야 한다는 번거로움이 있다.  
이런 불편함을 해소하기 위해 닷넷 2.0에서 `System.Transaction.TracnsactionScope 타입`을 추가했다.  
이 클래스를 사용하면 다음과 같이 `간단하게 트랜잭션 구역만 지정하면 된다.`  


```c#
string connStr = 
  "Host=[서버];Port=[포트번호];Username=[계정명];Password=[비밀번호];Database=[DB명]";

using (NpgsqlConnection conn = new NpgsqlConnection(connStr))
{
  using (TransactionScope tx = new TransactionScope())
  {
    conn.Open();
    string query = "INSERT INTO member_info (name, birth, email, family) VALUES ('{0}', '{1}', '{2}', {3});";

    NpgsqlCommand cmd = new NpgsqlCommand
    {
      Connection = conn,
      CommandText = string.Format(query, "Fox", "1970-01-25", "fox@gamil.com", "5")
    };

    cmd.ExecuteNonQuery();

    cmd.CommandText = string.Format(query, "Dana", "1972-01-25", "fox@gamil.com", "1");
    cmd.ExecuteNonQuery(); // 중복으로 예외 발생

    tx.Complete();
  }
}
```


`TransactionScope`와 `NpgsqlTransaction`은 트랙잭션을 `시작하는 시점에도 차이가 있다.`  
NpgsqlTransaction의 경우 연결 개체가 Open한 이후에만 BeginTransaction 메서드를 호출해 트랜잭션을 시작할 수 있었지만  
TransactionScope는 그 반대로 연결 개체가 Open하기 이전에 미리 생성돼 있어야 한다.  
요즘은 직관성이나 부가적인 이유로 TransactionScope가 더 선호되는 추세다.  

<br />


## 참고 자료

- 시작하세요! C# 7.1 프로그래밍(정성태)
- http://www.npgsql.org/doc/index.html
- https://ko.wikipedia.org/wiki/ADO.NET
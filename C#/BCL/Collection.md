# BCL - 컬렉션

## 목차

- 개요
- 컬렉션
- System.Collections
  - System.Collections.ArrayList
  - System.Collections.HashTable
  - System.Collections.SortedList
  - System.Collections.Stack
  - System.Collections.Queue
- System.Collections.Generic
- System.Collections.Concurrent(ToDo)
- 참고자료

<br />

## 개요

본 문서는 `컬렉션`을 학습하며 정리한 문서입니다.

<br />

## 컬렉션

배열은 크기가 고정돼 있다는 특징이 있다.  
변수 자체에 재할당을 통해 크기를 바꾸는 것이 가능하지만 이전의 데이터는 보존되지는 않는다.

```c#
int[] arr = new int[40];
arr[0] = 50;

arr = new int[60]; // arr[0]에는 50이 보존되지 않는다.
```

배열과 달리 컬렉션은 응용 프로그램의 요구에 따라 동적으로 크기를 증가하거나 줄일 수 있다.  
BCL에서는 `System.Collections` 네임스페이스 하위에 이와 관련된 타입을 묶어서 제공한다.

<br />

## System.Collections

`System.Collections` 네임스페이스의 클래스는 요소를 특정 형식의 개체로 저장하지 않고 `object`형식의 개체로 저장한다.  

이는 `Collection`에 데이터를 넣을 때마다 `박싱`이  
`Collection`에서 데이터 접근 할 때마다 본래 타입으로의 형식 변환(`언박싱`)이  
반복적으로 일어나기 때문에 곧 성능의 저하로 나타난다.  

가능하면 `System.Collections`네임스페이스의 레거시 형식 대신  
`System.Collections.Generic` 네임스페이스 또는 `System.Collections.Concurrent` 네임스페이스의  
제네릭 컬렉션을 사용해야 한다.

`System.Collections` 네임스페이스에서 자주 사용하는 일부 클래스는 다음과 같다.  

|  클래스  | 설명 |
| ------------- | ------------- |
| ArrayList | 필요에 따라 크기가 동적으로 증가하는 개체의 배열 |
| Hashtable | 키의 해시 코드에 따리 구성된 키/값 쌍의 컬렉션 |
| SortedList | 키를 기준으로 정렬되고 키와 인덱스로 엑세스할 수 있는 키/값 쌍의 컬렉션 |
| Stack | LIFO(후입선출) 방식의 개체 컬렉션 |
| Queue | FIFO(선입선출) 방식의 개체 컬렉션 |


### System.Collections.ArrayList


ArrayList는 object 타입 및 그와 형변환할 수 있는 모든 타입을 인자로 받아  
컬렉션에 추가/삭제/변경/조회할 수 있는 기능을 구현한 타입이다.  

간단하게 크기를 자유롭게 변경 할 수 있는 배열이라고 보면 된다.  

```c#
ArrayList ar = new ArrayList();

ar.Add("Hello");
ar.Add(6);
ar.Add("World");
ar.Add(true);

// 숫자 6을 포함하고 있는지 판단
Console.WriteLine("Contains(6): " + ar.Contains(6));

// "World" 문자열을 컬렉션에서 삭제
ar.Remove("World");

// 2번째 요소의 값을 false로 변경
ar[2] = false;

Console.WriteLine();

// 컬렉션의 모든 요소를 출력
foreach (object obj in ar)
{
  Console.WriteLine(obj);
}
```

ArrayList느 object를 인자로 갖기 때문에 닷넷의 모든 타입을 담을 수 있다는 장점이 있지만  
반대로 이로 인해 `박싱`이 발생한다는 단점이 있다.  

따라서 `System.ValueType`을 상속받는 값 형식을 위한 컬렉션으로는 정당하지 않다.  

이를 위해 닷넷2.0부터 지원되는 `System.Collections.Generic` 네임스페이스에 포함되어 있는  
제네릭(Generic)이 적용된 `List<T>` 타입을 사용하는 것이 권장된다.  


```c#
List<string> ar2 = new List<string>();
ar2.Add("item1");
ar2.Add("item2");
ar2.Add("item3");
ar2.Add("item4");

Console.WriteLine("Contains(item1): " + ar.Contains("item1"));

ar2.Remove("item2");

ar2[2] = "updated";


foreach (string item in ar2)
{
  Console.WriteLine(item);
}
```

`System.Collections.Generic`는 뒤에서 자세히 다룬다.

ArrayList는 요소를 정렬할 수 있는 메서드도 제공한다.  
배열의 경우에는 Array.Sort() 정적 메서드를 이용했지만  
ArrayList에는 인스턴스 메서드롤 Sort가 제공된다.  

Sort 메서드를 호출할 때 제약사항이 있다면 ArrayList안에 있는 요소가 모두 같은 타입이어야 한다는 것이다.  
서로 다른 타입이 섞여 있으면 `ArgumentException` 예외가 발생한다.  

```c#
ArrayList ar = new ArrayList();

ar.Add("Hello");
ar.Add("World");
ar.Add("My");
ar.Add("Sample");

// int형을 추가 후 정렬하면 System.ArgumentException이 발생한다.
// ar.Add(3);

ar.Sort();
foreach (string item in ar)
{
  Console.WriteLine(item);
}
```

그런데 만약 `사용자 정의 타입`을 요소로 가지고 있다면 어떻게 Sort를 해야 할까?  
`IComparer`인터페이스를 구현한 타입의 객체를 Sort메서드의 2번째 인자로 전달하면 된다.  

또 다른 방법으로는 `IComparable`인터페이스를 이용하는 방법이다.  
ArrayList.Sort 메서드는 기본적으로 요소의 객체가 IComparable 인터페이스를 구현하고 있는지 확인한다.  
만약 그렇다면 `CompareTo`메서드를 호출해 그 결과로 정렬 작업을 수행한다.  

```c#
public class Person : IComparable
{
  private readonly int Age;
  private readonly string Name;

  public Person(int age, string name)
  {
    Age = age;
    Name = name;
  }

  public int CompareTo(object obj)
  {
    Person target = (Person) obj;
    if (Age > target.Age) return 1;
    else if (Age == target.Age) return 0;
    return -1;
  }

  public override string ToString()
  {
    return string.Format("{0}({1})", Name, Age);
  }
}

class Program
{
  static void Main(string[] args)
  {
    ArrayList ar = new ArrayList();

    ar.Add(new Person(32, "Cooper"));
    ar.Add(new Person(56, "Anderson"));
    ar.Add(new Person(17, "Sammy"));
    ar.Add(new Person(27, "Paul"));

    ar.Sort();

    foreach (Person person in ar)
    {
      Console.WriteLine(person);
    }
  }
}

// Sammy(17)
// Paul(27)
// Cooper(32)
// Anderson(56)
```


### System.Collections.HashTable

HashTable 컬렉션은 값(Value)뿐만 아니라 해시에 사용되는 키(Key)가 추가되어 빠른 검색 속도를 자랑한다.  
따라서 검색 속도의 중요도에 따라 ArrayList 또는 HashTable을 선택할지를 결정한다.  

컬렉션에 담긴 항목 수가 많아질수록 HashTable의 검색 속도가 ArrayList와 비교해서 더욱 빨라진다.  
이 때문에 작은 크기의 컬렉션인 경우에는 ArrayList를 선택해도 무방하다.  

```c#
Hashtable ht = new Hashtable();

ht.Add("key1", "add");
ht.Add("key2", "remove");
ht.Add("key3", "update");
ht.Add("key4", "search");

Console.WriteLine(ht["key4"]);

ht.Remove("key3");

ht["key2"] = "delete";

Console.WriteLine();

foreach (object key in ht.Keys)
{
  Console.WriteLine("{0} => {1}", key, ht[key]);
}

// search
//
// key2 => delete
// key1 => add
// key4 => search
```

HashTable은 키 값이 중복될 수 없다.  
키 값이 중복될 경우 Add 메서드에서 ArgumentException 예외가 발생한다.  

또한 HashTable은 ArrayList와 달리 키 값도 내부적으로 보관하고 있기 때문에  
그만큼 메모리가 낭비된다는 단점이 있다.  

마지막으로 ArrayList와 마찬가지로 키와 값이 모두 object 타입으로 다뤄지기 때문에  
박싱 문제가 발생한다.


### System.Collections.SortedList

SortedList는 ArrayList와 비슷할 것 같지만 의와로 Hashtable 타입과 사용법이 유사하다.  
단지 Hashtable에서는 키가 해시되어 데이터를 가리키는 인덱스 용도로 사용됐던 반면  
SortedList의 키는 그 자체가 정렬되어 값의 순서에 영향을 준다.  

```c#
SortedList sl = new SortedList();

sl.Add(32, "Cooper");
sl.Add(56, "Anderson");
sl.Add(17, "Sammy");
sl.Add(2, "Paul");

// 같은 타입이 아니면 System.ArgumentException 발생
// sl.Add('4', "Paul");

foreach (object key in sl.GetKeyList())
{
  Console.WriteLine(string.Format("{0} {1}", key, sl[key]));
}

// 17 Sammy
// 27 Paul
// 32 Cooper
// 56 Anderson
```

SortedList는 명시적으로 Sort 메서드를 호출할 필요 없이 Add 메서드에 요소가 삽입될 때 마다 바로 정렬된다.  
정렬은 키로 전달되는 첫 번째 인자를 기준으로 하고  
두 번째 인자로 전달된 값은 키의 정렬에 따라 순서가 바뀐다.  

Hashtable과 마찬가지로 키가 중복되는 경우 예외가 발생한다.

그리고 키 요소가 모두 같은 타입이어야 한다.


### System.Collections.Stack

Stack 타입은 자료구조의 스택을 그대로 구현한다.  
간단한게 LIFO(후입선출)이라고 하는데, Stack에 먼저 넣은 데이터는 가장 나중에 나온다는 특징이 있다.  

```c#
Stack st = new Stack();

st.Push(1);
st.Push(5);
st.Push(3);

int last = (int) st.Pop();

st.Push(7);
while (st.Count > 0)
{
  Console.Write(st.Pop() + ", ");
}
```

Stack 타입 역시 object를 인자로 다루기 때문에 박싱 문제가 발생한다.


### System.Collections.Queue

Queue 타입은 자료구조의 스택을 그대로 구현한다.  
간단한게 FIFO(선입선출)이라고 하는데, Stack에 먼저 넣은 데이터는 가장 먼저 나온다는 특징이 있다.  

```c#
Queue q = new Queue();

q.Enqueue(1);
q.Enqueue(5);
q.Enqueue(3);

int first = (int) q.Dequeue();

q.Enqueue(7);

while (q.Count > 0)
{
  Console.Write(q.Dequeue() + ", ");
}
```

Queue 타입 역시 object를 인자로 다루기 때문에 박싱 문제가 발생한다.

<br />

## System.Collections.Generic

`System.Collections.Generic` 네임스페이스의 클래스 중 하나를 사용하여 제네릭 컬렉션을 만들 수 있다.  
제네릭 컬렉션은 컬렉션 항목의 데이터 형식이 모두 같을 때 유용하다.  

`System.Collections.Generic` 네임스페이스에서 자주 사용하는 일부 클래스는 다음과 같다.  

|  클래스  | 설명 |
| ------------- | ------------- |
| List | 인덱스로 엑세스할 수 있는 개체 목록을 나타냄 |
| Dictionary | 키를 기반으로 구성된 키/값 쌍의 컬렉션 |
| SortedList | 연관된 `IComparer` 구현을 기반으로 키에 따라 정렬된 키/값 쌍의 컬렉션 |
| Stack | LIFO(후입선출) 방식의 개체 컬렉션 |
| Queue | FIFO(선입선출) 방식의 개체 컬렉션 |


### System.Collections.Generic.List

`System.Collections.Generic.List`는 `System.Collections.ArrayList`와 같은 기능을 하며,  
다른점이라면 형식매개변수 `T`로 타입을 지정하고 사용하는 것이다.  

`System.Collections.ArrayList`와 메소드도 동일하다.  

```c#
List<string> list = new List<string>();

list.Add("aaa");
list.Add("ccc");
list.Add("bbb");
list.Add("ddd");

Console.WriteLine("Contains(6): " + list.Contains("aaa"));

list.Remove("ccc");

list[2] = "updated";

Console.WriteLine();

foreach (string item in list)
{
  Console.WriteLine(item);
}

// Contains(6): True
//
// aaa
// bbb
// updated
```


### System.Collections.Generic.Dictionary

`System.Collections.Generic.Dictionary`는 `System.Collections.Hashtable`와 같은 기능을 하며,  
다른점이라면 형식매개변수 `T`로 타입을 지정하고 사용하는 것이다.  

`System.Collections.Hashtable`와 메소드도 유사하다.  

```c#
Dictionary<string, string> dic = new Dictionary<string, string>();

dic.Add("key1", "add");
dic.Add("key2", "remove");
dic.Add("key3", "update");
dic.Add("key4", "search");

Console.WriteLine(dic["key4"]);

dic.Remove("key3");

dic["key2"] = "delete";

Console.WriteLine();

foreach (string key in dic.Keys)
{
  Console.WriteLine("{0} => {1}", key, dic[key]);
}

// search
//
// key1 => add
// key2 => delete
// key4 => search
```


### System.Collections.Generic.SortedList

`System.Collections.Generic.SortedList`는 `System.Collections.SortedList`와 같은 기능을 하며,  
다른점이라면 형식매개변수 `T`로 타입을 지정하고 사용하는 것이다.  

`System.Collections.SortedList`와 메소드도 유사하다.  

```c#
SortedList<int, string> sl = new SortedList<int, string>();

sl.Add(32, "Cooper");
sl.Add(56, "Anderson");
sl.Add(17, "Sammy");
sl.Add(2, "Paul");

foreach (int key in sl.Keys)
{
  Console.WriteLine(string.Format("{0} {1}", key, sl[key]));
}

// 2 Paul
// 17 Sammy
// 32 Cooper
// 56 Anderson
```


### System.Collections.Generic.Stack

`System.Collections.Generic.Stack`는 `System.Collections.Stack`와 같은 기능을 하며,  
다른점이라면 형식매개변수 `T`로 타입을 지정하고 사용하는 것이다.  

`System.Collections.Stack`와 메소드도 유사하다.  

```c#
Stack<int> st = new Stack<int>();

st.Push(1);
st.Push(5);
st.Push(3);

int last = (int) st.Pop();

st.Push(7);
while (st.Count > 0)
{
  Console.Write(st.Pop() + ", ");
}

// 7, 5, 1,
```


### System.Collections.Generic.Queue

`System.Collections.Generic.Queue`는 `System.Collections.Queue`와 같은 기능을 하며,  
다른점이라면 형식매개변수 `T`로 타입을 지정하고 사용하는 것이다.  

`System.Collections.Queue`와 메소드도 유사하다.  

```c#
Queue<int> q = new Queue<int>();

q.Enqueue(1);
q.Enqueue(5);
q.Enqueue(3);

int first = (int) q.Dequeue();

q.Enqueue(7);

while (q.Count > 0)
{
  Console.Write(q.Dequeue() + ", ");
}

// 5, 3, 7,
```

<br />

## System.Collections.Concurrent

-  `ToDo 스레드 학습 후 정리`

<br />

## 참고 자료

- 시작하세요! C# 7.1 프로그래밍(정성태)
- [msdn](https://msdn.microsoft.com/ko-kr/library/ybcx56wz(v=vs.120).aspx?cs-save-lang=1&cs-lang=csharp#BKMK_Collections)
